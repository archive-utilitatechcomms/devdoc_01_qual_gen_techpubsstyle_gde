﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../../Resources/TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1>System: Simple Object Access Protocol (SOAP)</h1>
        <p><a href="https://en.wikipedia.org/wiki/SOAP" target="_blank" title="Wikipedia: SOAP" alt="Wikipedia: SOAP">SOAP</a> (formerly an acronym for Simple Object Access Protocol) is a messaging protocol specification for exchanging structured information in the implementation of web services in computer networks. Its purpose is to provide extensibility, neutrality, verbosity and independence. It uses XML Information Set for its message format, and relies on application layer protocols, most often <a href="Sys_HTTP.htm" target="_blank">Hypertext Transfer Protocol (HTTP)</a>, although some legacy systems communicate over <a href="Sys_SMTP.htm" target="_blank">Simple Mail Transfer Protocol (SMTP)</a>, for message negotiation and transmission.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Advantages</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>SOAP's neutrality characteristic explicitly makes it suitable for use with any transport protocol. Implementations often use HTTP as a transport protocol, but other popular transport protocols can be used. For example, SOAP can also be used over <a href="Sys_SMTP.htm" target="_blank">SMTP</a>, <a href="https://en.wikipedia.org/wiki/Java_Message_Service" target="_blank">JMS</a> and <a href="https://en.wikipedia.org/wiki/Message_queue" target="_blank">message queues</a>.</p>
                    </li>
                    <li>
                        <p>SOAP, when combined with <a href="Sys_HTTP.htm" target="_blank">HTTP</a> post/response exchanges, tunnels easily through existing firewalls and proxies, and consequently doesn't require modifying the widespread computing and communication infrastructures that exist for processing <a href="Sys_HTTP.htm" target="_blank">HTTP</a> post/response exchanges.</p>
                    </li>
                    <li>
                        <p>SOAP has available to it all the facilities of <a href="https://en.wikipedia.org/wiki/XML" target="_blank">XML</a>, including easy internationalisation and extensibility with <a href="https://en.wikipedia.org/wiki/XML" target="_blank">XML</a> Namespaces.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Disadvantages</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>When using standard implementation and the default SOAP/HTTP binding, the <a href="https://en.wikipedia.org/wiki/XML" target="_blank">XML</a> infoset is serialised as XML. To improve performance for the special case of XML with embedded binary objects, the <a href="https://en.wikipedia.org/wiki/Message_Transmission_Optimization_Mechanism" target="_blank">Message Transmission Optimisation Mechanism</a> was introduced.</p>
                    </li>
                    <li>
                        <p>When relying on HTTP as a transport protocol and not using <a href="https://en.wikipedia.org/wiki/WS-Addressing" target="_blank">Web Services Addressing</a> or an <a href="https://en.wikipedia.org/wiki/Enterprise_Service_Bus" target="_blank">Enterprise Service Bus</a>, the roles of the interacting parties are fixed. Only one party (the client) can use the services of the other.</p>
                    </li>
                    <li>
                        <p>SOAP is less "simple" than the name would suggest. The verbosity of the protocol, slow parsing speed of <a href="https://en.wikipedia.org/wiki/XML" target="_blank">XML</a>, and lack of a standardised interaction model led to the dominance of services using the <a href="Sys_HTTP.htm" target="_blank">HTTP</a> protocol more directly. See, for example, <a href="Sys_REST.htm" target="_blank">REST</a>.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>SOAP allows developers to invoke processes running on disparate operating systems (such as Windows, macOS, and Linux) to authenticate, authorise, and communicate using Extensible Markup Language (XML). Since Web protocols like <a href="Sys_HTTP.htm" target="_blank">HTTP</a> are installed and running on all operating systems, SOAP allows clients to invoke web services and receive responses independent of language and platforms.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Characteristics</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>SOAP provides the Messaging Protocol layer of a web services protocol stack for web services. It is an XML-based protocol consisting of three parts:</p>
                <ol>
                    <li>
                        <p>An envelope, which defines the message structure and how to process it.</p>
                    </li>
                    <li>
                        <p>A set of encoding rules for expressing instances of application-defined datatypes.</p>
                    </li>
                    <li>
                        <p>A convention for representing procedure calls and responses.</p>
                    </li>
                </ol>
                <p>SOAP has three major characteristics:</p>
                <ol>
                    <li>
                        <p>Extensibility (security and <a href="https://en.wikipedia.org/wiki/WS-Addressing" target="_blank" title="WS-Addressing" alt="WS-Addressing">WS-Addressing</a> are among the extensions under development).</p>
                    </li>
                    <li>
                        <p>Neutrality (SOAP can operate over any protocol such as <a href="Sys_HTTP.htm" target="_blank">HTTP</a>, <a href="Sys_SMTP.htm" target="_blank">SMTP</a>, <a href="https://en.wikipedia.org/wiki/Transmission_Control_Protocol" target="_blank">TCP</a>, <a href="https://en.wikipedia.org/wiki/SOAP-over-UDP" target="_blank">UDP</a>).</p>
                    </li>
                    <li>
                        <p>Independence (SOAP allows for any programming model).</p>
                    </li>
                </ol>
                <p>As an example of what SOAP procedures can do, an application can send a SOAP request to a server that has web services enabled - such as a tariff price database - with the parameters for a search. The server then returns a SOAP response (an XML-formatted document with the resulting data), e.g., prices, location, features. Since the generated data comes in a standardised machine-parsable format, the requesting application can then integrate it directly. The SOAP architecture consists of several layers of specifications for:</p>
                <ul>
                    <li>
                        <p>Message format.</p>
                    </li>
                    <li>
                        <p>Message Exchange Patterns (<a href="https://en.wikipedia.org/wiki/Message_Exchange_Pattern" target="_blank">MEP</a>).</p>
                    </li>
                    <li>
                        <p>Underlying transport protocol bindings.</p>
                    </li>
                    <li>
                        <p>Message processing models.</p>
                    </li>
                    <li>
                        <p>Protocol extensibility.</p>
                    </li>
                </ul>
                <p>SOAP evolved as a successor of <a href="https://en.wikipedia.org/wiki/XML-RPC" target="_blank">XML-RPC</a>, though it borrows its transport and interaction neutrality from Web Service Addressing and the envelope / header / body from elsewhere (probably from <a href="https://en.wikipedia.org/wiki/WDDX" target="_blank">WDDX</a>).</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>SOAP terminology</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>SOAP specification can be broadly defined to be consisting of the following 3 conceptual components: </p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Protocol concepts</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <ul>
                            <li>
                                <p><b>SOAP:</b> This is a set of rules formalising and governing the format and processing rules for information exchanged between a SOAP sender and a SOAP receiver.</p>
                            </li>
                            <li>
                                <p><b>SOAP nodes:</b> These are physical/logical machines with processing units which are used to transmit/forward, receive and process SOAP messages. These are analogous to nodes in a network.</p>
                            </li>
                            <li>
                                <p><b>SOAP roles:</b> Over the path of a SOAP message, all nodes assume a specific role. The role of the node defines the action that the node performs on the message it receives. For example, a role "none" means that no node will process the SOAP header in any way and simply transmit the message along its path.</p>
                            </li>
                            <li>
                                <p><b>SOAP protocol binding:</b> A SOAP message needs to work in conjunction with other protocols to be transferred over a network. For example, a SOAP message could use TCP as a lower layer protocol to transfer messages. These bindings are defined in the SOAP protocol binding framework.</p>
                            </li>
                            <li>
                                <p><b>SOAP features:</b> SOAP provides a messaging framework only. However, it can be extended to add features such as reliability, security, etc. There are rules to be followed when adding features to the SOAP framework.</p>
                            </li>
                            <li>
                                <p><b>SOAP module:</b> A collection of specifications regarding the semantics of SOAP header to describe any new features being extended upon SOAP. A module needs to realise zero or more features. SOAP requires modules to adhere to prescribed rules.</p>
                            </li>
                        </ul>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Data encapsulation concepts</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <ul>
                            <li>
                                <p><b>SOAP message:</b> Represents the information being exchanged between 2 SOAP nodes.</p>
                            </li>
                            <li>
                                <p><b>SOAP envelope:</b> It is the enclosing element of an XML message identifying it as a SOAP message.</p>
                            </li>
                            <li>
                                <p><b>SOAP header block:</b> A SOAP header can contain more than one of these blocks, each being a discrete computational block within the header. In general, the SOAP role information is used to target nodes on the path. A header block is said to be targeted at a SOAP node if the SOAP role for the header block is the name of a role in which the SOAP node operates. (ex: A SOAP header block with role attribute as ultimateReceiver is targeted only at the destination node which has this role. A header with a role attribute as next is targeted at each intermediary as well as the destination node.)</p>
                            </li>
                            <li>
                                <p><b>SOAP header:</b> A collection of one or more header blocks targeted at each SOAP receiver.</p>
                            </li>
                            <li>
                                <p><b>SOAP body:</b> Contains the body of the message intended for the SOAP receiver. The interpretation and processing of SOAP body is defined by header blocks.</p>
                            </li>
                            <li>
                                <p><b>SOAP fault:</b> In case a SOAP node fails to process a SOAP message, it adds the fault information to the SOAP fault element. This element is contained within the SOAP body as a child element.</p>
                            </li>
                        </ul>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Network (message sender and receiver) concepts </MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <ul>
                            <li>
                                <p><b>SOAP sender:</b> The node that transmits a SOAP message.</p>
                            </li>
                            <li>
                                <p><b>SOAP receiver:</b> The node receiving a SOAP message. (Could be an intermediary or the destination node).</p>
                            </li>
                            <li>
                                <p><b>SOAP message path:</b> The path consisting of all the nodes that the SOAP message traversed to reach the destination node.</p>
                            </li>
                            <li>
                                <p><b>Initial SOAP sender:</b> This is the node which originated the SOAP message to be transmitted. This is the root of the SOAP message path.</p>
                            </li>
                            <li>
                                <p><b>SOAP intermediary:</b> All the nodes in between the SOAP originator and the intended SOAP destination. It processes the SOAP header blocks targeted at it and acts to forward a SOAP message towards an ultimate SOAP receiver.</p>
                            </li>
                            <li>
                                <p><b>Ultimate SOAP receiver:</b> The destination receiver of the SOAP message. This node is responsible for processing the message body and any header blocks targeted at it.</p>
                            </li>
                        </ul>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Specification and building blocks</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The SOAP specification defines the messaging framework, which consists of:</p>
                <ul>
                    <li>
                        <p>The SOAP processing model, defining the rules for processing a SOAP message.</p>
                    </li>
                    <li>
                        <p>The SOAP extensibility model defining the concepts of SOAP features and SOAP modules.</p>
                    </li>
                    <li>
                        <p>The SOAP underlying protocol binding framework describing the rules for defining a binding to an underlying protocol that can be used for exchanging SOAP messages between SOAP nodes.</p>
                    </li>
                    <li>
                        <p>The SOAP message construct defining the structure of a SOAP message.</p>
                    </li>
                </ul>
                <p>A SOAP message is an ordinary XML document containing the following elements:</p>
                <table style="width: 100%;margin-left: 0;margin-right: auto;mc-table-style: url('../../Resources/TableStyles/TabMenuMap.css');" class="TableStyle-TabMenuMap" cellspacing="0">
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 50px;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 500px;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 50px;" />
                    <thead>
                        <tr class="TableStyle-TabMenuMap-Head-Header1">
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Element</th>
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Description</th>
                            <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Required</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Envelope</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Identifies the XML document as a SOAP message.</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Yes</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Header</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Contains header information.</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">No</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Body</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Contains call and response information.</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Yes</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">Fault</td>
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">Provides information about errors that occurred while processing the message.</td>
                            <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">No</td>
                        </tr>
                    </tbody>
                </table>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Transport methods</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Both <a href="Sys_SMTP.htm" target="_blank">SMTP</a> and <a href="Sys_HTTP.htm" target="_blank">HTTP</a> are valid application layer protocols used as transport for SOAP, but HTTP has gained wider acceptance as it works well with today's internet infrastructure; specifically, HTTP works well with network <a href="Sys_Firewall.htm" target="_blank">firewalls</a>. SOAP may also be used over <a href="Sys_HTTPS.htm" target="_blank">HTTPS</a> (which is the same protocol as HTTP at the application level, but uses an <a href="https://en.wikipedia.org/wiki/Transport_Layer_Security" target="_blank">encrypted transport protocol</a> underneath) with either simple or mutual authentication; this is the advocated WS-I method to provide web service security as stated in the WS-I Basic Profile 1.1.</p>
                <p>This is a major advantage over other distributed protocols like <a href="https://en.wikipedia.org/wiki/General_Inter-ORB_Protocol" target="_blank">GIOP/IIOP</a> or <a href="https://en.wikipedia.org/wiki/Distributed_Component_Object_Model" target="_blank">DCOM</a>, which are normally filtered by firewalls. SOAP over <a href="https://en.wikipedia.org/wiki/AMQP" target="_blank">AMQP</a> is yet another possibility that some implementations support. SOAP also has an advantage over DCOM that it is unaffected by security rights configured on the machines that require knowledge of both transmitting and receiving nodes. This lets SOAP be loosely coupled in a way that is not possible with DCOM. There is also the <a href="https://en.wikipedia.org/wiki/SOAP-over-UDP" target="_blank">SOAP-over-UDP</a> <a href="https://en.wikipedia.org/wiki/OASIS_(organization)" target="_blank">OASIS</a> standard.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Message format</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p><a href="https://en.wikipedia.org/wiki/XML_Information_Set" target="_blank">XML Information Set</a> was chosen as the standard message format because of its widespread use by major corporations and <a href="https://en.wikipedia.org/wiki/Open-source_model" target="_blank">open source</a> development efforts. Typically, XML Information Set is <a href="https://en.wikipedia.org/wiki/Serialization" target="_blank">serialised</a> as <a href="https://en.wikipedia.org/wiki/XML" target="_blank">XML</a>. A wide variety of freely available tools significantly eases the transition to a SOAP-based implementation. The somewhat lengthy syntax of XML can be both a benefit and a drawback. While it promotes readability for humans, facilitates error detection, and avoids interoperability problems such as byte-order (<a href="https://en.wikipedia.org/wiki/Endianness" target="_blank">endianness</a>), it can slow processing speed and can be cumbersome. For example, <a href="https://en.wikipedia.org/wiki/CORBA" target="_blank">CORBA</a>, <a href="https://en.wikipedia.org/wiki/General_Inter-ORB_Protocol" target="_blank">GIOP</a>, <a href="https://en.wikipedia.org/wiki/Distributed_Component_Object_Model" target="_blank">ICE</a>, and <a href="https://en.wikipedia.org/wiki/Distributed_Component_Object_Model" target="_blank">DCOM</a> use much shorter, binary message formats. On the other hand, hardware appliances are available to accelerate processing of XML messages. <a href="https://en.wikipedia.org/wiki/Binary_XML" target="_blank">Binary XML</a> is also being explored as a means for streamlining the throughput requirements of XML. XML messages by their self-documenting nature usually have more 'overhead' (e.g., headers, nested tags, delimiters) than actual data in contrast to earlier protocols where the overhead was usually a relatively small percentage of the overall message.</p>
                <p>In financial messaging SOAP was found to result in a 2–4 times larger message than previous protocols <a href="https://en.wikipedia.org/wiki/Financial_Information_eXchange" target="_blank">FIX (Financial Information Exchange)</a> and <a href="https://en.wikipedia.org/wiki/Common_Data_Representation" target="_blank">CDR (Common Data Representation)</a>.</p>
                <p>XML Information Set does not have to be serialised in XML. For instance, CSV and <a href="https://en.wikipedia.org/wiki/JSON" target="_blank">JSON</a> XML-infoset representations exist. There is also no need to specify a generic transformation framework. The concept of SOAP bindings allows for specific bindings for a specific application. The drawback is that both the senders and receivers have to support this newly defined binding.</p>
                <p>The message below is encapsulated in HTTP and requests a stock price for AT&amp;T (stock ticker symbol "T").</p><pre class="code">POST /InStock HTTP/1.1
Host: www.example.org
Content-Type: application/soap+xml; charset=utf-8
Content-Length: 299
SOAPAction: "http://www.w3.org/2003/05/soap-envelope"

&lt;?xml version="1.0"?&gt;
&lt;soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:m="http://www.example.org"&gt;
  &lt;soap:Header&gt;
  &lt;/soap:Header&gt;
  &lt;soap:Body&gt;
    &lt;m:GetStockPrice&gt;
      &lt;m:StockName&gt;T&lt;/m:StockName&gt;
    &lt;/m:GetStockPrice&gt;
  &lt;/soap:Body&gt;
&lt;/soap:Envelope&gt;</pre>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>