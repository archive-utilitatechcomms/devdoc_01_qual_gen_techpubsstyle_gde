﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>Automated Build</MicroContentPhrase>
        <MicroContentPhrase>Tell me about micro content</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>DevOps - Automated Build</xhtml:h1>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                    <xhtml:p>In the context of software development, build refers to the process that converts files and other assets under the developers’ responsibility into a software product in its final or consumable form. The build may include:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>
                            <xhtml:p>Compiling source files.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Packaging compiled files into compressed formats (such as jar, zip).</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Producing installers.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Creating or updating of database schema or data.</xhtml:p>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>The build is automated when these steps are repeatable, require no direct human intervention, and can be performed at any time with no information other than what is stored in the source code control repository.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Build automation is a prerequisite to effective use of <xhtml:a xhtml:href="DevOps_CI.htm" xhtml:target="_blank" xhtml:title="Continuous Integration" xhtml:alt="Continuous Integration">continuous integration</xhtml:a>. However, it brings benefits of its own:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Eliminating a source of variation, and thus of defects; a manual build process containing a large number of necessary steps offers as many opportunities to make mistakes.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Requiring thorough documentation of assumptions about the target environment, and of dependencies on third party products.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>The practice of build automation should not be confused with continuous integration: the latter consists of “executing” the build process as frequently as possible (ideally whenever a code change is checked into the source code control repository) and “verifying” the correctness of the resulting product, in particular by <xhtml:a xhtml:href="DevOps_UnitTesting.htm" xhtml:target="_blank" xhtml:title="Unit Tests" xhtml:alt="Unit Tests">unit tests</xhtml:a>.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>In particular, continuous integration tools (CruiseControl, Hudson, etc.) are a category distinct from build automation tools (make, Ant, Maven, rake, etc.).</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Being able to trigger some build operations from within a development environment (IDE) is usually not sufficient: as it is often the case that some build operations are not supported within the IDE, it must be possible to perform a build outside of the IDE.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>The duration of a build process should be under ten minutes, including the execution of automated tests; beyond this order of magnitude it will generally be difficult for the team to achieve continuous integration.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>1977: creation of the “make” tool for Unix systems – the principle of automating software builds is not a new idea.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>1990s: owing to the rise in popularity of RAD tools and IDEs, “make” type tools acquire a mixed reputation.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>2000s: even though the practice is far from new, nor limited to Agile teams, it is partly due to Agile practices that a revival of “make” type build automation takes place.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Signs of Use</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The best way to ascertain whether a team practices build automation is a surprise test: ask the team to provide an installable version of the product. Measure how much time is necessary to obtain a release, then attempt to install or deploy to an off the shelf PC or environment – one which has not been previously set up by the development team. Any “surprise” during this process will suggest ways to improve the automated build process.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>