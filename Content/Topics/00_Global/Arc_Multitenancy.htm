﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>Architecture: Multitenancy</h1>
        <p>The term "software multitenancy" refers to a software architecture in which a single instance of software runs on a server and serves multiple tenants. Systems designed in such manner are often called shared (in contrast to dedicated or isolated). A tenant is a group of users who share a common access with specific privileges to the software instance. </p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Differentiation from virtualization</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>In a multitenancy environment, multiple customers share the same application, running on the same operating system, on the same hardware, with the same data-storage mechanism. The distinction between the customers is achieved during application design, thus customers do not share or see each other's data. Compare this with virtualization where components are transformed, enabling each customer application to appear to run on a separate virtual machine.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Competitive differentiation</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Some companies actively promote the principle of multitenancy and use it as a source of competitive differentiation. The use of multitenancy is increasing day by day.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>With a multitenant architecture, a software application is designed to provide every tenant a dedicated share of the instance - including its data, configuration, user management, tenant individual functionality and non-functional properties. Multitenancy contrasts with multi-instance architectures, where separate software instances operate on behalf of different tenants. </p>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">Some commentators regard multitenancy as an important feature of cloud computing.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Cost savings</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Multitenancy allows for cost savings over and above the basic economies of scale achievable from consolidating IT resources into a single operation. An application instance usually incurs a certain amount of memory and processing overhead which can be substantial when multiplied by many customers, especially if the customers are small. Multitenancy reduces this overhead by spreading it over many customers.</p>
                <p>Further cost savings may come from licensing costs of the underlying software (such as operating systems and database management systems). Put crudely, if you can run everything on a single software instance, you only have to buy one software license. The cost savings can be eclipsed by the difficulty of scaling the single instance as demand grows - increasing the performance of the instance on a single server can only be done by buying faster hardware, such as fast CPUs, more memory, and faster disk systems, and typically these costs grow faster than if the load was split between multiple servers with roughly the same aggregate capacity.[citation needed] In addition, development of multitenant systems[8] is more complex, and security testing is more stringent owing to the fact that multiple customers' data is being commingled.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Data aggregation / data mining</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>One of the most compelling reasons for vendors/ISVs to utilise multitenancy is for the inherent data aggregation benefits. Instead of collecting data from multiple data sources, with potentially different database schemas, all data for all customers is stored in a single database schema. Thus, running queries across customers, mining data, and looking for trends is much simpler. This reason is probably overhyped as one of the core multitenancy requirements is the need to prevent Service Provider access to customer (tenant) information. Further, it is common to separate the operational database from the mining database (usually because of different workload characteristics), thus weakening the argument even more.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Complexity</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Because of the additional customisation complexity and the need to maintain per-tenant metadata, multitenant applications require a larger development effort. Considerations such as vector-based data sequencing, encryptable algorithm infrastructure, and virtualized control interfaces, must be taken into account.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Release management</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Multitenancy simplifies the release management process. In a traditional release management process, packages containing code and database changes are distributed to client desktop and/or server machines; in the single-instance case, this would be one server machine per customer. These packages then have to be installed on each individual machine. With the multitenant model, the package typically only needs to be installed on a single server. This greatly simplifies the release management process, and the scale is no longer dependent on the number of customers.</p>
                <p>At the same time, multitenancy increases the risks and impacts inherent in applying a new release version. As there is a single software instance serving multiple tenants, an update on this instance may cause downtime for all tenants even if the update is requested and useful for only one tenant. Also, some bugs and issues resulted from applying the new release could manifest in other tenants' personalized view of the application. Because of possible downtime, the moment of applying the release may be restricted depending on time usage schedule of more than one tenant.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Customisation</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Multitenant applications are typically required to provide a high degree of customization to support each target organization's needs. Customization typically includes the following aspects:</p>
                <ul>
                    <li>
                        <p><b>Branding:</b> allowing each organization to customize the look-and-feel of the application to match their corporate branding (often referred to as a distinct "skin").</p>
                    </li>
                    <li>
                        <p><b>Workflow:</b> accommodating differences in workflow to be used by a wide range of potential customers.</p>
                    </li>
                    <li>
                        <p><b>Extensions to the data model:</b> supporting an extensible data model to give customers the ability to customize the data elements managed by the application to meet their specific needs.</p>
                    </li>
                    <li>
                        <p><b>Access control:</b> letting each client organization independently customize access rights and restrictions for each user.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Quality of service</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Multitenant applications are expected to provide adequate isolation of security, robustness and performance between multiple tenants which is provided by the layers below the application in case of multi-instance applications.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Virtualisation</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The costs of redesigning applications for multitenancy can be significant, especially for software vendors who continue to offer an on-premises single tenant version of their product. They end up being forced to support two distinct products with all the resulting costs.</p>
                <p>An increasingly viable alternative route to multitenancy that eliminates the need for significant architectural change is to use virtualization technology to host multiple isolated instances of an application on one or more servers. Indeed, when applications are repackaged as virtual appliances the same appliance image can be deployed in ISV hosted, on-premises or trusted-third party locations and even migrated from one deployment site to another over time.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
    </body>
</html>