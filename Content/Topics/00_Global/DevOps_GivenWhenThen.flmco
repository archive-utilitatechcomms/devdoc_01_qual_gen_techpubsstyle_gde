﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>Given-When-Then</MicroContentPhrase>
        <MicroContentPhrase>Tell me about micro content</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>DevOps - Given-When-Then</xhtml:h1>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                    <xhtml:p>The Given-When-Then formula is a template intended to guide the writing of <xhtml:a xhtml:href="DevOps_AcceptanceTests.htm" xhtml:target="_blank" xhtml:title="Acceptance Tests" xhtml:alt="Acceptance Tests">acceptance tests</xhtml:a> for a <xhtml:a xhtml:href="DevOps_UserStories.htm" xhtml:target="_blank" xhtml:title="User Story" xhtml:alt="User Story">User Story</xhtml:a>:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>
                            <xhtml:p>(Given) some context.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>(When) some action is carried out.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>(Then) a particular set of observable consequences should obtain.</xhtml:p>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>An example:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>
                            <xhtml:p>Given my bank account is in credit, and I made no withdrawals recently.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>When I attempt to withdraw an amount less than my card’s limit.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Then the withdrawal should complete without errors or warnings.</xhtml:p>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>Tools such as JBehave, RSpec or Cucumber encourage use of this template, though it can also be used purely as a heuristic irrespective of any tool.</xhtml:p>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>