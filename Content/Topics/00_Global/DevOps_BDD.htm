﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>DevOps - Behaviour Driven Development (BDD)</h1>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" target="_blank" title="Development Resources: IT Project Management" alt="Development Resources: IT Project Management">Development Resources: IT Project Management</a>.</p>
        <p>Behaviour Driven Development (BDD) is a synthesis and refinement of practices stemming from <a href="DevOps_TDD.htm" target="_blank" title="Test Driven Development (TDD)" alt="Test Driven Development (TDD)">Test Driven Development (TDD)</a> and <a href="DevOps_AcceptanceTests.htm" target="_blank" title="Acceptance Test Driven Development (ATDD)" alt="Acceptance Test Driven Development (ATDD)">Acceptance Test Driven Development (ATDD)</a>. BDD augments TDD and ATDD with the following tactics:</p>
        <ul>
            <li>
                <p>Apply the <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/04_PM_Plays/04_PM_Plays_5Whys.htm" target="_blank" title="Development Resources: IT Project Management (Playbooks - Five Why's)" alt="Development Resources: IT Project Management (Playbooks - Five Why's)">Five Why’s</a> principle to each proposed <a href="DevOps_UserStories.htm" target="_blank" title="User Story" alt="User Story">user story</a>, so that its purpose is clearly related to business outcomes.</p>
            </li>
            <li>
                <p>thinking “from the outside in”, in other words implement only those behaviours which contribute most directly to these business outcomes, so as to minimize waste.</p>
            </li>
            <li>
                <p>describe behaviours in a single notation which is directly accessible to domain experts, testers and developers, so as to improve communication.</p>
            </li>
            <li>
                <p>apply these techniques all the way down to the lowest levels of abstraction of the software, paying particular attention to the distribution of behavior, so that evolution remains cheap.</p>
            </li>
        </ul>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Also Known As</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>BDD is also referred to as Specification by Example.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Teams already using TDD or ATDD may want to consider BDD for several reasons:</p>
                <ul>
                    <li>
                        <p>BDD offers more precise guidance on organizing the conversation between developers, testers and domain experts.</p>
                    </li>
                    <li>
                        <p>Notations originating in the BDD approach, in particular the <a href="DevOps_GivenWhenThen.htm" target="_blank" title="Given-When-Then" alt="Given-When-Then">given-when-then</a> canvas, are closer to everyday language and have a shallower learning curve compared to those of tools such as Fit/FitNesse.</p>
                    </li>
                    <li>
                        <p>Tools targeting a BDD approach generally afford the automatic generation of technical and end user documentation from BDD “specifications”.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Although Dan North, who first formulated the BDD approach, claims that it was designed to address recurring issues in the teaching of TDD, it is clear that BDD requires familiarity with a greater range of concepts than TDD does, and it seems difficult to recommend a novice programmer should first learn BDD without prior exposure to TDD concepts</p>
                <p>The use of BDD requires no particular tools or programming languages, and is primarily a conceptual approach; to make it a purely technical practice or one that hinges on specific tooling would be to miss the point altogether</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>2003: <a href="http://agiledox.sourceforge.net/" target="_blank">agiledox</a>, the ancestor of BDD, is a tool generating technical documentation automatically from JUnit tests, written by Chris Stevenson.</p>
                    </li>
                    <li>
                        <p>2004: Chris Matts and Dan North proposed the <a href="DevOps_GivenWhenThen.htm" target="_blank" title="Given-When-Then" alt="Given-When-Then">given-when-then</a> canvas to expand the scope of BDD to business analysis and documents.</p>
                    </li>
                    <li>
                        <p>2004: in order to test his hypotheses about de-emphasizing “test” terminology in favour of “behaviour”, Dan North releases <a href="http://jbehave.org/" target="_blank">JBehave</a>.</p>
                    </li>
                    <li>
                        <p>2006: Dan North documents the approach in  “<a href="http://blog.dannorth.net/introducing-bdd/" target="_blank" title="Introducing BDD" alt="Introducing BDD">Introducing BDD</a>”.</p>
                    </li>
                    <li>
                        <p>2006-2009: several new tools are released confirming the community’s investment in BDD, such as RSpec or more recently, Cucumber and GivWenZen.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Signs of Use</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>A team using BDD should be able to provide a significant portion of “functional documentation” in the form of User Stories augmented with executable scenarios or examples.</p>
                    </li>
                    <li>
                        <p>Instead of referring to “tests”, a BDD practitioner will prefer the terms “scenario” and “specification”. As currently practiced, BDD aims to gather in a single place the specification of an outcome valuable to a user, generally using the <a href="DevOps_UserStories.htm" target="_blank" title="User Stories - Role-Feature" alt="User Stories - Role-Feature">role-feature</a> matrix of (<a href="DevOps_UserStories.htm" target="_blank" title="User Stories" alt="User Stories">User Stories</a>), as well as examples or scenarios expressed in the form <a href="DevOps_GivenWhenThen.htm" target="_blank" title="Given-When-Then" alt="Given-When-Then">given-when-then</a>; these two notations being often considered the most readable.</p>
                    </li>
                    <li>
                        <p>In emphasizing the term “specification”, the intent of BDD is to provide a single answer to what many Agile teams view as separate activities: the creation of unit tests and “technical” code on one hand, the creation of functional tests and “features” on the other hand. This should lead to increased collaboration between developers, test specialists, and domain experts.</p>
                    </li>
                    <li>
                        <p>Rather than refer to “the unit tests of a class”, a practitioner or a team using BDD prefers to speak of “the specifications of the behaviour of the class”. This reflects a greater focus on the documentary role of such specifications: their names are expected to be more expressive, and, when completed with their description in <a href="DevOps_GivenWhenThen.htm" target="_blank" title="Given-When-Then" alt="Given-When-Then">given-when-then</a> format, to serve as technical documentation.</p>
                    </li>
                    <li>
                        <p>Rather than refer to “functional tests”, the preferred term will be “specifications of the product’s behaviour”. The technical aspects of BDD are placed on an equal footing with techniques encouraging more effective conversation with customers, users and domain experts.</p>
                    </li>
                    <li>
                        <p>In addition to <a href="DevOps_Refactoring.htm" target="_blank" title="Refactoring" alt="Refactoring">refactoring</a> techniques already present in TDD, the design philosophy in BDD pays particular attention to appropriate distribution of responsibilities among classes, which leads its practitioners to emphasize “<a href="DevOps_MockObjects.htm" target="_blank" title="Mock Objects" alt="Mock Objects">mocking</a>”.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>“<a href="https://dannorth.net/introducing-bdd/" target="_blank" title="Introducing BDD" alt="Introducing BDD">Introducing BDD</a>”, by Dan North (2006)</p>
                    </li>
                    <li>
                        <p>“<a href="http://lizkeogh.com/2009/11/06/translating-tdd-to-bdd/" target="_blank" title="Translating TDD to BDD" alt="Translating TDD to BDD">Translating TDD to BDD</a>”, by Liz Keogh (2009)</p>
                    </li>
                    <li>
                        <p><a href="http://www.citeulike.org/user/Scis0000002/article/7536581" target="_blank" title="A tool stack for implementing Behaviour-Driven Development in Python Language" alt="A tool stack for implementing Behaviour-Driven Development in Python Language">A tool stack for implementing Behaviour-Driven Development in Python Language</a> by Tavares, Rezende, dos Santos, Manhaes, de Carvalho (2010)</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>