﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>Device Testing</MicroContentPhrase>
        <MicroContentPhrase>Testing Devices</MicroContentPhrase>
        <MicroContentPhrase>Metrology testing</MicroContentPhrase>
        <MicroContentPhrase>Protocol testing</MicroContentPhrase>
        <MicroContentPhrase>Security testing</MicroContentPhrase>
        <MicroContentPhrase>Functional testing</MicroContentPhrase>
        <MicroContentPhrase>Interoperability testing</MicroContentPhrase>
        <MicroContentPhrase>Interchangeability testing</MicroContentPhrase>
        <MicroContentPhrase>Accelerated life testing</MicroContentPhrase>
        <MicroContentPhrase>End‐to‐end testing</MicroContentPhrase>
        <MicroContentPhrase>Mandatory Testing</MicroContentPhrase>
        <MicroContentPhrase>Optional Testing</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:head>
                    <xhtml:link xhtml:href="../../Resources/TableStyles/TabMenuMap.css" xhtml:rel="stylesheet" MadCap:stylesheetType="table" />
                </xhtml:head>
                <xhtml:body>
                    <xhtml:h1>System: Smart Device - Testing Devices</xhtml:h1>
                    <xhtml:p>The government’s impact assessment for the national rollout estimates the cost of smart devices to be a little under £5 billion, with installation costing another £1.6 billion. Based on these figures, a 1 per cent failure of installed devices would cost in the region of £66 million. It’s essential, therefore, that devices are subjected to adequate testing before being installed in customers’ homes.</xhtml:p>
                    <xhtml:p>
                        <xhtml:img xhtml:src="00_IMG_Sys_TestingDev.png" xhtml:class="Screen_Thumbnail" xhtml:style="max-width: 955px;max-height: 1089px;" />
                    </xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information in the <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_04_TP_SMETS/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="SMETS DevOps Help Guide" xhtml:alt="SMETS DevOps Help Guide">SMETS DevOps Help Guide</xhtml:a>.</xhtml:p>
                    <xhtml:h2>Device Testing - Overview</xhtml:h2>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Who’s responsible?</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>With the exception of the CH (which is the responsibility of the CSP), the responsibility for just about all other device testing falls to the registered supplier. If the registered supplier changes (that is, if the customer switches supplier), the responsibility passes to the new supplier.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Why test?</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The requirements for metrology testing are set out in the Electricity and Gas Acts. SMETS defines the requirements for protocol, security and functional testing. The Smart Energy Code (SEC) not only requires a supplier to use SMETS‐compliant equipment (section F3.4), but also requires them to install interoperable devices (section F4.3). </xhtml:p>
                            <xhtml:p>No regulatory obligation exists to do any interchangeability, accelerated life or end‐to‐end testing, but most suppliers recognise the commercial imperative for these.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Providing evidence of testing</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Suppliers are ultimately responsible for ensuring that a device has been adequately tested and for providing evidence that this has been done, if requested to do so by the DCC.</xhtml:p>
                            <xhtml:p>However, this doesn’t mean that they have to do the actual physical testing. It’s likely that the device manufacturers will be required to provide evidence that their products have been adequately tested before even making it onto a supplier’s shortlist.</xhtml:p>
                            <xhtml:p>Providing evidence is easy enough for testing where established testing regimes and certification processes exist (for example, CPA Certificates for Security testing). But it’s a bit more challenging where no such regimes and processes exist (as is currently the case for functional, interoperability and interchangeability testing).</xhtml:p>
                            <xhtml:p>In an ideal world, a supplier would insist on seeing a full set of test certificates as a prerequisite to procuring a device. In practice, these won’t be available within the timescales currently set out in the Joint Industry Plan and suppliers are likely to make procurement decisions contingent on manufacturers achieving certification post contract signature.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Smart Metering Device Assurance</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>To address the absence of certification schemes for interoperability and interchangeability testing, the following bodies came together and appointed Gemserv as the Smart Metering Device Assurance (SMDA) scheme operator:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Energy UK, representing the suppliers responsible for testing the devices.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>British Electrotechnical and Allied Manufacturers’ Association (BEAMA), representing the device manufacturers.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Community of Meter Asset Providers (CMAP), representing the device owners.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Energy and Utilities Alliance, representing just about everybody.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>The SMDA scheme operator is tasked with establishing an independent assurance scheme covering interoperability and interchangeability testing. This is likely to entail:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Developing a set of test specifications.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Approving one or more test houses to conduct the tests.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Awarding certification based on test output.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>Therefore, device manufacturers will likely need to add an SMDA certificate to their bundle of certifications in order to get on a supplier’s shortlist.</xhtml:p>
                            <xhtml:p>That just leaves functional and accelerated life testing, neither of which currently has an assurance scheme. It’s conceivable that functional testing may find its way into the SMDA’s remit, but accelerated life testing probably won’t, so it will doubtless fall to testing houses to come up with compelling accelerated life testing propositions.</xhtml:p>
                            <xhtml:p>It would make a lot of sense for SMDA to add functional testing to its remit. Whilst much of a device’s functionality will undoubtedly be tested during end‐to‐end testing, suppliers will not be testing non‐supplier functionality such as that exclusive to network operators. Just because a supplier doesn’t have access to maximum demand registers doesn’t free them from their obligation to demonstrate that they work. Far easier to give this to the SMDA scheme operator to sort out.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:h2>Device Testing - Types</xhtml:h2>
                    <xhtml:p>At least eight different types of testing can be applied to devices – five of which are mandated. The following summarises these tests, including the drivers for undertaking the various types of testing and the practitioners capable of providing them:</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Metrology testing</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>This is mandatory testing to ensure that ESMEs and GSMEs are certified safe and fit for purpose. ‘Safe and fit for purpose’ translates into compliance with Schedule 7 of the Electricity Act 1989 and its associated Statutory Instruments (for an ESME), Section 17 of the Gas Act 1986 (for a GSME) and the European Measuring Instruments Directive (MID 2004/22/EC) (for both).</xhtml:p>
                            <xhtml:p>Testing is conducted by an Ofgem‐appointed meter examiner (currently SGS (UK) Ltd) and, if successful, results in the meter being listed in a statutory register of meter types approved for use in the UK. After a model is approved, individual meters of that type can be tested and, once certified, are sealed to secure the measuring elements of the meter from tamper. This form of meter approval and verification has been around for some time and applies to all meters (smart or traditional), so it’s business‐as‐usual for meter manufacturers.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Protocol testing</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Protocol testing is also mandatory and ensures that a smart device conforms to the communications protocols that it uses over the HAN and/or SM WAN. SMETS2 devices use a new hybrid of ZigBee and Device Language Message Specification Companion Specification for Energy Metering (DLMS/COSEM) protocols as defined in the GB Companion Specification (GBCS), so protocol testing means compliance with the GBCS.</xhtml:p>
                            <xhtml:p>Four Authorised Test Service Providers exist for certifying ZigBee products:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>TRaC Global</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>China Electronics Standardisation Institute</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>National Technical Systems Inc.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>TU'' VRheinland</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>In contrast, any member of the DLMS User Association who has purchased the DLMS Conformance Test Tool (CTT) can do DLMS/COSEM testing. As such, most device manufacturers do their own DLMS/COSEM protocol testing.</xhtml:p>
                            <xhtml:p>ZigBee and DLMS/COSEM testing is well established, but GBCS compliance is new. It’s likely, however, that GBCS testing will emerge as an extension to the existing ZigBee testing services.</xhtml:p>
                            <xhtml:p>In an attempt to accelerate development of devices in the absence of a DCC environment, the DCC has commissioned Critical Software to modify a tool previously developed to validate GBCS (the unfortunately named GBCS Interface Testing, or GIT for short). The modified tool, GIT for Industry (GFI) will allow device manufacturers to generate ‘gold standard’ GBCS commands on a ZigBee HAN to which they can connect and test their devices. By the time you read this, the first version of this tool should have been released and we should be well into a series of seven GBCS Test Events organised by the DCC for budding device manufacturers to come along and test out their devices against GIT.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Security testing</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>ESMEs, GSMEs, CHs and most Type 1 Devices (devices that actually get to do things) need to be security certified under CESG’s Commercial Product Assurance (CPA) scheme. Type 2 Devices, which are essentially ‘read only’, don’t need to be CPA assured. Specific CPA Security Characteristics exist for each device type (ESME, GSME, CH and HCALCS) that set out the features, testing and deployment requirements necessary to meet CPA certification. </xhtml:p>
                            <xhtml:p>These cover features such as:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Physical protection (detecting, logging and notifyingtampering, for example).</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Message protection (authentication, integrity checking, protection against replay and so on).</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Protection of sensitive data (encryption and provision of Privacy PINs).</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">PPMIDs, although designated as Type 1 Devices, are not subject to CPA testing by virtue of the fact that, although they get to control supply, they can only enable it, not disable it. CESG’s website cites six CESG‐approved CPA Test Labs (CGI being one of them). Although the CPA Security Characteristics for these devices are new, the CPA testing process is well established.<xhtml:br />CESG is a branch of the more famous Government Communications Headquarters (GCHQ). It used to stand for Communications Electronics Security Group but it now stands for the National Technical Authority for Information Assurance (no doubt they retained the CESG acronym to confuse the enemy).</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Functional testing</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>ESMEs, GSMEs, PPMIDs, HCALCS and IHDs must be tested to ensure that they meet the functional requirements set out in SMETS. Similarly, the CSPs must demonstrate that their CHs comply with CHTS functionality. SMETS functional testing is new but is probably something that existing test houses will want to offer. Whether there’ll be any accepted certification scheme for this testing is another matter.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Interoperability testing</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>In this context, interoperability means the ability for a ESME, GSME, CH or Type 1 Device to respond to commands received from the DCC in accordance with GBCS (Type 2 Devices don’t get to receive HAN commands, so this type of testing doesn’t apply to them). As with functional testing, interoperability testing is new. Unlike functional testing, it requires a DCC test environment and the ability to interface with it. Would‐be interoperability testers are, therefore, likely to need to become DCC Users, or at least pass whatever entry criteria the DCC chooses to mandate in order to gain access to a DCC test environment.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Interchangeability testing</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>In this context, interchangeability means the ability for a given device to work with any other device on the same HAN, regardless of type, manufacturer, make, model or firmware version.</xhtml:p>
                            <xhtml:p>Most devices are installed by suppliers, so when a customer switches supplier, the new supplier may inherit devices that are unfamiliar. If one of those devices fails, it’s the new supplier’s responsibility to replace it, and this may well be with a different make and model. The replacement device must be compatible with the rest of the installed devices to avoid the expense of replacing the lot.</xhtml:p>
                            <xhtml:p>As with interoperability testing, interchangeability testing is new and likely to require a DCC test environment. Given the need to test devices with every other type of device, it’s also likely to require a very large and ever‐growing permanent collection of devices (a ‘device zoo’).</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Accelerated life testing</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Most smart devices have a life expectancy of at least 10 to 15 years. Given the cost of a device (not to mention the expense of a site visit to install it), a device must achieve a ripe old age, preferably shuffling off its mortal coil via a statutory meter change (replacement when its certification expires). Because many of the devices that will be rolled out are still on the drawing board, little evidence exists that they’ll achieve their dotage. As its name suggests, accelerated life testing aims to exercise a device far in excess of normal operating conditions, thus simulating the passage of time.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>End‐to‐end testing</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Though not specifically aimed at testing devices, devices will play an essential part in a DCC User’s end‐to‐end testing (in which the DCC User tests full operation of all their processes from their back office systems right through to the customer).</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:h2>Mandatory or Optional Testing?</xhtml:h2>
                    <xhtml:p>The following table summarises the eight types of device testing and the device types to which they apply (‘M’ means ‘Mandatory’; ‘O’ means ‘Optional’).</xhtml:p>
                    <xhtml:table xhtml:style="width: 100%;margin-left: 0;margin-right: auto;mc-table-style: url('../../Resources/TableStyles/TabMenuMap.css');" xhtml:class="TableStyle-TabMenuMap" xhtml:cellspacing="0">
                        <xhtml:col xhtml:class="TableStyle-TabMenuMap-Column-Column1" />
                        <xhtml:col xhtml:class="TableStyle-TabMenuMap-Column-Column1" />
                        <xhtml:col xhtml:class="TableStyle-TabMenuMap-Column-Column1" />
                        <xhtml:col xhtml:class="TableStyle-TabMenuMap-Column-Column1" />
                        <xhtml:col xhtml:class="TableStyle-TabMenuMap-Column-Column1" />
                        <xhtml:col xhtml:class="TableStyle-TabMenuMap-Column-Column1" />
                        <xhtml:col xhtml:class="TableStyle-TabMenuMap-Column-Column1" />
                        <xhtml:thead>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Head-Header1">
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadD-Column1-Header1" xhtml:colspan="7">SMETS2 Device Testing</xhtml:th>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Head-Header1">
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Type of Testing</xhtml:th>
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadE-Column1-Header1">ESME</xhtml:th>
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadE-Column1-Header1">GSME</xhtml:th>
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadE-Column1-Header1">CH</xhtml:th>
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadE-Column1-Header1">PPMID</xhtml:th>
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadE-Column1-Header1">HCALCS</xhtml:th>
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadD-Column1-Header1">IHD / CAD</xhtml:th>
                            </xhtml:tr>
                        </xhtml:thead>
                        <xhtml:tbody>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Metrology</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">&#160;</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">&#160;</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">&#160;</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">&#160;</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Protocol (ZigBee)</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">M</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Protocol (DLMS/COSEM)</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">&#160;</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">&#160;</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">&#160;</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">&#160;</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">&#160;</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Protocol (GBCS)</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">M</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Security (CPA)</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">&#160;</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">&#160;</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Functional</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">M</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">M</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Interoperability</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">O</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Interchangeability</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">O</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Accelerated life</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">O</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyB-Column1-Body1">End‐to‐end</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyB-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyB-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyB-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyB-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyB-Column1-Body1">O</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyA-Column1-Body1">O</xhtml:td>
                            </xhtml:tr>
                        </xhtml:tbody>
                    </xhtml:table>
                    <xhtml:p>Just because a device has been certified doesn’t mean that it won’t need to be re‐tested. Smart devices these days comprise both hardware and firmware, the latter being upgradeable remotely. Depending on the extent of the change, a new version of firmware may require a new set of testing. And don’t forget that the life span of some certificates is less than the anticipated life of the device (for example, CPA certificates for a given product must be renewed every six years).</xhtml:p>
                    <xhtml:p>You can update the firmware in an ESME or GSME remotely via the DCC. The process is as follows:</xhtml:p>
                    <xhtml:ol>
                        <xhtml:li>
                            <xhtml:p>Distribute the new firmware in bulk. (To multiple devices in a single command – the only bulk Service Request that the DCC supports.)</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Activate the firmware in each device individually using a different Service Request.</xhtml:p>
                        </xhtml:li>
                    </xhtml:ol>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>