﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>Scrum Master</MicroContentPhrase>
        <MicroContentPhrase>iteration manager</MicroContentPhrase>
        <MicroContentPhrase>agile coach</MicroContentPhrase>
        <MicroContentPhrase>team coach</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>DevOps - Scrum Master</xhtml:h1>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                    <xhtml:p>The scrum master is the team role responsible for ensuring the team lives agile values and principles and follows the processes and practices that the team agreed they would use.</xhtml:p>
                    <xhtml:p>The responsibilities of this role include:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>
                            <xhtml:p>Clearing obstacles.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Establishing an environment where the team can be effective.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Addressing team dynamics.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Ensuring a good relationship between the team and product owner as well as others outside the team.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Protecting the team from outside interruptions and distractions.</xhtml:p>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>The scrum master role was created as part of the Scrum framework.  The name was initially intended to indicate someone who is an expert at Scrum and can therefore coach others.</xhtml:p>
                    <xhtml:p>The role does not generally have any actual authority. People filling this role have to lead from a position of influence, often taking a servant-leadership stance.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Also Known As</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Scrum master is typically the term used to refer to this role, even by teams not explicitly following Scrum.  Other terms used infrequently include iteration manager, agile coach, or team coach.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The general benefit expected from having a scrum master on a team is providing a self-organizing team with ongoing access to someone who has used agile, and scrum in particular in another setting and can help the team figure out the best way to apply it in their situation.</xhtml:p>
                            <xhtml:p>Another expected benefit is that the scrum master is someone that can address distractions, disruptions, and obstacles so that the remainder of the team is free to focus on the work of producing output that will generate the desired outcome.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>While having a scrum master can provide several benefits, there are also several problems that arise from improper application of the role.  Those problems include:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Assuming that you can just slide project managers who are used to command and control type leadership into a scrum master role and expect them to be effective.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Asking someone to fill the scrum master role without any experience working in an agile setting.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Expecting the work load for a scrum master to be the same on every team irrespective of how long the team has worked together, their understanding of agile values and principles, and their experience in the domain.  A well functioning team will most likely need much less coaching from a scrum master than a team new to working with each other and in agile values and principles.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Skill Levels</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>While there are no officially defined levels of skill for scrum masters, in practice there are different levels of experience:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Rotating scrum master – members of a team rotate scrum master responsibilities (primarily the administrative ones) amongst each other on a sprint by sprint basis.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Part time scrum master – one individual on the team takes on scrum master responsibilities in addition to other responsibilities on the same team.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Full time dedicated scrum master – one individual’s sole responsibility is as a scrum master for one time.  This model is best suited for a team learning agile.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Full time scrum master with more than one team – this model is quite frequently applied where an individual’s sole responsibility is to be a scrum master, but they work with more than one team.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Agile Coach – An individual does not have a specifically assigned team but works with several teams and only on specific needs.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p><xhtml:a xhtml:href="http://www.scrumguides.org/scrum-guide.html#team-sm" xhtml:target="_blank">Scrum Master definition in the Scrum Guide</xhtml:a>
                                    </xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>