﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>Spill</MicroContentPhrase>
        <MicroContentPhrase>Feed-in Tariff</MicroContentPhrase>
        <MicroContentPhrase>FIT</MicroContentPhrase>
        <MicroContentPhrase>FITS</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>Energy Industry: Spill</xhtml:h1>
                    <xhtml:p>In addition to measuring consumption, smart electricity meters are capable of recording electricity exported to the distribution grid.</xhtml:p>
                    <xhtml:p>Under the government’s Feed-in Tariff scheme (FIT), consumers who install their own generation (usually in the form of solar panels) get paid for every kWh they generate and a much smaller amount for every kWh they don’t use and, therefore, ‘spill’ onto the grid. Because traditional meters don’t record export, export suppliers currently assume spill to be 50 per cent of generation.</xhtml:p>
                    <xhtml:p>Smart meters now mean that suppliers can measure spill. However, given that an average 4kW array will generate between 10 and 15kWh per day (300 to 450kWh per month), a 50 per cent spill at £0.04/kWh equates to an average monthly payment of around £7.50 for spilt generation. If metering were to show that the customer is only spilling 40 per cent rather than the estimated 50 per cent, the export supplier would save £1.50 per month, which would more than offset the fixed monthly ES charge per meter of 3p.</xhtml:p>
                    <xhtml:p>However, on a sunny day, an empty household (with the occupants out at work or school) could easily spill three quarters of its generation, costing the ES an additional £3.75 per month for the privilege of metering, rather than estimating, the export... Given this dubious business case, it’s unlikely we’ll end up with many ESs unless the government chooses to mandate the use of smart meters for measuring export.</xhtml:p>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>