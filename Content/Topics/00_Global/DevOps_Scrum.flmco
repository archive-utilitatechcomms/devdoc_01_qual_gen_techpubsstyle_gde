<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>Scrum</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>DevOps - Scrum</xhtml:h1>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                    <xhtml:p>Scrum is a process framework used to manage product development and other knowledge work.  Scrum is empirical in that it provides a means for teams to establish a hypothesis of how they think something works, try it out, reflect on the experience, and make the appropriate adjustments.  That is, when the framework is used properly.</xhtml:p>
                    <xhtml:p>Scrum is structured in a way that allows teams to incorporate practices from other frameworks where they make sense for the team’s context.</xhtml:p>
                    <xhtml:p>Scrum’s primary contribution to the software development world is a simple, but effective approach to managing the work of a small collaborative team involved in product development. It provides a framework and set of simple rules that allow an appropriate amount of planning, control over the work, and risk identification and mitigation and issue identification and resolution.</xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <xhtml:a xhtml:href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>When Applicable</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Scrum is best suited in the case where a cross functional team is working in a product development setting where there is a non trivial amount of work that lends itself to being split into more than one 2 – 4 week iteration.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Values</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Teams following scrum are expected to learn and explore the following values:</xhtml:p>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Commitment</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>Team members personally commit to achieving team goals</xhtml:p>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Courage</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>Team members do the right thing and work on tough problems.</xhtml:p>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Focus</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>Concentrate on the work identified for the sprint and the goals of the team.</xhtml:p>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Openness</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>Team members and stakeholders are open about all the work and the challenges the team encounters.</xhtml:p>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Respect</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>Team members respect each other to be capable and independent.</xhtml:p>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Principles</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The following principles underpin the empirical nature of scrum:</xhtml:p>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Transparency</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>The team must work in an environment where everyone is aware of what issues other team members are running into. Teams surface issues within the organisation, often ones that have been there for a long time, that get in the way of the team’s success.</xhtml:p>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Inspection</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>Frequent inspection points built into the framework to allow the team an opportunity to reflect on how the process is working. These inspection points include the Daily Scrum meeting and the Sprint Review Meeting.</xhtml:p>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Adaptation</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>The team constantly investigates how things are going and revises those items that do not seem to make sense.</xhtml:p>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Practices</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Events</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>Sprint</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>The Sprint is a timebox of one month or less during which the team produces a potentially shippable product Increment. Typical characteristics of Sprints:</xhtml:p>
                                            <xhtml:ul>
                                                <xhtml:li>
                                                    <xhtml:p>Maintain a consistent duration throughout a development effort.</xhtml:p>
                                                </xhtml:li>
                                                <xhtml:li>
                                                    <xhtml:p>A new Sprint immediately follows the conclusion of the previous Sprint.</xhtml:p>
                                                </xhtml:li>
                                                <xhtml:li>
                                                    <xhtml:p>Start date and end date of Sprint are fixed.</xhtml:p>
                                                </xhtml:li>
                                            </xhtml:ul>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>Sprint Planning</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>A team starts out a Sprint with a discussion to determine which items from the product backlog they will work on during the Sprint.  The end result of <xhtml:a xhtml:href="DevOps_SprintPlanning.htm" xhtml:target="_blank" xhtml:title="Sprint Planning" xhtml:alt="Sprint Planning">Sprint Planning</xhtml:a> is the <xhtml:a xhtml:href="DevOps_SprintBacklog.htm" xhtml:target="_blank" xhtml:title="Sprint Backlog" xhtml:alt="Sprint Backlog">Sprint Backlog</xhtml:a>.</xhtml:p>
                                            <xhtml:p>Sprint Planning typically occurs in two parts. In the first part, the <xhtml:a xhtml:href="DevOps_ProductOwner.htm" xhtml:target="_blank" xhtml:title="Product Owner" xhtml:alt="Product Owner">product owner</xhtml:a> and the rest of the team agree on which product backlog items will be included in the Sprint.</xhtml:p>
                                            <xhtml:p>In the Second Part of Sprint Planning, the team determines how they will successfully deliver the identified product backlog items as part of the potentially shippable product increment.  The team may identify specific tasks necessary to make that happen if that is one of their practices.  The product backlog items identified for delivery and tasks if applicable, makes up the Sprint Backlog.</xhtml:p>
                                            <xhtml:p>Once the team and product owner establish the scope of the Sprint as described by the product backlog items no more items can be added to the Sprint Backlog. This protects the team from scope changes within that Sprint.</xhtml:p>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>Daily Scrum</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>The <xhtml:a xhtml:href="DevOps_DailyMeeting.htm" xhtml:target="_blank" xhtml:title="Daily Scrum" xhtml:alt="Daily Scrum">Daily Scrum</xhtml:a> is a short (usually limited to 15 minutes) discussion where the team coordinates their activities for the following day. The Daily Scrum is not intended to be a status reporting meeting or a problem solving discussion.</xhtml:p>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>Sprint Review</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>At the end of the Sprint, the entire team (including product owner) reviews the results of the sprint with stakeholders of the product. The purpose of this discussion is to discuss, demonstrate, and potentially give the stakeholders a chance to use, the increment in order to get feedback. The Sprint Review is not intended to provide a status report.  Feedback from the sprint review gets placed into the Product Backlog for future consideration.</xhtml:p>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>Sprint Retrospective</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>At the end of the Sprint following the sprint review the team (including product owner) should reflect upon how things went during the previous sprint and identify adjustments they could make going forward. The result of this <xhtml:a xhtml:href="DevOps_Retrospective.htm" xhtml:target="_blank" xhtml:title="Retrospective" xhtml:alt="Retrospective">retrospective</xhtml:a> is at least one action item included on the following Sprint’s Sprint Backlog.</xhtml:p>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Artifacts</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>Product Backlog</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>The <xhtml:a xhtml:href="DevOps_ProductBacklog.htm" xhtml:target="_blank" xhtml:title="Product Backlog" xhtml:alt="Product Backlog">product backlog</xhtml:a> is an ordered list of all the possible changes that could be made to the product.  Items on the product backlog are options, not commitments in that just because they exist on the Product Backlog does not guarantee they will be delivered.</xhtml:p>
                                            <xhtml:p>The <xhtml:a xhtml:href="DevOps_ProductOwner.htm" xhtml:target="_blank" xhtml:title="Product Owner" xhtml:alt="Product Owner">Product Owner</xhtml:a><xhtml:a xhtml:href="DevOps_BacklogRefinement.htm" xhtml:target="_blank" xhtml:title="Backlog Refinement" xhtml:alt="Backlog Refinement">maintains the product backlog</xhtml:a> on an ongoing basis including its content, availability, and ordering.</xhtml:p>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>Sprint Backlog</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>The Sprint Backlog is the collection of product backlog items selected for delivery in the Sprint, and if the team identifies tasks, the tasks necessary to deliver those product backlog items and achieve the Sprint Goal.</xhtml:p>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>Increment</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>The increment is the collection of the Product Backlog Items that meet the team’s Definition of Done by the end of the Sprint.  The Product Owner may decide to release the increment or build upon it in future Sprints.</xhtml:p>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>Definition of Done</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>The <xhtml:a xhtml:href="DevOps_DefinitionOfDone.htm" xhtml:target="_blank" xhtml:title="Definition of Done" xhtml:alt="Definition of Done">definition of done</xhtml:a> is a team’s shared agreement on the criteria that a Product Backlog Item must meet before it is considered done.</xhtml:p>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Roles</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>The Product Owner</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>The <xhtml:a xhtml:href="DevOps_ProductOwner.htm" xhtml:target="_blank" xhtml:title="Product Owner" xhtml:alt="Product Owner">product owner</xhtml:a> is a role team responsible for managing the product backlog in order to achieve the desired outcome that the team seeks to accomplish.</xhtml:p>
                                            <xhtml:p>The product owner role exists in Scrum to address challenges that product development teams had with multiple, conflicting direction, or no direction at all with respect to what to build.</xhtml:p>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>The Scrum Master</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>The <xhtml:a xhtml:href="DevOps_ScrumMaster.htm" xhtml:target="_blank" xhtml:title="Scrum Master" xhtml:alt="Scrum Master">scrum master</xhtml:a> is the team role responsible for ensuring the team lives agile values and principles and follows the processes and practices that the team agreed they would use.</xhtml:p>
                                            <xhtml:p>The name was initially intended to indicate someone who is an expert at Scrum and can therefore coach others.</xhtml:p>
                                            <xhtml:p>The role does not generally have any actual authority. People filling this role have to lead from a position of influence, often taking a servant-leadership stance.</xhtml:p>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <MadCap:dropDown>
                                        <MadCap:dropDownHead>
                                            <MadCap:dropDownHotspot>The Development Team</MadCap:dropDownHotspot>
                                        </MadCap:dropDownHead>
                                        <MadCap:dropDownBody>
                                            <xhtml:p>The <xhtml:a xhtml:href="DevOps_Team.htm" xhtml:target="_blank" xhtml:title="Development Team" xhtml:alt="Development Team">development team</xhtml:a> consists of the people who deliver the product increment inside a Sprint.</xhtml:p>
                                            <xhtml:p>The main responsibility of the development team is to deliver the increment that delivers value every Sprint. How the work is divided up to do that is left up to the team to determine based on the conditions at that time.</xhtml:p>
                                            <xhtml:p>&#160;</xhtml:p>
                                        </MadCap:dropDownBody>
                                    </MadCap:dropDown>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Lifecycle</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>Scrum is a framework that allows development teams flexibility to respond to changing situations. This framework has sufficient control points in place to ensure the team does not stray from the desired outcome, and that issues can be identified and resolved and process adjustments made while the effort is still underway.</xhtml:p>
                                    <xhtml:p>The Scrum Lifecycle starts with a prioritized backlog, but does not provide any guidance as to how that backlog is developed or prioritized.</xhtml:p>
                                    <xhtml:p>The Scrum Lifecycle consists of a series of Sprints, where the end result is a potentially shippable product increment. Inside of these sprints, all of the activities necessary for the development of the product occur on a small subset of the overall product.  Below is a description of the key steps in the Scrum Lifecycle:</xhtml:p>
                                    <xhtml:ol>
                                        <xhtml:li>
                                            <xhtml:p>Establish the Product Backlog.</xhtml:p>
                                        </xhtml:li>
                                        <xhtml:li>
                                            <xhtml:p>The product owner and development team conduct Sprint Planning. Determine the scope of the Sprint in the first part of Sprint Planning and the plan for delivering that scope in the second half of Sprint Planning.</xhtml:p>
                                        </xhtml:li>
                                        <xhtml:li>
                                            <xhtml:p>As the Sprint progresses, development team perform the work necessary to deliver the selected product backlog items.</xhtml:p>
                                        </xhtml:li>
                                        <xhtml:li>
                                            <xhtml:p>On a daily basis, the development team coordinate their work in a Daily Scrum.</xhtml:p>
                                        </xhtml:li>
                                        <xhtml:li>
                                            <xhtml:p>At the end of the Sprint the development team delivers the Product Backlog Items selected during Sprint Planning. The development team holds a Sprint Review to show the customer the increment and get feedback.  The development team and product owner also reflect on how the Sprint has proceeded so far and adapting their processes accordingly during a retrospective.</xhtml:p>
                                        </xhtml:li>
                                        <xhtml:li>
                                            <xhtml:p>The Team repeats steps 2–5 until the desired outcome of the product have been met.</xhtml:p>
                                        </xhtml:li>
                                    </xhtml:ol>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>1986: Takeuchi and Nonaka publish their article ”<xhtml:a xhtml:href="http://www.enterprisescrum.com/publications/The New New Product Development Game - Nonaka and Takeuchi.pdf" xhtml:target="_blank">The New New Product Development Game</xhtml:a>” in Harvard Business Review. The article describes a rugby approach where “the product development process emerges from the constant interaction of a hand-picked, multidisciplinary team whose members work together from start to finish.” This article is often cited as the inspiration for the Scrum framework.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>1993: Jeff Sutherland invents Scrum as a process at Easel Corporation.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>1995: Ken Schwaber and Jeff Sutherland co-present Scrum at the OOPSLA Conference.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>You can find more information about Agile and Scrum in <xhtml:a xhtml:href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p><xhtml:a xhtml:href="http://www.scrumguides.org/" xhtml:target="_blank">Scrum Guide</xhtml:a>
                                    </xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p><xhtml:a xhtml:href="http://amzn.to/2nYGSUy" xhtml:target="_blank" xhtml:title="Agile Software Development with Scrum by Ken Schwaber and Mike Beedle" xhtml:alt="Agile Software Development with Scrum by Ken Schwaber and Mike Beedle">Agile Software Development with Scrum</xhtml:a> by Ken Schwaber and Mike Beedle</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p><xhtml:a xhtml:href="http://amzn.to/2nKWp9X" xhtml:target="_blank" xhtml:title="Agile Project Management with Scrum by Ken Schwaber" xhtml:alt="Agile Project Management with Scrum by Ken Schwaber">Agile Project Management</xhtml:a> with Scrum by Ken Schwaber</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p><xhtml:a xhtml:href="https://www.scrumalliance.org/why-scrum" xhtml:target="_blank">Scrum Alliance</xhtml:a>
                                    </xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>