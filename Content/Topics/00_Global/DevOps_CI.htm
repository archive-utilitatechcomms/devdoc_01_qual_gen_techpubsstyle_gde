﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>DevOps - Continuous Integration (CI)</h1>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" target="_blank" title="Development Resources: IT Project Management" alt="Development Resources: IT Project Management">Development Resources: IT Project Management</a>.</p>
        <p>Teams practising continuous <a href="DevOps_Integration.htm" target="_blank" title="Integration" alt="Integration">integration</a> seek two objectives:</p>
        <ul>
            <li>
                <p>Minimise the duration and effort required by each integration episode.</p>
            </li>
            <li>
                <p>Be able to deliver a product version suitable for release at any moment.</p>
            </li>
        </ul>
        <p>In practice, this dual objective requires an integration procedure which is reproducible at the very least, and largely automated. This is achieved through <a href="DevOps_VersionControl.htm" target="_blank" title="Version Control" alt="Version Control">version control</a> tools, team policies and conventions, and tools specifically designed to help achieve continuous integration.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>As suggested above, the practice of continuous integration should not be confused with the tools that assist it (CI servers such as Cruise Control, Hudson, etc.). Continuous integration is first and foremost a matter of attitude rather than tools, and it relies on more than one kind of tool: tools for testing, tools for automating build processes, and tools for version control.</p>
                    </li>
                    <li>
                        <p>Continuous integration aims to lessen the pain of integration by increasing its frequency. Therefore, “any” effort related to producing intermediate releases, and which the team experiences as particularly burdensome, is a candidate for inclusion in the team’s continuous integration process. (This is the reasoning that leads teams to <a href="DevOps_ContinuousDeployment.htm" target="_blank" title="Continuous Deployment" alt="Continuous Deployment">continuous deployment</a>.)</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>1993: the phrase “continuous integration” is already in use and thus predates what will later be known as Agile processes, for instance an article contrasts it with “scheduled” integration, and recommends the latter, citing “lack of thorough testing” as one issue with continuous integration; this helps explain why the automated testing favored by Agile teams is an enabler for continuous integration.</p>
                    </li>
                    <li>
                        <p>1996: Steve McConnell <a href="https://stevemcconnell.com/articles/daily-build-and-smoke-test/" target="_blank">describes</a> the “Daily Build and Smoke Test” technique, used at Microsoft for Windows NT 3.0 during the 1990’s; the emphasis is not so much on the automation as on the frequency, the daily cycle being at that time considered “extreme”.</p>
                    </li>
                    <li>
                        <p>1998: continuous integration is listed among the core practices of Extreme Programming.</p>
                    </li>
                    <li>
                        <p>2000: an article by Martin Fowler provides perhaps the <a href="https://www.martinfowler.com/articles/originalContinuousIntegration.html" target="_blank">most complete description</a> of the continuous integration practice available at that time.</p>
                    </li>
                    <li>
                        <p>2001: <a href="http://cruisecontrol.sourceforge.net/" target="_blank">Cruise Control</a>, the first “continuous integration server”, is published under an open source license; it automates monitoring of the source code repository, triggering the build and test process, notifications of the results to the developers, and archival of the test reports; the period 2001-2007 sees a large number of similar tools appear, leading perhaps to an excessive focus on tools over practice.</p>
                    </li>
                    <li>
                        <p>2004: an <a href="http://www.developertesting.com/archives/month200404/20040401-eXtremeFeedbackForSoftwareDevelopment.html" target="_blank">article</a> by Alberto Savoia proposes “Extreme Feedback Devices” such as lava lamps or dedicated monitors, to display the results of the most recent integration, an important innovation in CI.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Signs of Use</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>For most teams, continuous integration in practice amounts to the following:</p>
                <ul>
                    <li>
                        <p>Use of a version control tool (CVS, SVN, Git, etc.).</p>
                    </li>
                    <li>
                        <p>An <a href="DevOps_AutomatedBuild.htm" target="_blank" title="Automated Build" alt="Automated Build">automated build</a> and product release process.</p>
                    </li>
                    <li>
                        <p>Instrumentation of the build process to trigger <a href="DevOps_UnitTesting.htm" target="_blank" title="Unit Testing" alt="Unit Testing">unit</a> and <a href="DevOps_AcceptanceTests.htm" target="_blank" title="Acceptance Tests" alt="Acceptance Tests">acceptance tests</a> “every time any change is published to version control”.</p>
                    </li>
                    <li>
                        <p>In the event of even a single test failing, alerting the team of a “broken build” so that the team can reach a stable, releasable baseline again soonest.</p>
                    </li>
                    <li>
                        <p>Optionally, the use of a tool such as a continuous integration server, which automates the process of integration, testing and reporting of test results.</p>
                    </li>
                </ul>
                <p>Look for a dedicated display in a prominent spot of the <a href="DevOps_TeamRoom.htm" target="_blank" title="Team Room" alt="Team Room">team room</a>: this can be a normal monitor, an LED display or even a lava lamp, with the sole purpose of reporting on the result of the most recent integration.</p>
                <p>Observe also how the team responds to a broken build, suggesting that a defect may have been detected. If the team is aware of defects, but tolerates them or continues working on a product that isn’t in a releasable state, the term continuous integration no longer applies, irrespective of tooling!</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p><a href="http://www.martinfowler.com/articles/continuousIntegration.html" target="_blank" title="Continuous Integration, Martin Fowler (2006)" alt="Continuous Integration, Martin Fowler (2006)">Continuous Integration</a>, Martin Fowler (2006)</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>