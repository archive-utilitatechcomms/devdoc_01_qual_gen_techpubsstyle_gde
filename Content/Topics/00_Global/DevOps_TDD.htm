﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>DevOps - Test-Driven Development (TDD)</h1>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" target="_blank" title="Development Resources: IT Project Management" alt="Development Resources: IT Project Management">Development Resources: IT Project Management</a>.</p>
        <p>“Test-driven development” refers to a style of programming in which three activities are tightly interwoven: coding, testing (in the form of writing <a href="DevOps_UnitTesting.htm" target="_blank">unit tests</a>) and design (in the form of <a href="DevOps_Refactoring.htm" target="_blank">refactoring</a>).</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Rules</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>It can be succinctly described by the following set of rules:</p>
                <ul>
                    <li>
                        <p>Write a “single” unit test describing an aspect of the program.</p>
                    </li>
                    <li>
                        <p>Run the test, which should fail because the program lacks that feature.</p>
                    </li>
                    <li>
                        <p>Write “just enough” code, the simplest possible, to make the test pass.</p>
                    </li>
                    <li>
                        <p>“Refactor” the code until it conforms to the <a href="DevOps_RuleSimplicity.htm" target="_blank">simplicity criteria</a>.</p>
                    </li>
                    <li>
                        <p>Repeat, “accumulating” unit tests over time.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>Many teams report significant reductions in defect rates, at the cost of a moderate increase in initial development effort.</p>
                    </li>
                    <li>
                        <p>The same teams tend to report that these overheads are more than offset by a reduction in effort in projects’ final phases.</p>
                    </li>
                    <li>
                        <p>Although empirical research has so far failed to confirm this, veteran practitioners report that TDD leads to improved design qualities in the code, and more generally a higher degree of “internal” or technical quality, for instance improving the metrics of cohesion and coupling.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Typical individual mistakes include:</p>
                <ul>
                    <li>
                        <p>forgetting to run tests frequently</p>
                    </li>
                    <li>
                        <p>writing too many tests at once</p>
                    </li>
                    <li>
                        <p>writing tests that are too large or coarse-grained</p>
                    </li>
                    <li>
                        <p>writing overly trivial tests, for instance omitting assertions</p>
                    </li>
                    <li>
                        <p>writing tests for trivial code, for instance accessors</p>
                    </li>
                </ul>
                <p>Typical team pitfalls include:</p>
                <ul>
                    <li>
                        <p>partial adoption – only a few developers on the team use TDD</p>
                    </li>
                    <li>
                        <p>poor maintenance of the test suite – most commonly leading to a test suite with a prohibitively long running time</p>
                    </li>
                    <li>
                        <p>abandoned test suite (i.e. seldom or never run) – sometimes as a result of poor maintenance, sometimes as a result of team turnover</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>While the idea of having test elaboration precede programming is not original to the Agile community, TDD constitutes a breakthrough insofar as it combines that idea with that of “developer testing”, providing developer testing with renewed respectability.</p>
                <ul>
                    <li>
                        <p>1976: publication of “<a href="http://www.amazon.fr/dp/0471627658" target="_blank">Software Reliability</a>” by Glenford Myers, which states as an “axiom” that “a developer should never test their own code” (Dark Ages of Developer Testing)</p>
                    </li>
                </ul>
                <ul>
                    <li>
                        <p>1990: testing discipline dominated by “black box” techniques, in particular in the form of “capture and replay” testing tools</p>
                    </li>
                    <li>
                        <p>1991: independent creation of a testing framework at Taligent with striking similarities to SUnit (<a href="http://shebanator.com/2007/08/21/a-brief-history-of-test-frameworks/" target="_blank">source</a>)</p>
                    </li>
                    <li>
                        <p>1994: Kent Beck writes the SUnit testing framework for Smalltalk (<a href="http://www.macqueen.us/smalltalkReport/ST/91_95/SMAL0402.PDF" target="_blank">source</a>)</p>
                    </li>
                    <li>
                        <p>1998: article on <a href="DevOps_ExtremeProgramming.htm" target="_blank">Extreme Programming</a> mentions that “we usually write the test first” (<a href="http://www.xprogramming.com/publications/dc9810cs.pdf" target="_blank">source</a>)</p>
                    </li>
                    <li>
                        <p>1998 to 2002: “Test First” is elaborated into “Test Driven”, in particular on the <a href="http://c2.com/cgi/wiki?TestDrivenDevelopment" target="_blank">C2.com</a> Wiki</p>
                    </li>
                    <li>
                        <p>2000: <a href="DevOps_MockObjects.htm" target="_blank">Mock Objects</a> are among the novel techniques developed during that period (<a href="http://www.mockobjects.com/files/endotesting.pdf" target="_blank">source</a>)</p>
                    </li>
                    <li>
                        <p>2003: publication of “<a href="http://www.amazon.fr/dp/0321146530" target="_blank">Test Driven Development: By Example</a>” by Kent Beck</p>
                    </li>
                </ul>
                <p>By 2006 TDD is a relatively mature discipline which has started encouraging further innovations derived from it, such as <a href="DevOps_ATDD.htm" target="_blank">ATDD</a> or <a href="DevOps_BDD.htm" target="_blank">BDD</a>).</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Signs of Use</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>“<a href="https://en.wikipedia.org/wiki/Code_coverage" target="_blank">Code coverage</a>” is a common approach to evidencing the use of TDD; while high coverage does not guarantee appropriate use of TDD, coverage below 80% is likely to indicate deficiencies in a team’s mastery of TDD.</p>
                    </li>
                    <li>
                        <p><a href="DevOps_VersionControl.htm" target="_blank">Version control</a> logs should show that test code is checked in each time product code is checked in, in roughly comparable amounts.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Skill Levels</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Beginner</p>
                <ul>
                    <li>
                        <p>able to write a unit test prior to writing the corresponding code</p>
                    </li>
                    <li>
                        <p>able to write code sufficient to make a failing test pass</p>
                    </li>
                </ul>
                <p>Intermediate</p>
                <ul>
                    <li>
                        <p>practices “test driven bug fixing”: when a defect is found, writes a test exposing the defect before correction</p>
                    </li>
                    <li>
                        <p>able to decompose a compound program feature into a sequence of several unit tests to be written</p>
                    </li>
                    <li>
                        <p>knows and can name a number of tactics to guide the writing of tests (for instance “when testing a recursive algorithm, first write a test for the recursion terminating case”)</p>
                    </li>
                    <li>
                        <p>able to factor out reusable elements from existing unit tests, yielding situation-specific testing tools</p>
                    </li>
                </ul>
                <p>Advanced</p>
                <ul>
                    <li>
                        <p>able to formulate a “roadmap” of planned unit tests for a macroscopic features (and to revise it as necessary)</p>
                    </li>
                    <li>
                        <p>able to “test drive” a variety of design paradigms: object-oriented, functional, event-drive</p>
                    </li>
                    <li>
                        <p>able to “test drive” a variety of technical domains: computation, user interfaces, persistent data access…</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
    </body>
</html>