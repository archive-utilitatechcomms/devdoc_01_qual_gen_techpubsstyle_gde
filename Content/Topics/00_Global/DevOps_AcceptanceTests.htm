﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>DevOps - Acceptance Tests</h1>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" target="_blank" title="Development Resources: IT Project Management" alt="Development Resources: IT Project Management">Development Resources: IT Project Management</a>.</p>
        <p>An acceptance test is a formal description of the behavior of a software product, generally expressed as an example or a usage scenario. A number of different notations and approaches have been proposed for such examples or scenarios. In many cases the aim is that it should be possible to automate the execution of such tests by a software tool, either ad-hoc to the development team or off the shelf.</p>
        <p>Similar to a <a href="DevOps_UnitTesting.htm" target="_blank" title="Unit Test" alt="Unit Test">unit test</a>, an acceptance test generally has a binary result, pass or fail. A failure suggests, though does not prove, the presence of a defect in the product.</p>
        <p>Teams mature in their practice of agile  use acceptance tests as the main form of functional specification and the only formal expression of business requirements. Other teams use acceptance tests as a complement to specification documents containing uses cases or more narrative text.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Also Known As</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The terms “functional test”, “acceptance test” and “customer test” are used more or less interchangeably. A more specific term “story test”, referring to <a href="DevOps_UserStories.htm" target="_blank" title="User Stories" alt="User Stories">user stories</a> is also used, as in the phrase “story test driven development”.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Acceptance testing has the following benefits, complementing those which can be obtained from <a href="DevOps_UnitTesting.htm" target="_blank" title="Unit Testing" alt="Unit Testing">unit tests</a>:</p>
                <ul>
                    <li>
                        <p>Encouraging closer collaboration between developers on the one hand and customers, users or domain experts on the other, as they entail that business requirements should be expressed.</p>
                    </li>
                    <li>
                        <p>Providing a clear and unambiguous “contract” between customers and developers; a product which passes acceptance tests will be considered adequate (though customers and developers might refine existing tests or suggest new ones as necessary).</p>
                    </li>
                    <li>
                        <p>Decreasing the chance and severity both of new defects and regressions (defects impairing functionality previously reviewed and declared acceptable).</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Customers and domain experts, the primary audience for acceptance tests, find acceptance tests that contain implementation details difficult to review and understand.  To prevent acceptance tests from being overly concerned with technical implementation, involve customers and/or domain experts in the creation and discussion of acceptance tests.  See <a href="DevOps_BDD.htm" target="_blank" title="Behaviour Driven Development" alt="Behaviour Driven Development">Behaviour Driven Development</a> for more information.</p>
                <p>Acceptance tests that are unduly focused on technical implementation also run a the risk of failing due to minor or cosmetic changes which in reality do not have any impact on the product’s behavior.  For example, if an acceptance test references the label for a text field, and that label changes, the acceptance test fails even though the actual functioning of the product is not impacted.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Potential Costs</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Unlike automated unit tests, automated acceptance tests are not universally viewed as a net benefit and some controversy has arisen after experts such as Jim Shore or Brian Marick questioned whether the following costs were outweighed by the benefits of the practice:</p>
                <ul>
                    <li>
                        <p>Many teams report that the creation of automated acceptance tests requires significant effort.</p>
                    </li>
                    <li>
                        <p>Sometimes due to the “fragile” test issue, teams find the maintenance of automated acceptance tests burdensome.</p>
                    </li>
                    <li>
                        <p>The first generation of tools in the Fit/FitNesse tradition resulted in acceptance tests that customers or domain experts could not understand..</p>
                    </li>
                </ul>
                <p>The <a href="DevOps_BDD.htm" target="_blank" title="Behavior Driven Development " alt="Behavior Driven Development ">BDD</a> approach may hold promise for a resolution of this controversy.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>1996: Automated tests identified as a practice of <a href="DevOps_ExtremeProgramming.htm" target="_blank" title="Extreme Programming" alt="Extreme Programming">Extreme Programming</a>, without much emphasis on the distinction between unit and acceptance testing, and with no particular notation or tool recommended.</p>
                    </li>
                    <li>
                        <p>2002: Ward Cunningham, one of the inventors of Extreme Programming, publishes Fit, a tool for acceptance testing based on a tabular, Excel-like notation.</p>
                    </li>
                    <li>
                        <p>2003: Bob Martin combines Fit with Wikis (another invention of Cunningham’s), creating FitNesse.</p>
                    </li>
                    <li>
                        <p>2003-2006: Fit/FitNesse combo eclipses most other tools and becomes the mainstream model for Agile acceptance testing.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>For a comprehensive survey, see <a href="https://ieeexplore.ieee.org/document/4599450" target="_blank" title="Automated Acceptance Testing: A Literature Review and an Industrial Case Study" alt="Automated Acceptance Testing: A Literature Review and an Industrial Case Study">Automated Acceptance Testing: A Literature Review and an Industrial Case Study</a>.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>