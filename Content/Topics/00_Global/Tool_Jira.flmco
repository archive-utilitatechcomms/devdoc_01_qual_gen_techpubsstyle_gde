﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="jira">
        <MicroContentPhrase>Jira</MicroContentPhrase>
        <MicroContentPhrase>Project Management</MicroContentPhrase>
        <MicroContentPhrase>Issue Tracking</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>Tool: Jira (Project Management)</xhtml:h1>
                    <xhtml:p><xhtml:a xhtml:href="https://www.atlassian.com/software/jira" xhtml:target="_blank" xhtml:title="Jira" xhtml:alt="Jira">Jira</xhtml:a> is a proprietary <xhtml:a xhtml:href="https://en.wikipedia.org/wiki/Issue_tracking_system" xhtml:target="_blank" xhtml:title="Issue Tracking" xhtml:alt="Issue Tracking">issue tracking</xhtml:a> product, developed by <xhtml:a xhtml:href="https://www.atlassian.com/" xhtml:target="_blank" xhtml:title="Atlassian" xhtml:alt="Atlassian">Atlassian</xhtml:a>. It provides <xhtml:a xhtml:href="https://en.wikipedia.org/wiki/Bug_tracking_system" xhtml:target="_blank" xhtml:title="Bug tracking system" xhtml:alt="Bug tracking system">bug tracking</xhtml:a>, issue tracking, and <xhtml:a xhtml:href="https://en.wikipedia.org/wiki/Project_management" xhtml:target="_blank" xhtml:title="Project management" xhtml:alt="Project management">project management</xhtml:a> functions. The product name is a truncation of Gojira, the original Japanese word that was anglicised to Godzilla, itself a reference to Jira's main competitor, Bugzilla.</xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">To access Utilita's IT projects go to Atlassian Jira <xhtml:a xhtml:href="https://utilita.atlassian.net/secure/BrowseProjects.jspa" xhtml:target="_blank" xhtml:title="Utilita's Projects in Jira" xhtml:alt="Utilita's Projects in Jira">here</xhtml:a>.</xhtml:p>
                    <xhtml:p>Jira is used for issue tracking and project management by over 75,000 customers in 122 countries around the globe. </xhtml:p>
                    <xhtml:ul>
                        <xhtml:li><xhtml:b>Jira Core</xhtml:b> is intended at generic project management.</xhtml:li>
                        <xhtml:li><xhtml:b>Jira Software</xhtml:b> includes the base software, including agile project management features (previously a separate product: Jira Agile).</xhtml:li>
                        <xhtml:li><xhtml:b>Jira Service Desk</xhtml:b> is intended for use by IT or business service desks.</xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>Jira is written in Java and uses the Pico inversion of control container, Apache OFBiz entity engine, and WebWork 1 technology stack. For remote procedure calls (RPC), Jira supports REST, SOAP, and XML-RPC. Jira integrates with source control programs such as Clearcase, Concurrent Versions System (CVS), Git, Mercurial, Perforce, Subversion, and Team Foundation Server.</xhtml:p>
                    <xhtml:p>The main features of Jira for agile software development are the functionality to plan development iterations, the iteration reports and the bug tracking functionality.</xhtml:p>
                    <xhtml:p>Jira supports the Networked Help Desk API for sharing customer support tickets with other issue tracking systems.</xhtml:p>
                    <xhtml:p>You can find further information in <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_DevTools_GDE/Content/Topics/2_Resource-DevTools/DevDoc_Resource-DevTools_02_Jira.htm" xhtml:title="Development Resources: Development Tools - Jira" xhtml:alt="Development Resources: Development Tools - Jira" xhtml:target="_blank">Development Resources: Development Tools - Jira</xhtml:a>.</xhtml:p>
                    <xhtml:p>&#160;</xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about the tools we use <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_DevTools_GDE/Content/Topics/1_Introduction/01_DevDoc_Resource-DevTools_00.htm" xhtml:target="_blank" xhtml:title="Development Resources: Tools" xhtml:alt="Development Resources: Tools">here</xhtml:a>.</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>