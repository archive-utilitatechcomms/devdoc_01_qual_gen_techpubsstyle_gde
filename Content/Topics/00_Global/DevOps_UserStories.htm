﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>DevOps - User Stories</h1>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" target="_blank" title="Development Resources: IT Project Management" alt="Development Resources: IT Project Management">Development Resources: IT Project Management</a>.</p>
        <p>In consultation with the customer or <a href="DevOps_ProductOwner.htm" target="_blank">product owner</a>, the team divides up the work to be done into functional increments called “user stories.”</p>
        <p>Each user story is expected to yield, once implemented, a contribution to the value of the overall product, irrespective of the order of implementation; these and other assumptions as to the nature of user stories are captured by the <a href="DevOps_INVEST.htm" target="_blank">INVEST</a> formula.</p>
        <p>To make these assumptions tangible, user stories are reified into a physical form: an index card or sticky note, on which a brief descriptive sentence is written to serve as a reminder of its value. This emphasizes the “atomic” nature of user stories and encourages <a href="https://en.wikipedia.org/wiki/Direct_manipulation_interface" target="_blank">direct physical manipulation</a>: for instance, decisions about scheduling are made by physically moving around these “story cards.”</p>
        <ul>
            <li>
                <p>A classic mistake consists, in teams where the Agile approach is confined to the development team or adopted after a non-Agile requirements phase, to start from a requirements document in narrative format and derive user stories directly based on its structure: for instance one story for each section of the document.</p>
            </li>
            <li>
                <p>As the 3 C’s model emphasises, a user story is not a document; the term encompasses all of the knowledge required to transform a version V of the product into version V’ which is more valuable with respect to a particular goal.</p>
            </li>
            <li>
                <p>The level of detail corresponding to a user story is not constant, it evolves over time as a function of the “planning horizon”; for instance a user story which is scheduled for the next iteration should be better understood than one which will not be implemented until the next release.</p>
            </li>
            <li>
                <p>A user story is not a Use Case; although it is often useful to compare and contrast the two notions, they are not equivalent and do not admit a one-to-one mapping.</p>
            </li>
            <li>
                <p>A user story does not in general correspond to a technical or user interface component: even though it may sometimes be a useful shortcut to talk about e.g. “the search dialog story”, screens, dialog boxes and buttons are not user stories.</p>
            </li>
        </ul>
        <p>For most Agile teams user stories are the main vehicle of incremental software delivery, and offer the following benefits:</p>
        <ul>
            <li>
                <p>Mitigating the risks of delayed feedback, all the more so.</p>
            </li>
            <li>
                <p>If the increments are small.</p>
            </li>
            <li>
                <p>If the software is released to production frequently.</p>
            </li>
            <li>
                <p>For the customer or product owner, the option to change their mind on the details or the schedule priority of any user story not yet implemented.</p>
            </li>
            <li>
                <p>For developers, being given clear and precise acceptance criteria, and ongoing feedback as they complete work.</p>
            </li>
            <li>
                <p>Promoting a clear separation of responsibilities between defining the “what” (province of the customer or product owner) and the “how” (province of the technical team), leveraging the skills and creativity of each.</p>
            </li>
        </ul>
        <p>Incremental development in general, and the “nano-incremental” strategy embodied in user stories in particular, has significant impacts on projects’ testing strategies, and in particular all but mandates significant test automation.</p>
        <p>This follows from the so-called “<a href="http://www.hans-eric.com/wp-content/uploads/2011/05/Incremental-development-and-regression-testing.pdf" target="_blank">quadratic growth</a>” problem: after implementation of feature F1 it is normal to test that feature. After implementation of feature F2, the risk of regression dictates re-testing F1 as well as testing F2. A test sequence assuming incremental development therefore goes: F1,F1+F2,F1+F2+F3, etc. – this grows as the square of the number of features, and can therefore quickly become unmanageable as projects grow in size. Test automation (in particular <a href="DevOps_UnitTesting.htm" target="_blank">unit</a> and <a href="DevOps_AcceptanceTests.htm" target="_blank">acceptance</a> testing) mitigates this effect, although it does come at a cost.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>User Stories originate with <a href="DevOps_ExtremeProgramming.htm" target="_blank">Extreme Programming</a>, their first written description in 1998 only claims that customers define project scope “with user stories, which are like use cases”. Rather than offered as a distinct practice, they are described as one of the “game pieces” used in the “planning game”.</p>
                <p>However, most of the thrust of further writing centers around all the ways user stories are “unlike” use cases, in trying to answer in a more practical manner “how requirements are handled” in Extreme Programming (and more generally Agile) projects. This drives the emergence, over the years, of a more sophisticated account of user stories.</p>
                <ul>
                    <li>
                        <p>cf. the <a href="https://www.agilealliance.org/glossary/role-feature/" target="_blank">Role-feature-benefit template</a>, 2001</p>
                    </li>
                    <li>
                        <p>cf. the <a href="https://www.agilealliance.org/glossary/three-cs/" target="_blank">3 C’s model</a>, 2001</p>
                    </li>
                    <li>
                        <p>cf. the <a href="https://www.agilealliance.org/glossary/invest/" target="_blank">INVEST checklist</a>, 2003</p>
                    </li>
                    <li>
                        <p>cf. the <a href="https://www.agilealliance.org/glossary/gwt/" target="_blank">Given-When-Then template</a>, 2006</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Signs Of Use</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>The team uses visual planning tools (release plan, story map, task board) and index cards or stickies on these displays reflect product features.</p>
                    </li>
                    <li>
                        <p>The labels on cards that stand for user stories contain few or no references to technical elements (“database”, “screen” or “dialog”) but generally refer to end users’ goals.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Skill Levels</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>An Agile team will benefit if the skills that underpin effective use of user stories are widely distributed among the team; however, it is likely that people with background in “requirements analysis” or with a history in “analyst” roles will have a leg up in acquiring these skills.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>As an individual contributor:</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Beginner</p>
                        <ul>
                            <li>
                                <p>able to start from another formalism for requirements (narrative document, use cases) and transpose that to user stores</p>
                            </li>
                            <li>
                                <p>knows at least one standard format for expressing user stories</p>
                            </li>
                            <li>
                                <p>able to illustrate a user story with an example (including user’s goal, existing context, user’s actions and expected outcomes)</p>
                            </li>
                        </ul>
                        <p>Intermediate</p>
                        <ul>
                            <li>
                                <p>able to divide up the functional goals of a development effort into user stories (possibly epics), ensuring no gaps are left</p>
                            </li>
                            <li>
                                <p>knows several formats for expressing user stories and can choose the most appropriate one</p>
                            </li>
                            <li>
                                <p>able to express the acceptance criteria for a user story in terms that will be directly usable as an automated acceptance test</p>
                            </li>
                            <li>
                                <p>knows or can identify the relevant user roles and populations and refers to them appropriately in user stories</p>
                            </li>
                            <li>
                                <p>can assess a user story using the INVEST checklist or an equivalent, and rephrase or split the user story as necessary</p>
                            </li>
                        </ul>
                        <p>Advanced</p>
                        <ul>
                            <li>
                                <p>able to interpret so-called “non functional requirements” in terms of user stories</p>
                            </li>
                            <li>
                                <p>able to link user stories to higher level descriptions of project goals (e.g. project charter) and to provide grounds for the relevance and business value of each user story</p>
                            </li>
                        </ul>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Collectively, as a team:</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Beginner</p>
                        <ul>
                            <li>
                                <p>the team organizes project activities around user stories</p>
                            </li>
                            <li>
                                <p>the team is able to obtain, “just in time”, the information necessary to implement user stories, for instance by having access to the product owner or domain expert</p>
                            </li>
                        </ul>
                        <p>Intermediate</p>
                        <ul>
                            <li>
                                <p>the team formalizes all activity as work on user stories, each team member can answer at any moment the question “what user story are you working on at the moment”</p>
                            </li>
                        </ul>
                        <p>Advanced</p>
                        <ul>
                            <li>
                                <p>at any moment, any member of the team can answer the question “what is the most important user story in the backlog, and why”</p>
                            </li>
                        </ul>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p><a href="http://www.amazon.com/User-Stories-Applied-Software-Development/dp/0321205685" target="_blank">User Stories Applied</a>, by Mike Cohn</p>
                    </li>
                    <li>
                        <p><a href="http://www.amazon.com/Software-Numbers-Low-Risk-High-Return-Development/dp/0131407287" target="_blank">Software By Numbers</a> examines the economics of iterative software development</p>
                    </li>
                    <li>
                        <p>“<a href="http://faculty.ksu.edu.sa/zohair/Documents/CSC541/Chap2-SWE processes/Agile Requirements Eng.pdf" target="_blank">Agile requirements engineering practices: An empirical study</a>” discusses an investigation of 16 software development organisations’ Agile requirements practices, and provides an overview of some previous literature on Agile requirements and user stories. The study takes a mildly adversarial attitude to Agile and comes to somewhat strange conclusions, such as ranking <a href="DevOps_TDD.htm" target="_blank">test-driven development</a> among requirements engineering practices.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
    </body>
</html>