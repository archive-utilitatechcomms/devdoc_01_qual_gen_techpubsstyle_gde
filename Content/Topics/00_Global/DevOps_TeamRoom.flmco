﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>Team Room</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>DevOps - Team Room</xhtml:h1>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                    <xhtml:p>The team (ideally the <xhtml:a xhtml:href="DevOps_Team.htm" xhtml:target="_blank">whole team</xhtml:a>, including the <xhtml:a xhtml:href="DevOps_ProductOwner.htm" xhtml:target="_blank">product owner</xhtml:a> or domain expert) has the use of a dedicated space for the duration of the project, set apart from other groups’ activities.</xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">Team rooms are favourable to what Alistair Cockburn calls “<xhtml:a xhtml:href="https://alistair.cockburn.us/coming-soon/" xhtml:target="_blank">osmotic communication</xhtml:a>“: the diffusion of information, toward equilibirum conditions, between those who need it and those who have it; instead of relying on explicit communication mechanisms (telephone, email, IM or meetings) this diffusion is by “ambient” means, for instance overhearing others’ conversations or looking at information radiators</xhtml:p>
                    <xhtml:p>This space is furnished with the various amenities that the team may need: workstations (adapted for <xhtml:a xhtml:href="DevOps_PairProgramming.htm" xhtml:target="_blank">pairing</xhtml:a> if the team uses that practice), whiteboards and flipcharts, wall space to display <xhtml:a xhtml:href="DevOps_TaskBoard.htm" xhtml:target="_blank">task boards</xhtml:a>, project plans or other charts, and so on.</xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">The separation criterion is important, an “open space” is “not” an adequate instantiation of this practice; some research suggests that noise (conversations, mainly) spilling over from unrelated activites has a disruptive effect on the concentration needed for project work, but that overhearing project-related conversations is not only tolerated but enhances teamwork; cubicle-type work environments manage “the worst of both worlds”.</xhtml:p>
                    <xhtml:p>There is a sizeable body of research on the “environmental” conditions favorable to the performance of teams in general, some of it focusing on software teams in particular. Although it is generally difficult to firmly establish “any” claims on productivity, because of the ambiguous and elusive character of the <xhtml:a xhtml:href="http://www.martinfowler.com/bliki/CannotMeasureProductivity.html" xhtml:target="_blank">very notion</xhtml:a> of productivity of software efforts, these studies seem to suggest that either end of the spectrum of isolation yields the best results: “either” individual offices “or” dedicated team rooms.</xhtml:p>
                    <xhtml:p>The open space office, in spite of its economic attractiveness, and the more subtle advantage of being able to monitor workers unobtrusively, may well be the worst of both worlds, in particular in its “cubicle” incarnation.</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>
                            <xhtml:p>Capers Jones’ <xhtml:a xhtml:href="http://ieeexplore.ieee.org//xpls/abs_all.jsp?arnumber=362626" xhtml:target="_blank">How office space affects programming productivity</xhtml:a> examines some early studies, and tends to conclude in favor of individual offices (1995)</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p><xhtml:a xhtml:href="http://woody.rtsnet.ru/literature/Becker_Sims-Offices_That_Work-IWS_0002.pdf" xhtml:target="_blank">Offices That Work</xhtml:a>, a 2001 report on studies of “small, fast-moving firms’ workspace strategies”, examined the trade-offs between openness and privacy, concentration and communication</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p><xhtml:a xhtml:href="http://ieeexplore.ieee.org//xpls/abs_all.jsp?arnumber=1019481" xhtml:target="_blank">Rapid software development through team collocation</xhtml:a>, a more recent study, discusses evidence that seems to confirm the advantage of team rooms, based on subjective reporting, interviews and observational data (2002)</xhtml:p>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>