﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>DevOps - Pair Programming</h1>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" target="_blank" title="Development Resources: IT Project Management" alt="Development Resources: IT Project Management">Development Resources: IT Project Management</a>.</p>
        <p>Pair programming consists of two programmers sharing a single workstation (one screen, keyboard and mouse among the pair). The programmer at the keyboard is usually called the “driver”, the other, also actively involved in the programming task but focusing more on overall direction is the “navigator”; it is expected that the programmers swap roles every few minutes or so.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Also Known As</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>More simply “pairing”; the phrases “paired programming” and “programming in pairs” are also used, less frequently.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>Both programmers must be actively engaging with the task throughout a paired session, otherwise no benefit can be expected.</p>
                    </li>
                    <li>
                        <p>A simplistic but often raised objection is that pairing “doubles costs”; that is a misconception based on equating programming with typing – however, one should be aware that this is the worst-case outcome of poorly applied pairing.</p>
                    </li>
                    <li>
                        <p>At least the driver, and possibly both programmers, are expected to keep up a running commentary; pair programming is also “programming out loud” – if the driver is silent, the navigator should intervene.</p>
                    </li>
                    <li>
                        <p>Pair programming cannot be fruitfully forced upon people, especially if relationship issues, including the most mundane (such as personal hygiene), are getting in the way; solve these first!</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The names of various celebrities have been invoked in an attempt to imbue pair programming with an aura of necessity if not sanctity; anecdotes of John Von Neummann, Fred Brooks, Jerry Weinberg, Richard Gabriel or <a href="http://c2.com/cgi/wiki?DijkstraPairProgramming" target="_blank" title="Edsger Dijkstra" alt="Edsger Dijkstra">Edsger Dijkstra</a> using the practice are fascinating but sometimes hard to substantiate. However, the following timeline of verifiable sources does suggest that pair programming, in its modern form, has been around since well before the Agile movement:</p>
                <ul>
                    <li>
                        <p>1992: “Dynamic Duo” is the term coined by Larry Constantine, reporting on a visit to Whitesmiths Inc., a compiler vendor started by P.J. Plauger, one of the implementors of C: “At each terminal were two programmers! Of course, only one programmer was actually cutting code at each keyboard, but the others were peering over their shoulders.” Whitesmiths existed from 1978 to 1988.</p>
                    </li>
                    <li>
                        <p>1993: “The benefits of collaboration for student programmers” by Wilson et al. is one early empirical study indicating benefits of pairing for programming tasks specifically. Posterior studies are more abundant and driven by the desire to “validate” pair programming after it had already gained popularity through <a href="DevOps_ExtremeProgramming.htm" target="_blank" title="Extreme Programming" alt="Extreme Programming">Extreme Programming</a>.</p>
                    </li>
                    <li>
                        <p>1995: the pattern “Developing in Pairs” is given a brief description, in <a href="http://c2.com/cgi/wiki?AlexandrianForm" target="_blank">Alexandrian</a> pattern form, in Jim Coplien’s chapter “A Generative Development-Process Pattern Language” from the first patterns book, “Pattern Languages of Program Design”.</p>
                    </li>
                    <li>
                        <p>1998: in “Chrysler goes to Extremes”, the earliest article about Extreme Programming, pair programming is presented as one of the core practices of the C3 team; it is later described formally as one of XP’s original “twelve practices”.</p>
                    </li>
                    <li>
                        <p>2000: (or earlier) – the roles of Driver and Navigator are introduced to help explain pair programming; the earliest known reference is a <a href="http://tech.groups.yahoo.com/group/extremeprogramming/message/12405">mailing list posting</a>; note however that the reality of these roles has been disputed, for instance Sallyann Bryant’s article “<a href="http://www.sciencedirect.com/science/article/pii/S1071581907000456" target="_blank">Pair programming and the mysterious role of the navigator</a>“.</p>
                    </li>
                    <li>
                        <p>2002: “<a href="http://www.amazon.com/dp/0201745763" target="_blank">Pair Programming Illuminated</a>“, by Laurie Williams and Robert Kessler, is the first book devoted exclusively to the practice and discusses its theory, practice and the various studies up to that date.</p>
                    </li>
                    <li>
                        <p>2003: an anonymous article on the C2 Wiki describes <a href="http://www.c2.com/cgi/wiki?PairProgrammingPingPongPattern" target="_blank">Ping-Pong Programming</a>, a moderately popular variant which marries pairing with test-driven development.</p>
                    </li>
                    <li>
                        <p>2015: James Coplien publishes <a href="https://computingnow.computer.org/web/agile-careers/content?g=8504655&amp;type=article&amp;urlTitle=two-heads-are-better-than-one" target="_blank">Two Heads are Better Than One</a> which provides an overview of the history of Pair Programming that traces its origins back to the mid 1980’s if not before.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Skill Levels</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>As suggested above one of the major issues preventing effective pairing is passivity. When used simultaneously with <a href="DevOps_TDD.htm" target="_blank" title=" Test-Driven Development" alt=" Test-Driven Development">test-driven development</a>, one variant called “ping-pong programming” encourages more frequent switching of roles: one programmer writes a failing unit test, then passes the keyboard to the other who writes the corresponding code, then goes on to a new test. This variant can be used purely for pedagogic purposes, or by already experienced programmers as a playful variant.</p>
                <ul>
                    <li>
                        <p>Beginner.</p>
                    </li>
                    <li>
                        <p>able to participate as navigator, in particular to intervene appropriately.</p>
                    </li>
                    <li>
                        <p>able to participate as driver, in particular to explain code while writing it.</p>
                    </li>
                    <li>
                        <p>Intermediate.</p>
                    </li>
                    <li>
                        <p>can tell the right moment to give up the keyboard and switch roles.</p>
                    </li>
                    <li>
                        <p>can tell the right moment to “steal” the keyboard and switch roles.</p>
                    </li>
                    <li>
                        <p>Advanced.</p>
                    </li>
                    <li>
                        <p>able to “drop in” when another pair has been working on a task and pick up the navigator role smoothly.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Signs Of Use</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>The room’s furniture and workstations are set up so as to encourage pairing (in teams new or hostile to pairing, obvious mistakes are tolerated, such as desks with too little room for two chairs).</p>
                    </li>
                    <li>
                        <p>The room’s noise level is controlled: the muted conversations from several simultaneous pairs create a background hum but do not rise to the level where they would disturb anyone’s work.</p>
                    </li>
                    <li>
                        <p>If, on entering the room, you spot any programmer wearing an audio headset, take that as a “negative” sign – not only is pairing probably not practice d in the team but the conditions for successful adoptions are likely not met.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>Increased code quality: “programming out loud” leads to clearer articulation of the complexities and hidden details in coding tasks, reducing the risk of error or going down blind alleys.</p>
                    </li>
                </ul>
                <ul>
                    <li>
                        <p>Better diffusion of knowledge among the team, in particular when a developer unfamiliar with a component is pairing with one who knows it much better.</p>
                    </li>
                    <li>
                        <p>Better transfer of skills, as junior developers pick up micro-techniques or broader skills from more experienced team members.</p>
                    </li>
                    <li>
                        <p>Large reduction in coordination efforts, since there are N/2 pairs to coordinate instead of N individual developers.</p>
                    </li>
                    <li>
                        <p>Improved resiliency of a pair to interruptions, compared to an individual developer: when one member of the pair must attend to an external prompt, the other can remains focused on the task and can assist in regaining focus afterwards.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Potential Costs</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>While empirical studies have yet to yield definite results on either benefits or costs, a commonly cited best-case estimate of 15% overhead is claimed for systematic pairing, relative to individual work; this overhead, it is claimed (again with some empirical support, though not entirely conclusive), is compensated by gains in code quality which usually entails significant maintenance penalties down the road.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Academic Publications</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>Among the more interesting theoretical papers are those pursuing the ethnographic approach initiated among others by Sallyann Freudenberg (née Bryant), using close examination of programmers in their day-to-day work:</p>
                    </li>
                    <li>
                        <p><a href="http://www.scribd.com/doc/25304465/" target="_blank">How Pair Programming Really Works</a> surveys some of the work that has attacked the “driver/navigator” distinction.</p>
                    </li>
                    <li>
                        <p><a href="http://collaboration.csc.ncsu.edu/laurie/Papers/dissertation.pdf" target="_blank">The Collaborative Software Process</a>, Laurie Williams’ doctoral thesis, among the better known studies of the topic, reporting increased quality and no statistically significant cost overhead.</p>
                    </li>
                    <li>
                        <p><a href="http://www.idi.ntnu.no/grupper/su/publ/ebse/R11-pairprog-hannay-ist09.pdf" target="_blank">The effectiveness of pair programming: A meta-analysis</a>, surveying 18 major empirical studies, reporting increased quality and compressed schedules, but some cost overhead; schedule compression mainly for simpler tasks performed by junior developers, a situation which also correlates with lower quality.</p>
                    </li>
                    <li>
                        <p>Most empirical studies (14 out of the above mentioned 18) suffer from one common flaw often cited as an obstacle to generalizing conclusions: they are conducted with “convenience samples” of graduate or undergraduate students rather than on professionals in realistic work conditions.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>