﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>ATDD</MicroContentPhrase>
        <MicroContentPhrase>Acceptance Test Driven Development</MicroContentPhrase>
        <MicroContentPhrase>Story Test Driven Development</MicroContentPhrase>
        <MicroContentPhrase>SDD</MicroContentPhrase>
        <MicroContentPhrase>Specification by Example</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>DevOps - Acceptance Test Driven Development (ATDD)</xhtml:h1>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                    <xhtml:p>Analogous to <xhtml:a xhtml:href="DevOps_TDD.htm" xhtml:target="_blank" xhtml:title="Test-Driven Development" xhtml:alt="Test-Driven Development">test-driven development</xhtml:a>, Acceptance Test Driven Development (ATDD) involves team members with different perspectives (customer, development, testing) collaborating to write <xhtml:a xhtml:href="DevOps_AcceptanceTests.htm" xhtml:target="_blank" xhtml:title="Acceptance Testing" xhtml:alt="Acceptance Testing">acceptance tests</xhtml:a> in advance of implementing the corresponding functionality.  The collaborative discussions that occur to generate the acceptance test is often referred to as the <xhtml:a xhtml:href="DevOps_ThreeAmigos.htm" xhtml:target="_blank" xhtml:title="Three Amigos" xhtml:alt="Three Amigos">three amigos</xhtml:a>, representing the three perspectives of customer (what problem are we trying to solve?), development (how might we solve this problem?), and testing (what about…).</xhtml:p>
                    <xhtml:p>These acceptance tests represent the user’s point of view and act as a form of requirements to describe how the system will function, as well as serve as a way of verifying that the system functions as intended. In some cases the team automates the acceptance tests.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Also Known As</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>ATDD may also be referred to as Story Test Driven Development (SDD), Specification by Example or <xhtml:a xhtml:href="DevOps_BDD.htm" xhtml:target="_blank" xhtml:title="Behaviour Driven Development (BDD)" xhtml:alt="Behaviour Driven Development (BDD)">Behaviour Driven Development (BDD)</xhtml:a>.  These different terms exist to stress some differences in approach that lead to similar outcomes.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Just as TDD results in applications designed to be easier to <xhtml:a xhtml:href="DevOps_UnitTesting.htm" xhtml:target="_blank" xhtml:title="Unit Test" xhtml:alt="Unit Test">unit test</xhtml:a>, ATDD favors the creation of interfaces specific to functional testing. (Testing through an application’s actual UI is considered less effective.)</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Even more than the use of automated acceptance tests, this practice is strongly associated with the use of specific tools such as Fit/FitNess, Cucumber or others.</xhtml:p>
                            <xhtml:p>One major risk, therefore, is that the tool chosen will hinder rather than advance the main purpose of this practice: facilitating conversation between developers and product owners about product requirements. Tools should be adapted to meet product owners’ needs rather than the other way around.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>2003: Kent Beck briefly mentions ATDD in the book “Test Driven Development: By Example” but dismisses it as impractical</xhtml:p>
                            <xhtml:p>2003 to 2004: driven by the popularity of Fit/FitNesse ATDD becomes accepted practice in spite of Beck’s objections</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p><xhtml:a xhtml:href="http://testobsessed.com/wp-content/uploads/2011/04/atddexample.pdf" xhtml:target="_blank" xhtml:title="Driving Development with Tests: ATDD and TDD" xhtml:alt="Driving Development with Tests: ATDD and TDD">Driving Development with Tests: ATDD and TDD</xhtml:a>
                                    </xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p><xhtml:a xhtml:href="https://www.infoq.com/articles/atdd-from-the-trenches" xhtml:target="_blank" xhtml:title="ATDD From the Trenches" xhtml:alt="ATDD From the Trenches">ATDD From the Trenches</xhtml:a>
                                    </xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>