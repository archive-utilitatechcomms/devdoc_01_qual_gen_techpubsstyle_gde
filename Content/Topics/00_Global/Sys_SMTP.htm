﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>System: Simple Mail Transfer Protocol (SMTP)</h1>
        <p>The <a href="https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol" target="_blank" title="Wikipedia: Simple Mail Transfer Protocol" alt="Wikipedia: Simple Mail Transfer Protocol">Simple Mail Transfer Protocol (SMTP)</a> is a communication protocol for electronic mail transmission. As an Internet standard, SMTP was first defined in 1982 by <a href="https://en.wikipedia.org/wiki/RFC_(identifier)" target="_blank">RFC</a> <a href="https://tools.ietf.org/html/rfc821" target="_blank">821</a>, and updated in 2008 by RFC 5321 to <a href="https://en.wikipedia.org/wiki/Extended_SMTP" target="_blank">Extended SMTP</a> additions, which is the protocol variety in widespread use today. Mail servers and other <a href="https://en.wikipedia.org/wiki/Message_transfer_agent" target="_blank">message transfer agents</a> use SMTP to send and receive mail messages. SMTP servers commonly use the <a href="https://en.wikipedia.org/wiki/Transmission_Control_Protocol" target="_blank">Transmission Control Protocol</a> on <a href="https://en.wikipedia.org/wiki/Port_number" target="_blank">port number</a> 25.</p>
        <p>User-level email clients typically use SMTP only for sending messages to a mail server for relaying, and typically submit outgoing email to the mail server on port 587 or 465 per <a href="https://tools.ietf.org/html/rfc8314" target="_blank">RFC 8314</a>. For retrieving messages, <a href="https://en.wikipedia.org/wiki/Internet_Message_Access_Protocol" target="_blank">IMAP</a> and <a href="https://en.wikipedia.org/wiki/Post_Office_Protocol" target="_blank">POP3</a> are standard, but proprietary servers also often implement proprietary protocols, e.g., <a href="https://en.wikipedia.org/wiki/Exchange_ActiveSync" target="_blank">Exchange ActiveSync</a>.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Mail processing model</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Email is submitted by a mail client (<a href="https://en.wikipedia.org/wiki/Mail_user_agent" target="_blank">mail user agent, MUA</a>) to a mail server (<a href="https://en.wikipedia.org/wiki/Mail_submission_agent" target="_blank">mail submission agent, MSA</a>) using SMTP on TCP port 587. Most mailbox providers still allow submission on traditional port 25. The MSA delivers the mail to its mail transfer agent (<a href="https://en.wikipedia.org/wiki/Mail_transfer_agent" target="_blank">mail transfer agent, MTA</a>). Often, these two agents are instances of the same software launched with different options on the same machine. Local processing can be done either on a single machine, or split among multiple machines; mail agent processes on one machine can share files, but if processing is on multiple machines, they transfer messages between each other using SMTP, where each machine is configured to use the next machine as a smart host. Each process is an MTA (an SMTP server) in its own right.</p>
                <p>The boundary MTA uses <a href="Sys_DNS.htm" target="_blank">DNS</a> to look up the <a href="https://en.wikipedia.org/wiki/MX_record" target="_blank">MX (mail exchanger)</a> record for the recipient's domain (the part of the email address on the right of @). The MX record contains the name of the target MTA. Based on the target host and other factors, the sending MTA selects a recipient server and connects to it to complete the mail exchange.</p>
                <p>Message transfer can occur in a single connection between two MTAs, or in a series of hops through intermediary systems. A receiving SMTP server may be the ultimate destination, an intermediate "relay" (that is, it stores and forwards the message) or a "gateway" (that is, it may forward the message using some protocol other than SMTP). Each hop is a formal handoff of responsibility for the message, whereby the receiving server must either deliver the message or properly report the failure to do so.</p>
                <p>Once the final hop accepts the incoming message, it hands it to a <a href="https://en.wikipedia.org/wiki/Mail_delivery_agent" target="_blank">mail delivery agent (MDA)</a> for local delivery. An MDA saves messages in the relevant mailbox format. As with sending, this reception can be done using one or multiple computers, but in the diagram below the MDA is depicted as one box near the mail exchanger box. An MDA may deliver messages directly to storage, or forward them over a network using SMTP or other protocol such as <a href="https://en.wikipedia.org/wiki/Local_Mail_Transfer_Protocol" target="_blank">Local Mail Transfer Protocol (LMTP)</a>, a derivative of SMTP designed for this purpose.</p>
                <p>Once delivered to the local mail server, the mail is stored for batch retrieval by authenticated mail clients (MUAs). Mail is retrieved by end-user applications, called email clients, using <a href="https://en.wikipedia.org/wiki/Internet_Message_Access_Protocol" target="_blank">Internet Message Access Protocol (IMAP)</a>, a protocol that both facilitates access to mail and manages stored mail, or the <a href="https://en.wikipedia.org/wiki/Post_Office_Protocol" target="_blank">Post Office Protocol (POP)</a> which typically uses the traditional <a href="https://en.wikipedia.org/wiki/Mbox" target="_blank">mbox</a> mail file format or a proprietary system such as Microsoft Exchange/Outlook or Lotus Notes/Domino. Webmail clients may use either method, but the retrieval protocol is often not a formal standard.</p>
                <p>
                    <img src="00_IMG_Sys_SMTP.png" style="width: 500px;min-width: 500px;max-width: 500px;height: 298px;min-height: 298px;max-height: 298px;" />
                </p>
                <p>SMTP defines message transport, not the message content. Thus, it defines the mail envelope and its parameters, such as the envelope sender, but not the header (except trace information) nor the body of the message itself. STD 10 and <a href="https://tools.ietf.org/html/rfc5321" target="_blank">RFC 5321</a> define SMTP (the envelope), while STD 11 and <a href="https://tools.ietf.org/html/rfc5322" target="_blank">RFC 5322</a> define the message (header and body), formally referred to as the <a href="https://en.wikipedia.org/wiki/Internet_Message_Format" target="_blank">Internet Message Format</a>.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Protocol overview</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>SMTP is a connection-oriented, text-based protocol in which a mail sender communicates with a mail receiver by issuing command strings and supplying necessary data over a reliable ordered data stream channel, typically a Transmission Control Protocol (TCP) connection. An SMTP session consists of commands originated by an SMTP client (the initiating agent, sender, or transmitter) and corresponding responses from the SMTP server (the listening agent, or receiver) so that the session is opened, and session parameters are exchanged. A session may include zero or more SMTP transactions. An SMTP transaction consists of three command/reply sequences:</p>
                <ul>
                    <li>
                        <p><b>MAIL</b> command, to establish the return address, also called return-path, reverse-path, bounce address, mfrom, or envelope sender.</p>
                    </li>
                    <li>
                        <p><b>RCPT</b> command, to establish a recipient of the message. This command can be issued multiple times, one for each recipient. These addresses are also part of the envelope.</p>
                    </li>
                    <li>
                        <p><b>DATA</b> to signal the beginning of the message text; the content of the message, as opposed to its envelope. It consists of a message header and a message body separated by an empty line. DATA is actually a group of commands, and the server replies twice: once to the DATA command itself, to acknowledge that it is ready to receive the text, and the second time after the end-of-data sequence, to either accept or reject the entire message.</p>
                    </li>
                </ul>
                <p>Besides the intermediate reply for DATA, each server's reply can be either positive (2xx reply codes) or negative. Negative replies can be permanent (5xx codes) or transient (4xx codes). A reject is a permanent failure and the client should send a bounce message to the server it received it from. A drop is a positive response followed by message discard rather than delivery.</p>
                <p>The initiating host, the SMTP client, can be either an end-user's email client, functionally identified as a <a href="https://en.wikipedia.org/wiki/Mail_user_agent" target="_blank">mail user agent (MUA)</a>, or a relay server's <a href="https://en.wikipedia.org/wiki/Mail_transfer_agent" target="_blank">mail transfer agent (MTA)</a>, that is an SMTP server acting as an SMTP client, in the relevant session, in order to relay mail. Fully capable SMTP servers maintain queues of messages for retrying message transmissions that resulted in transient failures.</p>
                <p>A MUA knows the outgoing mail SMTP server from its configuration. A relay server typically determines which server to connect to by looking up the MX (Mail eXchange) DNS resource record for each recipient's domain name. If no MX record is found, a conformant relaying server (not all are) instead looks up the A record. Relay servers can also be configured to use a <a href="https://en.wikipedia.org/wiki/Smart_host" target="_blank">smart host</a>. A relay server initiates a TCP connection to the server on the "<a href="https://en.wikipedia.org/wiki/Well-known_port" target="_blank">well-known port</a>" for SMTP: port 25, or for connecting to an MSA, port 587. The main difference between an MTA and an MSA is that connecting to an MSA requires <a href="https://en.wikipedia.org/wiki/SMTP_Authentication" target="_blank">SMTP Authentication</a>.</p>
                <ul>
                    <li>
                        <p><b>SMTP vs mail retrieval:</b> SMTP is a delivery protocol only. In normal use, mail is "pushed" to a destination mail server (or next-hop mail server) as it arrives. Mail is routed based on the destination server, not the individual user(s) to which it is addressed. Other protocols, such as the <a href="https://en.wikipedia.org/wiki/Post_Office_Protocol" target="_blank">Post Office Protocol (POP)</a> and the Internet <a href="https://en.wikipedia.org/wiki/Internet_Message_Access_Protocol" target="_blank">Message Access Protocol (IMAP)</a> are specifically designed for use by individual users retrieving messages and managing mail boxes. To permit an intermittently-connected mail server to pull messages from a remote server on demand, SMTP has a feature to initiate mail queue processing on a remote server. POP and IMAP are unsuitable protocols for relaying mail by intermittently-connected machines; they are designed to operate after final delivery, when information critical to the correct operation of mail relay (the "mail envelope") has been removed.</p>
                    </li>
                </ul>
                <ul>
                    <li>
                        <p><b>Remote Message Queue Starting:</b> Remote Message Queue Starting enables a remote host to start processing of the mail queue on a server so it may receive messages destined to it by sending a corresponding command. The original <code>TURN</code> command was deemed insecure and was extended in <a href="https://tools.ietf.org/html/rfc1985" target="_blank">RFC 1985</a> with the <code>ETRN</code> command which operates more securely using an authentication method based on <a href="Sys_DNS.htm" target="_blank">Domain Name System</a> information.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Outgoing mail SMTP server</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>An email client needs to know the IP address of its initial SMTP server and this has to be given as part of its configuration (usually given as a DNS name). This server will deliver outgoing messages on behalf of the user.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Outgoing mail server access restrictions</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Server administrators need to impose some control on which clients can use the server. This enables them to deal with abuse, for example spam. Two solutions have been in common use:</p>
                        <ul>
                            <li>
                                <p>In the past, many systems imposed usage restrictions by the location of the client, only permitting usage by clients whose IP address is one that the server administrators control. Usage from any other client IP address is disallowed.</p>
                            </li>
                            <li>
                                <p>Modern SMTP servers typically offer an alternative system that requires authentication of clients by credentials before allowing access.</p>
                            </li>
                        </ul>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Restricting access by location</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Under this system, an ISP's SMTP server will not allow access by users who are outside the ISP's network. More precisely, the server may only allow access to users with an IP address provided by the ISP, which is equivalent to requiring that they are connected to the Internet using that same ISP. A mobile user may often be on a network other than that of their normal ISP, and will then find that sending email fails because the configured SMTP server choice is no longer accessible.</p>
                        <p>This system has several variations. For example, an organisation's SMTP server may only provide service to users on the same network, enforcing this by firewalling to block access by users on the wider Internet. Or the server may perform range checks on the client's IP address. These methods were typically used by corporations and institutions such as universities which provided an SMTP server for outbound mail only for use internally within the organisation. However, most of these bodies now use client authentication methods, as described below.</p>
                        <p>Where a user is mobile, and may use different ISPs to connect to the internet, this kind of usage restriction is onerous, and altering the configured outbound email SMTP server address is impractical. It is highly desirable to be able to use email client configuration information that does not need to change.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Client authentication</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Modern SMTP servers typically require authentication of clients by credentials before allowing access, rather than restricting access by location. This more flexible system is friendly to mobile users and allows them to have a fixed choice of configured outbound SMTP server. SMTP Authentication, often abbreviated SMTP AUTH, is an extension of the SMTP in order to log in using an authentication mechanism.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Open relay</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>A server that is accessible on the wider Internet and does not enforce these kinds of access restrictions is known as an <a href="https://en.wikipedia.org/wiki/Open_relay" target="_blank">open relay</a>. This is now generally considered a bad practice worthy of <a href="https://en.wikipedia.org/wiki/Blacklist_(computing)" target="_blank">blacklisting</a>.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Ports</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Communication between mail servers generally uses the standard TCP port 25 designated for SMTP.</p>
                        <p>Mail clients however generally don't use this, instead using specific "submission" ports. Mail services generally accept email submission from clients on one of::</p>
                        <ul>
                            <li>
                                <p>587 (Submission), as formalised in <a href="https://tools.ietf.org/html/rfc6409" target="_blank">RFC 6409.</a></p>
                            </li>
                            <li>
                                <p>465 This port was deprecated after <a href="https://tools.ietf.org/html/rfc2487" target="_blank">RFC 2487</a>, until the issue of <a href="https://tools.ietf.org/html/rfc8314" target="_blank">RFC 8314</a>.</p>
                            </li>
                        </ul>
                        <p>Port 2525 and others may be used by some individual providers, but have never been officially supported.</p>
                        <p>Most Internet service providers now block all outgoing port 25 traffic from their customers as an anti-spam measure. For the same reason, businesses will typically configure their firewall to only allow outgoing port 25 traffic from their designated mail servers.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>SMTP transport</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>A typical example of sending a message via SMTP to two mailboxes (alice and theboss) located in the same mail domain (example.com or localhost.com) is reproduced in the following session exchange. (In this example, the conversation parts are prefixed with S: and C:, for server and client, respectively; these labels are not part of the exchange.)</p>
                <p>After the message sender (SMTP client) establishes a reliable communications channel to the message receiver (SMTP server), the session is opened with a greeting by the server, usually containing its <a href="https://en.wikipedia.org/wiki/Fully_qualified_domain_name" target="_blank">fully qualified domain name (FQDN)</a>, in this case smtp.example.com. The client initiates its dialog by responding with a <code>HELO</code> command identifying itself in the command's parameter with its FQDN (or an address literal if none is available).[20]</p><pre class="code">S: 220 smtp.example.com ESMTP Postfix
C: HELO relay.example.com
S: 250 smtp.example.com, I am glad to meet you
C: MAIL FROM:&lt;bob@example.com&gt;
S: 250 Ok
C: RCPT TO:&lt;alice@example.com&gt;
S: 250 Ok
C: RCPT TO:&lt;theboss@example.com&gt;
S: 250 Ok
C: DATA
S: 354 End data with &lt;CR&gt;&lt;LF&gt;.&lt;CR&gt;&lt;LF&gt;
C: From: "Bob Example" &lt;bob@example.com&gt;
C: To: Alice Example &lt;alice@example.com&gt;
C: Cc: theboss@example.com
C: Date: Tue, 15 Jan 2008 16:02:43 -0500
C: Subject: Test message
C: 
C: Hello Alice.
C: This is a test message with 5 header fields and 4 lines in the message body.
C: Your friend,
C: Bob
C: .
S: 250 Ok: queued as 12345
C: QUIT
S: 221 Bye
{The server closes the connection}</pre>
                <p>The client notifies the receiver of the originating email address of the message in a <code>MAIL FROM</code> command. This is also the return or bounce address in case the message cannot be delivered. In this example the email message is sent to two mailboxes on the same SMTP server: one for each recipient listed in the To and Cc header fields. The corresponding SMTP command is <code>RCPT TO</code>. Each successful reception and execution of a command is acknowledged by the server with a result code and response message (e.g., 250 Ok).</p>
                <p>The transmission of the body of the mail message is initiated with a <code>DATA</code> command after which it is transmitted verbatim line by line and is terminated with an end-of-data sequence. This sequence consists of a new-line (<code>&lt;CR&gt;&lt;LF&gt;</code>), a single full stop (period), followed by another new-line. Since a message body can contain a line with just a period as part of the text, the client sends two periods every time a line starts with a period; correspondingly, the server replaces every sequence of two periods at the beginning of a line with a single one. Such escaping method is called dot-stuffing.</p>
                <p>The server's positive reply to the end-of-data, as exemplified, implies that the server has taken the responsibility of delivering the message. A message can be doubled if there is a communication failure at this time, e.g. due to a power shortage: Until the sender has received that 250 reply, it must assume the message was not delivered. On the other hand, after the receiver has decided to accept the message, it must assume the message has been delivered to it. Thus, during this time span, both agents have active copies of the message that they will try to deliver. The probability that a communication failure occurs exactly at this step is directly proportional to the amount of filtering that the server performs on the message body, most often for anti-spam purposes. The limiting timeout is specified to be 10 minutes.</p>
                <p>The <code>QUIT</code> command ends the session. If the email has other recipients located elsewhere, the client would <code>QUIT</code> and connect to an appropriate SMTP server for subsequent recipients after the current destination(s) had been queued. The information that the client sends in the <code>HELO</code> and <code>MAIL FROM</code> commands are added (not seen in the above example code) as additional header fields to the message by the receiving server. It adds a <code>Received </code>and <code>Return-Path</code> header field, respectively.</p>
                <p>Some clients are implemented to close the connection after the message is accepted (<code>250 Ok: queued as 12345</code>), so the last two lines may actually be omitted. This causes an error on the server when trying to send the <code>221</code> reply.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>