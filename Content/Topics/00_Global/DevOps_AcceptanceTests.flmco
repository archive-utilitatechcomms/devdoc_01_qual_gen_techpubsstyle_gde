﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>Acceptance Tests</MicroContentPhrase>
        <MicroContentPhrase>Acceptance Test</MicroContentPhrase>
        <MicroContentPhrase>functional test</MicroContentPhrase>
        <MicroContentPhrase>customer test</MicroContentPhrase>
        <MicroContentPhrase>story test</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>DevOps - Acceptance Tests</xhtml:h1>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                    <xhtml:p>An acceptance test is a formal description of the behavior of a software product, generally expressed as an example or a usage scenario. A number of different notations and approaches have been proposed for such examples or scenarios. In many cases the aim is that it should be possible to automate the execution of such tests by a software tool, either ad-hoc to the development team or off the shelf.</xhtml:p>
                    <xhtml:p>Similar to a <xhtml:a xhtml:href="DevOps_UnitTesting.htm" xhtml:target="_blank" xhtml:title="Unit Test" xhtml:alt="Unit Test">unit test</xhtml:a>, an acceptance test generally has a binary result, pass or fail. A failure suggests, though does not prove, the presence of a defect in the product.</xhtml:p>
                    <xhtml:p>Teams mature in their practice of agile  use acceptance tests as the main form of functional specification and the only formal expression of business requirements. Other teams use acceptance tests as a complement to specification documents containing uses cases or more narrative text.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Also Known As</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The terms “functional test”, “acceptance test” and “customer test” are used more or less interchangeably. A more specific term “story test”, referring to <xhtml:a xhtml:href="DevOps_UserStories.htm" xhtml:target="_blank" xhtml:title="User Stories" xhtml:alt="User Stories">user stories</xhtml:a> is also used, as in the phrase “story test driven development”.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Acceptance testing has the following benefits, complementing those which can be obtained from <xhtml:a xhtml:href="DevOps_UnitTesting.htm" xhtml:target="_blank" xhtml:title="Unit Testing" xhtml:alt="Unit Testing">unit tests</xhtml:a>:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Encouraging closer collaboration between developers on the one hand and customers, users or domain experts on the other, as they entail that business requirements should be expressed.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Providing a clear and unambiguous “contract” between customers and developers; a product which passes acceptance tests will be considered adequate (though customers and developers might refine existing tests or suggest new ones as necessary).</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Decreasing the chance and severity both of new defects and regressions (defects impairing functionality previously reviewed and declared acceptable).</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Customers and domain experts, the primary audience for acceptance tests, find acceptance tests that contain implementation details difficult to review and understand.  To prevent acceptance tests from being overly concerned with technical implementation, involve customers and/or domain experts in the creation and discussion of acceptance tests.  See <xhtml:a xhtml:href="DevOps_BDD.htm" xhtml:target="_blank" xhtml:title="Behaviour Driven Development" xhtml:alt="Behaviour Driven Development">Behaviour Driven Development</xhtml:a> for more information.</xhtml:p>
                            <xhtml:p>Acceptance tests that are unduly focused on technical implementation also run a the risk of failing due to minor or cosmetic changes which in reality do not have any impact on the product’s behavior.  For example, if an acceptance test references the label for a text field, and that label changes, the acceptance test fails even though the actual functioning of the product is not impacted.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Potential Costs</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Unlike automated unit tests, automated acceptance tests are not universally viewed as a net benefit and some controversy has arisen after experts such as Jim Shore or Brian Marick questioned whether the following costs were outweighed by the benefits of the practice:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Many teams report that the creation of automated acceptance tests requires significant effort.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Sometimes due to the “fragile” test issue, teams find the maintenance of automated acceptance tests burdensome.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>The first generation of tools in the Fit/FitNesse tradition resulted in acceptance tests that customers or domain experts could not understand..</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>The <xhtml:a xhtml:href="DevOps_BDD.htm" xhtml:target="_blank" xhtml:title="Behavior Driven Development " xhtml:alt="Behavior Driven Development ">BDD</xhtml:a> approach may hold promise for a resolution of this controversy.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>1996: Automated tests identified as a practice of <xhtml:a xhtml:href="DevOps_ExtremeProgramming.htm" xhtml:target="_blank" xhtml:title="Extreme Programming" xhtml:alt="Extreme Programming">Extreme Programming</xhtml:a>, without much emphasis on the distinction between unit and acceptance testing, and with no particular notation or tool recommended.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>2002: Ward Cunningham, one of the inventors of Extreme Programming, publishes Fit, a tool for acceptance testing based on a tabular, Excel-like notation.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>2003: Bob Martin combines Fit with Wikis (another invention of Cunningham’s), creating FitNesse.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>2003-2006: Fit/FitNesse combo eclipses most other tools and becomes the mainstream model for Agile acceptance testing.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>For a comprehensive survey, see <xhtml:a xhtml:href="https://ieeexplore.ieee.org/document/4599450" xhtml:target="_blank" xhtml:title="Automated Acceptance Testing: A Literature Review and an Industrial Case Study" xhtml:alt="Automated Acceptance Testing: A Literature Review and an Industrial Case Study">Automated Acceptance Testing: A Literature Review and an Industrial Case Study</xhtml:a>.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>