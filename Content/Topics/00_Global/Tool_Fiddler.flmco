﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="fiddler">
        <MicroContentPhrase>Fiddler</MicroContentPhrase>
        <MicroContentPhrase>Network Packet Analysis</MicroContentPhrase>
        <MicroContentPhrase>Packet Analysis</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>Tool: Fiddler (Network Packet Analysis)</xhtml:h1>
                    <xhtml:h2>About Fiddler</xhtml:h2>
                    <xhtml:p><xhtml:a xhtml:href="https://www.telerik.com/fiddler" xhtml:target="_blank" xhtml:title="Telerik Fiddler" xhtml:alt="Telerik Fiddler">Telerik Fiddler</xhtml:a> is an <xhtml:a xhtml:href="https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol" xhtml:target="_blank" xhtml:title="HTTP" xhtml:alt="HTTP">HTTP</xhtml:a> debugging proxy server application.It captures HTTP and HTTPS traffic and logs it for the user to review (the latter by implementing <xhtml:a xhtml:href="https://en.wikipedia.org/wiki/Man-in-the-middle_attack" xhtml:target="_blank" xhtml:title="Man-in-the-middle attack" xhtml:alt="Man-in-the-middle attack">man-in-the-middle interception</xhtml:a> using <xhtml:a xhtml:href="https://en.wikipedia.org/wiki/Self-signed_certificate" xhtml:target="_blank" xhtml:title="self-signed certificates" xhtml:alt="self-signed certificates">self-signed certificates</xhtml:a>).</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li><xhtml:b>Web Session Manipulation:</xhtml:b> Edit web sessions easily:just set a breakpoint to pause the processing of the session and permit alteration of the request/response. Compose your own HTTP requests and run them through Fiddler.</xhtml:li>
                        <xhtml:li><xhtml:b>Security Testing:</xhtml:b> Decrypt HTTPS traffic and display and modify web application requests using a man-in-the-middle decryption technique. Configure Fiddler to decrypt all traffic, or only specific sessions</xhtml:li>
                        <xhtml:li><xhtml:b>Performance Testing:</xhtml:b> Fiddler lets you see the “total page weight,” HTTP caching and compression at a glance. Isolate performance bottlenecks with rules such as “Flag any uncompressed responses larger than 25kb.”</xhtml:li>
                        <xhtml:li><xhtml:b>HTTP/HTTPS Traffic Recording:</xhtml:b> Use Fiddler to log all HTTP(S) traffic between your computer and the Internet. Debug traffic from virtually any application that supports a proxy (IE, Chrome, Safari, Firefox, Opera and more).</xhtml:li>
                        <xhtml:li xhtml:style="font-weight: normal;"><xhtml:b>Customisable Free Tool:</xhtml:b> Benefit from a rich extensibility model, ranging from simple FiddlerScript to powerful extensions which can be developed using any .NET language.</xhtml:li>
                        <xhtml:li><xhtml:b>Web Debugging:</xhtml:b> Debug traffic from PC, Mac or Linux systems and mobile (iOS and Android) devices. Ensure the proper cookies, headers and cache directives are transferred between the client and server. Supports any framework, including .NET, Java, Ruby, etc.</xhtml:li>
                    </xhtml:ul>
                    <xhtml:h2>Fiddler (Perform a quick trace)</xhtml:h2>
                    <xhtml:p>As a web debugging proxy tool , Fiddler captures HTTP(S) traffic in a Windows environment.</xhtml:p>
                    <xhtml:p>1.	Download <xhtml:a xhtml:href="https://www.telerik.com/download/fiddler" xhtml:target="_blank" xhtml:title="Fiddler" xhtml:alt="Fiddler">Fiddler</xhtml:a>.</xhtml:p>
                    <xhtml:p>2.	Open it.</xhtml:p>
                    <xhtml:p>3.	Clear your browser cache.</xhtml:p>
                    <xhtml:p>4.	Browse to your site. Visit the pages that are problematic and a contrasting non-problematic page if appropriate, for contrast.</xhtml:p>
                    <xhtml:p>Fiddler can capture local traffic by using the <xhtml:a xhtml:href="https://www.telerik.com/videos/fiddler" xhtml:target="_blank" xhtml:title="Fiddler Videos" xhtml:alt="Fiddler Videos">machine's name as the host name</xhtml:a> rather than 'localhost'.</xhtml:p>
                    <xhtml:p>5.	Click <xhtml:b>File</xhtml:b> &gt; <xhtml:b>Save</xhtml:b> &gt; <xhtml:b>All Sessions</xhtml:b>....</xhtml:p>
                    <xhtml:p>6.	Attach the resulting file in <xhtml:code>.saz</xhtml:code> format for Support.</xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">If you're using HTTPS, Fiddler has a functionality to capture traffic using <xhtml:a xhtml:href="http://docs.telerik.com/fiddler/configure-fiddler/tasks/decrypthttps" xhtml:target="_blank" xhtml:title="Configure Fiddler to Decrypt HTTPS Traffic" xhtml:alt="Configure Fiddler to Decrypt HTTPS Traffic">decrypt HTTPS</xhtml:a>. Make sure you enable this before you start capturing.</xhtml:p>
                    <xhtml:p>&#160;</xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about the tools we use <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_DevTools_GDE/Content/Topics/1_Introduction/01_DevDoc_Resource-DevTools_00.htm" xhtml:target="_blank" xhtml:title="Development Resources: Tools" xhtml:alt="Development Resources: Tools">here</xhtml:a>.</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>