﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>DevOps - Refactoring</h1>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" target="_blank" title="Development Resources: IT Project Management" alt="Development Resources: IT Project Management">Development Resources: IT Project Management</a>.</p>
        <p>Refactoring consists of improving the internal structure of an existing program’s source code, while preserving its external behavior.</p>
        <p>The noun “refactoring” refers to one particular behavior-preserving transformation, such as “Extract Method” or “Introduce Parameter.”</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Refactoring does “not” mean:</p>
                <ul>
                    <li>
                        <p>Rewriting code.</p>
                    </li>
                    <li>
                        <p>Fixing bugs.</p>
                    </li>
                    <li>
                        <p>Improve observable aspects of software such as its interface.</p>
                    </li>
                </ul>
                <p>Refactoring in the absence of safeguards against introducing defects (i.e. violating the “behaviour preserving” condition) is risky. Safeguards include aids to regression testing including automated unit tests or automated acceptance tests, and aids to formal reasoning such as type systems.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The following are claimed benefits of refactoring:</p>
                <ul>
                    <li>
                        <p>Refactoring improves objective attributes of code (length, duplication, coupling and cohesion, cyclomatic complexity) that correlate with ease of maintenance.</p>
                    </li>
                    <li>
                        <p>Refactoring helps code understanding.</p>
                    </li>
                    <li>
                        <p>Refactoring encourages each developer to think about and understand design decisions, in particular in the context of <a href="DevOps_CollectiveOwnership.htm" target="_blank" title=" Collective Ownership" alt=" Collective Ownership">collective ownership / collective code ownership</a>.</p>
                    </li>
                    <li>
                        <p>Refactoring favours the emergence of reusable design elements (such as design patterns) and code modules.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Signs Of Use</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p><a href="DevOps_VersionControl.htm" target="_blank" title="Version Control" alt="Version Control">Version control records</a> (such as CVS or git logs) include entries labeled “Refactoring”.</p>
                    </li>
                    <li>
                        <p>The code modifications corresponding to such entries can be verified to be behaviour-neutral: no new unit tests or functional tests are introduced, for example.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Skill Levels</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>Beginner</p>
                    </li>
                    <li>
                        <p>knows the definition of “refactoring”</p>
                    </li>
                    <li>
                        <p>can use some automated refactorings from the IDE</p>
                    </li>
                    <li>
                        <p>can perform some refactorings by hand</p>
                    </li>
                    <li>
                        <p>is aware of the risks of regression from manual and automated refactorings</p>
                    </li>
                    <li>
                        <p>is aware of code duplication and can remove it by refactoring</p>
                    </li>
                    <li>
                        <p>Intermediate</p>
                    </li>
                    <li>
                        <p>knows and is able to remedy a broader range of “code smells”</p>
                    </li>
                    <li>
                        <p>can chain several refactorings to carry out a design intention, in awareness of the dependencies between refactorings</p>
                    </li>
                    <li>
                        <p>refactors continuously, rather than in sporadic and lengthy sessions</p>
                    </li>
                    <li>
                        <p>Advanced</p>
                    </li>
                    <li>
                        <p>has an acute sense of code duplication and coupling</p>
                    </li>
                    <li>
                        <p>applies refactorings to non-code elements such as database schema, documents, etc.</p>
                    </li>
                    <li>
                        <p>uses refactoring to guide large bodies of code toward design styles intentionally chosen from a broad palette: object-oriented, functional, or inspired by known design patterns</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Although the practice of refactoring has become popular, rigorously establishing its benefit in an academic context has proven thorny, illustrating a common gap between research and common practice.</p>
                <ul>
                    <li>
                        <p><a href="http://win.ua.ac.be/~qsoeten/other/data/Soetens2010QUATIC.pdf" target="_blank" title="Studying the Effect of Refactorings: a Complexity Metrics Perspective" alt="Studying the Effect of Refactorings: a Complexity Metrics Perspective">Studying the Effect of Refactorings: a Complexity Metrics Perspective</a>, a 2010 study, finds surprisingly little correlation between refactoring episodes, as identified by version control logs, and decrease in cyclomatic complexity.</p>
                    </li>
                    <li>
                        <p><a href="http://www.amazon.com/dp/0201485672" target="_blank" title="Refactoring, by Martin Fowler" alt="Refactoring, by Martin Fowler">Refactoring</a>, by Martin Fowler.</p>
                    </li>
                    <li>
                        <p><a href="https://www.manning.com/books/the-mikado-method" target="_blank" title="The Mikado Method by Ola Ellnestam and Daniel Brolund" alt="The Mikado Method by Ola Ellnestam and Daniel Brolund">The Mikado Method</a> by Ola Ellnestam and Daniel Brolund.</p>
                    </li>
                </ul>
                <p>Such studies may be affected by methodological issues, such as identifying what counts as a “refactoring” when examining code histories after the fact; the above paper, for instance, finds that programmers often label “refactorings” sets of code changes which also include additional functionality or bug fixes.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>