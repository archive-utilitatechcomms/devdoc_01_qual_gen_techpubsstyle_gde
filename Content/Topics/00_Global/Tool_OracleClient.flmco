﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="oracle-client">
        <MicroContentPhrase>Oracle Client</MicroContentPhrase>
        <MicroContentPhrase>SQL Developer</MicroContentPhrase>
        <MicroContentPhrase>SQL</MicroContentPhrase>
        <MicroContentPhrase>Database Management</MicroContentPhrase>
        <MicroContentPhrase>Integrated Developer Environment</MicroContentPhrase>
        <MicroContentPhrase>IDE</MicroContentPhrase>
        <MicroContentPhrase>Oracle</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1><xhtml:a xhtml:name="Toad"></xhtml:a>Tool: Oracle Client &amp; SQL Developer (Database Management &amp; Integrated Developer Environment - IDE))</xhtml:h1>
                    <xhtml:h2>Oracle Client</xhtml:h2>
                    <xhtml:p>The <xhtml:a xhtml:href="http://www.oracle.com/technetwork/database/database-technologies/instant-client/overview/index.html" xhtml:target="_blank" xhtml:title="Oracle Instant Client" xhtml:alt="Oracle Instant Client">Oracle client</xhtml:a> is required on your PC if you want to connect to Utilita's databases, in two tier with applications such as application designer, data mover, and configuration manager.</xhtml:p>
                    <xhtml:p>The Oracle Instant Client enables applications to connect to a local or remote Oracle Database for development and production deployment. The Instant Client libraries provide the necessary network connectivity, as well as basic and high end data features, to make full use of Oracle Database. It underlies the Oracle APIs of popular languages and environments including Node.js, Python, and PHP, as well as providing access for OCI, OCCI, JDBC, ODBC and Pro*C applications. Tools included in Instant Client, such as SQL*Plus and Oracle Data Pump, provide quick and convenient data access.</xhtml:p>
                    <xhtml:p>You can download the latest version of the Oracle client <xhtml:a xhtml:href="http://www.oracle.com/technetwork/indexes/downloads/index.html" xhtml:target="_blank" xhtml:title="Oracle Client Download" xhtml:alt="Oracle Client Download">here</xhtml:a>. </xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">Oracle client is backwards compatible so you can use a version that is newer than the version of the Oracle database you are connecting to. So for example, you can use the 11g client and connect to a 10g database. There a number of versions of the Oracle client, the one I generally tend to install is the runtime client, although all you really need is the <xhtml:a xhtml:href="http://www.oracle.com/technetwork/database/database-technologies/instant-client/overview/index-4369172.html" xhtml:target="_blank" xhtml:title="Instant Client" xhtml:alt="Instant Client">Instant Client</xhtml:a> provided you install it correctly.</xhtml:p>
                    <xhtml:p>In computer science, a database connection is the means by which a database server and its client software communicate with each other. The term is used whether or not the client and the server are on different machines. The client uses a database connection to send commands to and receive replies from the server.</xhtml:p>
                    <xhtml:p>A database server is the Oracle software managing a database, and a client is an application that requests information from a server. Each computer in a network is a node that can host one or more databases. Each node in a distributed database system can act as a client, a server, or both, depending on the situation.</xhtml:p>
                    <xhtml:p>Instant Client allows you to run your applications without installing the standard Oracle client or having an <xhtml:code>ORACLE_HOME.</xhtml:code><xhtml:code>OCI</xhtml:code>, <xhtml:code>OCCI</xhtml:code>, <xhtml:code>Pro*C</xhtml:code>, <xhtml:code>ODBC</xhtml:code>, and <xhtml:code>JDBC</xhtml:code> applications work without modification, while using significantly less disk space than before. Even SQL*Plus can be used with Instant Client.</xhtml:p>
                    <xhtml:h2>SQL Developer</xhtml:h2>
                    <xhtml:p><xhtml:a xhtml:href="http://www.oracle.com/technetwork/developer-tools/sql-developer/what-is-sqldev-093866.html" xhtml:target="_blank" xhtml:title="What is SQL Developer? " xhtml:alt="What is SQL Developer? ">Oracle SQL Developer</xhtml:a> is the <xhtml:a xhtml:href="http://www.oracle.com/technetwork/developer-tools/sql-developer/overview/index.html" xhtml:target="_blank" xhtml:title="Oracle SQL Developer" xhtml:alt="Oracle SQL Developer">Oracle Database IDE</xhtml:a>. A free graphical user interface, Oracle SQL Developer allows database users and administrators to do their database tasks in fewer clicks and keystrokes.</xhtml:p>
                    <xhtml:p>A productivity tool, SQL Developer's main objective is to help the end user save time and maximise the return on investment in the Oracle Database technology stack.</xhtml:p>
                    <xhtml:p>SQL Developer supports Oracle Database 10g, 11g, and 12c and will run on any operating system that supports Java.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>For the Developer</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>SQL Developer provides powerful editors for working with SQL, PL/SQL, Stored Java Procedures, and XML.</xhtml:p>
                            <xhtml:p>Run queries, generate execution plans, export data to the desired format (XML, Excel, HTML, PDF, etc.), execute, debug, test, and document your database programs, and much more.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>For the DBA</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>SQL Develper isn't just for developers! Since version 3.0, the DBA Panel (available under the View menu) has provided database administrators a set of interfaces for their most critical tasks.</xhtml:p>
                            <xhtml:p>SQL Developer will continue to add and enhance features for the DBA. Today,  SQL Developer's DBA panel provides support for:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>Data Pump.</xhtml:li>
                                <xhtml:li>Recovery Manager (RMAN).</xhtml:li>
                                <xhtml:li>Oracle Auditing.</xhtml:li>
                                <xhtml:li>User and Role management.</xhtml:li>
                                <xhtml:li>Storage management, including the ability to add space to your tablespaces.</xhtml:li>
                                <xhtml:li>Resource Manager.</xhtml:li>
                                <xhtml:li>Diagnostic Pack features:</xhtml:li>
                            </xhtml:ul>
                            <xhtml:ul xhtml:class="Indent_step_bullet_ul_based">
                                <xhtml:li>Snapshots.</xhtml:li>
                                <xhtml:li>Baselines.</xhtml:li>
                                <xhtml:li>ADDM.</xhtml:li>
                                <xhtml:li>ASH.</xhtml:li>
                                <xhtml:li>AWR.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>For the Application Architect &amp; Data Modeler</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Oracle SQL Developer includes a complete data modeling solution with Oracle SQL Developer Data Modeler (SDDM) running inside the application (also available as a standalone and free installation).</xhtml:p>
                            <xhtml:p>SDDM supports:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>Logical, relational, physical, dimensional modeling.</xhtml:li>
                                <xhtml:li>Data Flow Diagrams.</xhtml:li>
                                <xhtml:li>DDL scripting.</xhtml:li>
                                <xhtml:li>Importing from data dictionaries, DDL scripts, Oracle Designer Repositories, and ERwin.</xhtml:li>
                                <xhtml:li>a Reporting Repository.</xhtml:li>
                                <xhtml:li>Versioning of your designs via Subversion.</xhtml:li>
                                <xhtml:li>Comparing models with generation of ALTER scripts.</xhtml:li>
                                <xhtml:li>a powerful search and reporting utility.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>For the Web Application Developer and Administrator</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Oracle SQL Developer allows you to administer Oracle Rest Data Services and for creating and altering your RESTful services.</xhtml:p>
                            <xhtml:p>Oracle SQL Developer integrates with Oracle APEX, allowing you to browse applications and perform other Application Express activities.</xhtml:p>
                            <xhtml:p>With Oracle SQL Developer you can browse, export and import, drop or deploy applications. There are a selection of Application Express reports and you can create your own custom reports.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>&#160;</xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about the tools we use <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_DevTools_GDE/Content/Topics/1_Introduction/01_DevDoc_Resource-DevTools_00.htm" xhtml:target="_blank" xhtml:title="Development Resources: Tools" xhtml:alt="Development Resources: Tools">here</xhtml:a>.</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>