﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>Three Amigos</MicroContentPhrase>
        <MicroContentPhrase>Story Kick Off huddles</MicroContentPhrase>
        <MicroContentPhrase>Triad</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>DevOps - Three Amigos</xhtml:h1>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                    <xhtml:p>Three amigos refers to the primary perspectives to examine an increment of work before, during, and after development.  Those perspectives are:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>
                            <xhtml:p>Business – What problem are we trying to solve?</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Development – How might we build a solution to solve that problem?</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Testing – What about this, what could possibly happen?</xhtml:p>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>People holding these different perspectives should collaborate to define what to do, and agree on how they know when it is done correctly.  The end result of such a collaboration results in a clearer description of an increment of work often in the form of examples, leading to a shared understanding for the team.</xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">George Dinwiddie originally used the term “three amigos” in this sense in a blog post titled <xhtml:a xhtml:href="http://blog.gdinwiddie.com/2009/06/17/if-you-dont-automate-acceptance-tests/" xhtml:target="_blank">If you don’t automate acceptance tests?</xhtml:a> Three Amigos is also known as Story Kick Off huddles, or the Triad. 2009: Further information can be found in <xhtml:a xhtml:href="https://www.stickyminds.com/better-software-magazine/three-amigos" xhtml:target="_blank">The Three Amigos: All for One and One for All</xhtml:a> by George Dinwiddie</xhtml:p>
                    <xhtml:p>It’s also good practice for to review increments of the product that have been implemented to make sure it’s correct from those different perspectives.</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>
                            <xhtml:p>Builds a shared understanding about the intent of an increment of work.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Identifies misunderstandings and confusion early and allows learning to happen sooner in the delivery of an increment of work.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Provides a reasonable guard rail for the number of people who should be involved in discussions about any given increment of work.</xhtml:p>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>The concept of three amigos intends to balance between no collaboration between people with different perspectives and involving an entire team in discussing all the details of every increment of work.</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>
                            <xhtml:p>Limiting three amigo discussions to only three people.  If there are other stakeholders who are relevant to a particular increment of work, include them in the discussion.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Expanding the three amigos discussion to the team.  The intent of this practice is to include each necessary perspective with as small a group as possible.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Three amigo discussions become regularly scheduled meetings and are treated as another ceremony for the team to do instead of as a handy guide of what perspectives should be included in a discussion about a particular increment of work.</xhtml:p>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>