<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>DevOps - Product Backlog</h1>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" target="_blank" title="Development Resources: IT Project Management" alt="Development Resources: IT Project Management">Development Resources: IT Project Management</a>.</p>
        <p>A product backlog is a list of the new features, changes to existing features, bug fixes, infrastructure changes or other activities that a team may deliver in order to achieve a specific outcome.</p>
        <p>The product backlog is the single authoritative source for things that a team works on. That means that nothing gets done that isn’t on the product backlog. Conversely, the presence of a product backlog item on a product backlog does not guarantee that it will be delivered. It represents an option the team has for delivering a specific outcome rather than a commitment.</p>
        <p>It should be cheap and fast to add a product backlog item to the product backlog, and it should be equally as easy to remove a product backlog item that does not result in direct progress to achieving the desired outcome or enable progress toward the outcome.</p>
        <p>Product backlog items take a variety of formats, with <a href="DevOps_UserStories.htm">user stories</a> being the most common. The team using the product backlog determines the format they chose to use and look to the backlog items as reminders of the aspects of a solution they may work on.</p>
        <p>Product backlog items vary in size and extent of detail based in large part in how soon a team will work on them. Those that a team will work on soon should be small in size and contain sufficient detail for the team to start work. The team may establish a <a href="DevOps_DefinitionOfReady.htm">definition of ready</a> to indicate their agreement on the information they’d like to have available in order to start working on a product backlog item. Product backlog items that are not slated for work may be fairly broad and have little detail.</p>
        <p>The sequence of product backlog items on a product backlog changes as a team gains a better understanding of the outcome and the identified solution. This reordering of existing product backlog items, the ongoing addition and removal of product backlog items, and the continuous refinement of product backlog items gives a product backlog its dynamic characteristic.</p>
        <p>A team owns its product backlog and may have a specific role – product owner – with the primary responsibility for maintaining the product backlog. The key activities in maintaining the product backlog include prioritizing product backlog items, deciding which product backlog items should be removed from the product backlog, and facilitating product backlog refinement.</p>
        <p>A product backlog can be an effective way for a team to communicate what they are working on and what they plan to work on next. Story Maps and information radiators can provide a clear picture of your backlog for the team and stakeholders.</p>
        <p>The product backlog can be represented in physical form using index cards or sticky notes, or or it may be represented in electronic form such as a text file, spreadsheet, or one of the many backlog management tools that exist. Electronic boards are the better option for a team that has remote members or collects a great deal of supplementary information about product backlog items. Physical boards offer the advantage of making the product backlog continuously visible and concrete during discussions around the product backlog.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Also Known As</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The product backlog is often referred to as simply the backlog. This entry clarifies the term as product backlog to avoid confusion with sprint backlogs, which are a related, but different concept.</p>
                <p>Scaled Agile Framework (SAFe) introduced a series of backlogs to replace the product backlog. Each type of backlog is intended to contain a specific granularity of backlog item based on a complicated backlog item hierarchy:</p>
                <ul>
                    <li>
                        <p>The <a href="http://scaledagileframework.com/portfolio-backlog/" target="_blank">portfolio backlog</a> contains the different initiatives an organisation is considering (referred to as <a href="https://www.agilealliance.org/epic-confusion/" target="_blank">epics</a>).</p>
                    </li>
                    <li>
                        <p>The <a href="https://www.scaledagileframework.com/program-and-solution-backlogs/" target="_blank">solution backlog</a> contains high level backlog items (referred to as capabilities and enablers) that represent aspects of a solution</p>
                    </li>
                    <li>
                        <p>The <a href="https://www.scaledagileframework.com/program-and-solution-backlogs/" target="_blank">program backlog</a> contains backlog items (referred to as features) that represent aspects of a solution.</p>
                    </li>
                    <li>
                        <p>The <a href="http://scaledagileframework.com/team-backlog/" target="_blank">team backlog</a> contains backlog items (user stories and others) that a team works on.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Product backlog items act as placeholders for future conversation about an option for achieving your desired outcome. That means a team doesn’t have to have an idea fully fleshed out before adding it to the product backlog. When a product backlog item is initially added to a product backlog it only needs to have enough information to remind the team what the option was. A product backlog item only needs to be fully described when a team is about to start work on it.</p>
                <p>The dynamic nature of a product backlog provides teams a way to manage their learning about the desired outcome and potential ways to deliver that outcome. The product backlog does not need to be complete when a team starts work, so the team can start with an initial idea and add new product backlog items as they learn more.</p>
                <p>Just because something is on a product backlog does not mean that it has to be delivered, so a team can remove product backlog items that they discover do not contribute toward achieving the desired outcome. This means that a team can avoid producing extraneous outputs that do not add value and spend their time working on truly valuable changes.</p>
                <p>Teams can use the product backlog to avoid wasting time debating whether an option is valuable or not based on limited information. When a new idea presents itself, the team can add a product backlog item as a reminder to investigate the idea further. The team can then prioritize consideration of that idea alongside other items, and remove the product backlog item if the idea proves to not provide progress toward the desired outcome.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The product backlog should not be confused with a requirements document. While it serves as an entry point to requirements information about the product it has some distinct differences from requirements documents:</p>
                <ul>
                    <li>
                        <p>Product backlog items are necessary but not sufficient to describe the intended changes to the product. A complete understanding of the product comes from conversations that occur about the individual product backlog items and supplementary information that a team chooses to record about the product backlog item.</p>
                    </li>
                    <li>
                        <p>In contrast to something that appears in a requirements document, the inclusion of a product backlog item in a product backlog does not guarantee that it will be delivered.</p>
                    </li>
                    <li>
                        <p>In contrast to a requirements document which is baselined and is expected not to change after a certain point, the product backlog evolves as understanding of the product evolves.</p>
                    </li>
                </ul>
                <p>If a team starts using an electronic tool before they have settled on their approach to product backlog management, the tool can drive a team’s approach to product backlog management. The team may also get hung up in how to use the tool rather than selecting the process that works best for them.</p>
                <p>It’s possible for a product backlog to get too large to be effectively managed. This happens if a team adds every idea that gets suggested for addressing the outcome but never explores the ideas or removes the items that won’t be delivered. Product backlogs can also grow to an unmanageable size if all large product backlog items are split into smaller product backlog items too far in advance of when the team will work on them.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p><a href="http://www.scrumguides.org/scrum-guide.html#artifacts-productbacklog" target="_blank">Product Backlog</a> from Scrum Guide.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
    </body>
</html>