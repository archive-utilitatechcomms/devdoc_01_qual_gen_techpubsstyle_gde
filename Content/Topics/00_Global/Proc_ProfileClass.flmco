﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent>
        <MicroContentPhrase>Profile Class</MicroContentPhrase>
        <MicroContentPhrase>PC</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:head>
                    <xhtml:link xhtml:href="../../Resources/TableStyles/TabMenuMap.css" xhtml:rel="stylesheet" MadCap:stylesheetType="table" />
                </xhtml:head>
                <xhtml:body>
                    <xhtml:h1>Process: Profile Class</xhtml:h1>
                    <xhtml:p>Every site that isn't measured every half hour is settled on a profile class, which illustrates the expected consumption pattern of electricity throughout the day.

</xhtml:p>
                    <xhtml:p>Each profile class has a load profile that represents the pattern of electricity usage by day and by year for the average customer in that profile class. Each profile class includes different consumption figures for summer and winter when volumes and patterns of electricity usage are different.
</xhtml:p>
                    <xhtml:p>
Profile classes 1 and 2 are for domestic premises and profile classes 3 to 8 are for non-domestic (commercial) premises. The eight generic profile classes were chosen as they represent large populations of similar customers. The consumption graphs for profile classes 1 and 2 (domestic customers) are shown below.
</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Profile Class 1 </MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>
                                <xhtml:img xhtml:src="00_IMG_profile_class_1.png" xhtml:style="mc-thumbnail: popup;" />
                            </xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Profile Class 2</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>
                                <xhtml:img xhtml:src="00_IMG_profile_class_2.png" xhtml:style="mc-thumbnail: popup;" />
                            </xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>The profiles are created by Elexon (<xhtml:a xhtml:href="http://www.elexon.co.uk/" xhtml:title="www.elexon.co.uk" xhtml:alt="www.elexon.co.uk" xhtml:target="_blank">www.elexon.co.uk</xhtml:a>) for each of the eight profile classes by randomly selecting sites and installing half-hourly meters or getting half-hourly consumption data directly from suppliers. These samples are designed to provide profiles that are representative of all meters in each profile class. The full list of profile classes is shown in the following table:</xhtml:p>
                    <xhtml:table xhtml:style="width: 100%;mc-table-style: url('../../Resources/TableStyles/TabMenuMap.css');margin-left: 0;margin-right: auto;" xhtml:class="TableStyle-TabMenuMap" xhtml:cellspacing="0">
                        <xhtml:col xhtml:style="width: 70px;" xhtml:class="TableStyle-TabMenuMap-Column-Column1" />
                        <xhtml:col xhtml:style="width: 500px;" xhtml:class="TableStyle-TabMenuMap-Column-Column1" />
                        <xhtml:thead>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Head-Header1">
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Profile Class</xhtml:th>
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Description</xhtml:th>
                            </xhtml:tr>
                        </xhtml:thead>
                        <xhtml:tbody>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">0</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">​Half-hourly metering is installed for large customers.</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">1</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">​Domestic unrestricted customers.</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">2</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">​Domestic Economy 7 customers.</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">3</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">​Non-domestic unrestricted customers.</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">4</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">​Non-domestic Economy 7 customers.</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">​Non-domestic maximum demand customers with a peak load factor of less than 20%.</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">6</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">​Non-domestic maximum demand customers with a peak load factor of between 20% and 30%.</xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyB-Column1-Body1">8</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyA-Column1-Body1">​Non-domestic maximum demand customers with a peak load factor of over 40%.</xhtml:td>
                            </xhtml:tr>
                        </xhtml:tbody>
                    </xhtml:table>
                    <xhtml:p>Customers in profile classes 5 to 8 are described as Maximum Demand (MD) customers. This refers to customers whose metering system has a register that gives the maximum demand for a given period. A peak load factor (LF) is the ratio, expressed as a percentage, of consumption in kWh during a given period to the number of kWh that would have been supplied had the maximum demand been maintained throughout the period.</xhtml:p>
                    <xhtml:p>Suppliers are responsible for allocating customers to the appropriate profile class.</xhtml:p>
                    <xhtml:p>The profile class of an individual customer is identified by the first two digits of their full 21-digit <xhtml:a xhtml:href="Sys_MPAN.htm#mpan" xhtml:title="Metering Point Administration Number (MPAN)" xhtml:alt="Metering Point Administration Number (MPAN)">Metering Point Administration Number (MPAN)</xhtml:a>, which is found on their bills.</xhtml:p>
                    <xhtml:p>
                        <xhtml:img xhtml:src="00_IMG_profile_class_in_mpan.png" xhtml:style="mc-thumbnail: popup;" />
                    </xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>