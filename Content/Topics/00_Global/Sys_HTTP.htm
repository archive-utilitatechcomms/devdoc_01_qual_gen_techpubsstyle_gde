﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../../Resources/TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1>System: HTTP</h1>
        <p>The <a href="https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol" target="_blank" title="Wikipedia: HTTP" alt="Wikipedia: HTTP">Hypertext Transfer Protocol (HTTP)</a> is an application layer protocol for distributed, collaborative, hypermedia information systems. HTTP is the foundation of data communication for the World Wide Web, where hypertext documents include hyperlinks to other resources that the user can easily access, for example by a mouse click or by tapping the screen in a web browser.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>HTTP Design</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>HTTP functions as a request–response protocol in the client–server computing model. A web browser, for example, may be the client and an application running on a computer hosting a website may be the server. The client submits an HTTP request message to the server. The server, which provides resources such as HTML files and other content, or performs other functions on behalf of the client, returns a response message to the client. The response contains completion status information about the request and may also contain requested content in its message body.</p>
                <p>A web browser is an example of a user agent (UA). Other types of user agent include the indexing software used by search providers (web crawlers), voice browsers, mobile apps, and other software that accesses, consumes, or displays web content.</p>
                <p>HTTP is designed to permit intermediate network elements to improve or enable communications between clients and servers. High-traffic websites often benefit from web cache servers that deliver content on behalf of upstream servers to improve response time. Web browsers cache previously accessed web resources and reuse them, when possible, to reduce network traffic. HTTP proxy servers at private network boundaries can facilitate communication for clients without a globally routable address, by relaying messages with external servers.</p>
                <p>HTTP is an application layer protocol designed within the framework of the Internet protocol suite. Its definition presumes an underlying and reliable transport layer protocol, and Transmission Control Protocol (TCP) is commonly used. However, HTTP can be adapted to use unreliable protocols such as the User Datagram Protocol (UDP), for example in HTTPU and Simple Service Discovery Protocol (SSDP).</p>
                <p>HTTP resources are identified and located on the network by Uniform Resource Locators (URLs), using the Uniform Resource Identifiers (URI's) schemes http and https. As defined in RFC 3986, URIs are encoded as hyperlinks in HTML documents, so as to form interlinked hypertext documents.</p>
                <p>HTTP/1.1 is a revision of the original HTTP (HTTP/1.0). In HTTP/1.0 a separate connection to the same server is made for every resource request. HTTP/1.1 can reuse a connection multiple times to download images, scripts, stylesheets, etc after the page has been delivered. HTTP/1.1 communications therefore experience less latency as the establishment of TCP connections presents considerable overhead.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>HTTP session</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>An HTTP session is a sequence of network request–response transactions. An HTTP client initiates a request by establishing a Transmission Control Protocol (TCP) connection to a particular port on a server (typically port 80, occasionally port 8080; see List of TCP and UDP port numbers). An HTTP server listening on that port waits for a client's request message. Upon receiving the request, the server sends back a status line, such as "HTTP/1.1 200 OK", and a message of its own. The body of this message is typically the requested resource, although an error message or other information may also be returned.</p>
                <ul>
                    <li>
                        <p><b>Persistent connections:</b> In HTTP/0.9 and 1.0, the connection is closed after a single request/response pair. In HTTP/1.1 a keep-alive-mechanism was introduced, where a connection could be reused for more than one request. Such persistent connections reduce request latency perceptibly because the client does not need to re-negotiate the TCP 3-Way-Handshake connection after the first request has been sent. Another positive side effect is that, in general, the connection becomes faster with time due to TCP's slow-start-mechanism.</p>
                    </li>
                </ul>
                <blockquote>
                    <p>Version 1.1 of the protocol also made bandwidth optimisation improvements to HTTP/1.0. For example, HTTP/1.1 introduced chunked transfer encoding to allow content on persistent connections to be streamed rather than buffered. HTTP pipelining further reduces lag time, allowing clients to send multiple requests before waiting for each response. Another addition to the protocol was byte serving, where a server transmits just the portion of a resource explicitly requested by a client.</p>
                </blockquote>
                <ul>
                    <li>
                        <p><b>HTTP session state:</b> HTTP is a stateless protocol. A stateless protocol does not require the HTTP server to retain information or status about each user for the duration of multiple requests. However, some web applications implement states or server side sessions using for instance HTTP cookies or hidden variables within web forms.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>HTTP authentication</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>HTTP provides multiple authentication schemes such as basic access authentication and digest access authentication which operate via a challenge–response mechanism whereby the server identifies and issues a challenge before serving the requested content.</p>
                <p>HTTP provides a general framework for access control and authentication, via an extensible set of challenge–response authentication schemes, which can be used by a server to challenge a client request and by a client to provide authentication information.</p>
                <p>The HTTP Authentication specification also provides an arbitrary, implementation-specific construct for further dividing resources common to a given root URI. The realm value string, if present, is combined with the canonical root URI to form the protection space component of the challenge. This in effect allows the server to define separate authentication scopes under one root URI.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Message format</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The client sends requests to the server and the server sends responses. The request message consists of the following:</p>
                <ul>
                    <li>
                        <p>A request line (e.g., GET /images/logo.png HTTP/1.1, which requests a resource called /images/logo.png from the server).</p>
                    </li>
                    <li>
                        <p>Request header fields (e.g., Accept-Language: en).</p>
                    </li>
                    <li>
                        <p>An empty line</p>
                    </li>
                    <li>
                        <p>An optional message body</p>
                    </li>
                </ul>
                <p>The request line and other header fields must each end with &lt;CR&gt;&lt;LF&gt; (that is, a carriage return character followed by a line feed character). The empty line must consist of only &lt;CR&gt;&lt;LF&gt; and no other whitespace. In the HTTP/1.1 protocol, all header fields except Host are optional.</p>
                <p>A request line containing only the path name is accepted by servers to maintain compatibility with HTTP clients before the HTTP/1.0 specification in RFC 1945.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Request methods</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>HTTP defines methods (sometimes referred to as verbs, but nowhere in the specification does it mention verb, nor is OPTIONS or HEAD a verb) to indicate the desired action to be performed on the identified resource. What this resource represents, whether pre-existing data or data that is generated dynamically, depends on the implementation of the server. Often, the resource corresponds to a file or the output of an executable residing on the server. The HTTP/1.0 specification defined the GET, HEAD and POST methods and the HTTP/1.1 specification added five new methods: OPTIONS, PUT, DELETE, TRACE and CONNECT. By being specified in these documents, their semantics are well-known and can be depended on. Any client can use any method and the server can be configured to support any combination of methods. If a method is unknown to an intermediate, it will be treated as an unsafe and non-idempotent method. There is no limit to the number of methods that can be defined and this allows for future methods to be specified without breaking existing infrastructure. For example, WebDAV defined seven new methods and RFC 5789 specified the PATCH method.</p>
                        <p>Method names are case sensitive.This is in contrast to HTTP header field names which are case-insensitive.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>GET</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>The GET method requests a representation of the specified resource. Requests using GET should only retrieve data and should have no other effect. (This is also true of some other HTTP methods.)</p>
                        <p>The W3C has published guidance principles on this distinction, saying, "Web application design should be informed by the above principles, but also by the relevant limitations."</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>HEAD</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>The HEAD method asks for a response identical to that of a GET request, but without the response body. This is useful for retrieving meta-information written in response headers, without having to transport the entire content.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>POST</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>The POST method requests that the server accept the entity enclosed in the request as a new subordinate of the web resource identified by the URI. The data POSTed might be, for example, an annotation for existing resources; a message for a bulletin board, newsgroup, mailing list, or comment thread; a block of data that is the result of submitting a web form to a data-handling process; or an item to add to a database.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>PUT</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>The PUT method requests that the enclosed entity be stored under the supplied URI. If the URI refers to an already existing resource, it is modified; if the URI does not point to an existing resource, then the server can create the resource with that URI.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>DELETE</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>The DELETE method deletes the specified resource.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>TRACE</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>The TRACE method echoes the received request so that a client can see what (if any) changes or additions have been made by intermediate servers.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>OPTIONS</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>The OPTIONS method returns the HTTP methods that the server supports for the specified URL. This can be used to check the functionality of a web server by requesting '*' instead of a specific resource.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>CONNECT</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>The CONNECT method converts the request connection to a transparent TCP/IP tunnel, usually to facilitate SSL-encrypted communication (HTTPS) through an unencrypted HTTP proxy.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>PATCH</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>The PATCH method applies partial modifications to a resource.</p>
                        <p>All general-purpose HTTP servers are required to implement at least the GET and HEAD methods, and all other methods are considered optional by the specification.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Safe methods</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Some of the methods (for example, GET, HEAD, OPTIONS and TRACE) are, by convention, defined as safe, which means they are intended only for information retrieval and should not change the state of the server. In other words, they should not have side effects, beyond relatively harmless effects such as logging, web caching, the serving of banner advertisements or incrementing a web counter. Making arbitrary GET requests without regard to the context of the application's state should therefore be considered safe. However, this is not mandated by the standard, and it is explicitly acknowledged that it cannot be guaranteed.</p>
                <p>By contrast, methods such as POST, PUT, DELETE and PATCH are intended for actions that may cause side effects either on the server, or external side effects such as financial transactions or transmission of email. Such methods are therefore not usually used by conforming web robots or web crawlers; some that do not conform tend to make requests without regard to context or consequences.</p>
                <p>Despite the prescribed safety of GET requests, in practice their handling by the server is not technically limited in any way. Therefore, careless or deliberate programming can cause non-trivial changes on the server. This is discouraged, because it can cause problems for web caching, search engines and other automated agents, which can make unintended changes on the server. For example, a website might allow deletion of a resource through a URL such as http://example.com/article/1234/delete, which, if arbitrarily fetched, even using GET, would simply delete the article.</p>
                <p>One example of this occurring in practice was during the short-lived Google Web Accelerator beta, which prefetched arbitrary URLs on the page a user was viewing, causing records to be automatically altered or deleted en masse. The beta was suspended only weeks after its first release, following widespread criticism.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Idempotent methods and web applications</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Methods PUT and DELETE are defined to be idempotent, meaning that multiple identical requests should have the same effect as a single request. Methods GET, HEAD, OPTIONS and TRACE, being prescribed as safe, should also be idempotent, as HTTP is a stateless protocol.</p>
                <p>In contrast, the POST method is not necessarily idempotent, and therefore sending an identical POST request multiple times may further affect state or cause further side effects (such as financial transactions). In some cases this may be desirable, but in other cases this could be due to an accident, such as when a user does not realise that their action will result in sending another request, or they did not receive adequate feedback that their first request was successful. While web browsers may show alert dialog boxes to warn users in some cases where reloading a page may re-submit a POST request, it is generally up to the web application to handle cases where a POST request should not be submitted more than once.</p>
                <p>Note that whether a method is idempotent is not enforced by the protocol or web server. It is perfectly possible to write a web application in which (for example) a database insert or other non-idempotent action is triggered by a GET or other request. Ignoring this recommendation, however, may result in undesirable consequences, if a user agent assumes that repeating the same request is safe when it is not.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Security</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The TRACE method can be used as part of a class of attacks known as cross-site tracing; for that reason, common security advice is for it to be disabled in the server configuration. Microsoft IIS supports a proprietary "TRACK" method, which behaves similarly, and which is likewise recommended to be disabled.</p>
                <table style="width: 100%;margin-left: 0;margin-right: auto;mc-table-style: url('../../Resources/TableStyles/TabMenuMap.css');" class="TableStyle-TabMenuMap" cellspacing="0">
                    <col class="TableStyle-TabMenuMap-Column-Column1" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" />
                    <thead>
                        <tr class="TableStyle-TabMenuMap-Head-Header1">
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">HTTP Method</th>
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">RFC</th>
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Request has Body</th>
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Response has Body</th>
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Safe</th>
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Idempotent</th>
                            <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Cacheable</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><b>GET</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><a href="https://en.wikipedia.org/wiki/RFC_(identifier)" target="_blank">RFC</a> <a href="https://tools.ietf.org/html/rfc7231" target="_blank">7231</a></td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Optional</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Yes</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><b>HEAD</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><a href="https://en.wikipedia.org/wiki/RFC_(identifier)" target="_blank">RFC</a> <a href="https://tools.ietf.org/html/rfc7231" target="_blank">7231</a></td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Optional</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">No</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Yes</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><b>POST</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><a href="https://en.wikipedia.org/wiki/RFC_(identifier)" target="_blank">RFC</a> <a href="https://tools.ietf.org/html/rfc7231" target="_blank">7231</a></td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">No</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">No</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Yes</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><b>PUT</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><a href="https://en.wikipedia.org/wiki/RFC_(identifier)" target="_blank">RFC</a> <a href="https://tools.ietf.org/html/rfc7231" target="_blank">7231</a></td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">No</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">No</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><b>DELETE</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><a href="https://en.wikipedia.org/wiki/RFC_(identifier)" target="_blank">RFC</a> <a href="https://tools.ietf.org/html/rfc7231" target="_blank">7231</a></td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Optional</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">No</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">No</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><b>CONNECT</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><a href="https://en.wikipedia.org/wiki/RFC_(identifier)" target="_blank">RFC</a> <a href="https://tools.ietf.org/html/rfc7231" target="_blank">7231</a></td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Optional</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">No</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">No</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">No</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><b>OPTIONS</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><a href="https://en.wikipedia.org/wiki/RFC_(identifier)" target="_blank">RFC</a> <a href="https://tools.ietf.org/html/rfc7231" target="_blank">7231</a></td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Optional</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">No</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><b>TRACE</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><a href="https://en.wikipedia.org/wiki/RFC_(identifier)" target="_blank">RFC</a> <a href="https://tools.ietf.org/html/rfc7231" target="_blank">7231</a></td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">No</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">No</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><b>PATCH</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><a href="https://en.wikipedia.org/wiki/RFC_(identifier)" target="_blank">RFC</a> <a href="https://tools.ietf.org/html/rfc5789" target="_blank">5789</a></td>
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">Yes</td>
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">No</td>
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">No</td>
                            <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">No</td>
                        </tr>
                    </tbody>
                </table>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Response message</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The response message consists of the following:</p>
                <ul>
                    <li>
                        <p>A status line which includes the status code and reason message (e.g., HTTP/1.1 200 OK, which indicates that the client's request succeeded).</p>
                    </li>
                    <li>
                        <p>Response header fields (e.g., Content-Type: text/html).</p>
                    </li>
                    <li>
                        <p>An empty line.</p>
                    </li>
                    <li>
                        <p>An optional message body.</p>
                    </li>
                </ul>
                <p>The status line and other header fields must all end with &lt;CR&gt;&lt;LF&gt;. The empty line must consist of only &lt;CR&gt;&lt;LF&gt; and no other whitespace. This strict requirement for &lt;CR&gt;&lt;LF&gt; is relaxed somewhat within message bodies for consistent use of other system linebreaks such as &lt;CR&gt; or &lt;LF&gt; alone.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Status codes</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>In HTTP/1.0 and since, the first line of the HTTP response is called the status line and includes a numeric status code (such as "404") and a textual reason phrase (such as "Not Found"). The way the user agent handles the response depends primarily on the code, and secondarily on the other response header fields. Custom status codes can be used, for if the user agent encounters a code it does not recognise, it can use the first digit of the code to determine the general class of the response.</p>
                        <p>The standard reason phrases are only recommendations, and can be replaced with "local equivalents" at the web developer's discretion. If the status code indicated a problem, the user agent might display the reason phrase to the user to provide further information about the nature of the problem. The standard also allows the user agent to attempt to interpret the reason phrase, though this might be unwise since the standard explicitly specifies that status codes are machine-readable and reason phrases are human-readable. HTTP status code is primarily divided into five groups for better explanation of request and responses between client and server as named:</p>
                        <ul>
                            <li>
                                <p>Informational <code>1XX</code></p>
                            </li>
                            <li>
                                <p>Successful <code>2XX</code></p>
                            </li>
                            <li>
                                <p>Redirection <code>3XX</code></p>
                            </li>
                            <li>
                                <p>Client Error <code>4XX</code></p>
                            </li>
                            <li>
                                <p>Server Error <code>5XX</code></p>
                            </li>
                        </ul>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Encrypted connections</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The most popular way of establishing an encrypted HTTP connection is HTTPS. Two other methods for establishing an encrypted HTTP connection also exist: Secure Hypertext Transfer Protocol, and using the HTTP/1.1 Upgrade header to specify an upgrade to TLS. Browser support for these two is, however, nearly non-existent.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Example session</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Below is a sample conversation between an HTTP client and an HTTP server running on www.example.com, port 80.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Client request</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody><pre class="code"><span style="color: #0000ff;">GET /</span> <span style="color: #008000;">HTTP/1.1</span><br /><span style="color: #008000;">Host:</span> www.example.com</pre>
                        <p>A client request (consisting in this case of the request line and only one header field) is followed by a blank line, so that the request ends with a double newline, each in the form of a carriage return followed by a line feed. The "Host" field distinguishes between various DNS names sharing a single IP address, allowing name-based virtual hosting. While optional in HTTP/1.0, it is mandatory in HTTP/1.1. (The "/" means /index.html if there is one.)</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Server response</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody><pre class="code">HTTP/1.1 200 OK
Date: Mon, 23 May 2005 22:38:34 GMT
Content-Type: text/html; charset=UTF-8
Content-Length: 155
Last-Modified: Wed, 08 Jan 2003 23:11:55 GMT
Server: Apache/1.3.3.7 (Unix) (Red-Hat/Linux)
ETag: "3f80f-1b6-3e1cb03b"
Accept-Ranges: bytes
Connection: close

&lt;html&gt;
  &lt;head&gt;
    &lt;title&gt;An Example Page&lt;/title&gt;
  &lt;/head&gt;
  &lt;body&gt;
    &lt;p&gt;Hello World, this is a very simple HTML document.&lt;/p&gt;
  &lt;/body&gt;
&lt;/html&gt;</pre>
                        <p>The ETag (entity tag) header field is used to determine if a cached version of the requested resource is identical to the current version of the resource on the server. Content-Type specifies the Internet media type of the data conveyed by the HTTP message, while Content-Length indicates its length in bytes. The HTTP/1.1 webserver publishes its ability to respond to requests for certain byte ranges of the document by setting the field Accept-Ranges: bytes. This is useful, if the client needs to have only certain portions of a resource sent by the server, which is called byte serving. When Connection: close is sent, it means that the web server will close the TCP connection immediately after the transfer of this response.</p>
                        <p>Most of the header lines are optional. When Content-Length is missing the length is determined in other ways. Chunked transfer encoding uses a chunk size of 0 to mark the end of the content. Identity encoding without Content-Length reads content until the socket is closed. A Content-Encoding like gzip can be used to compress the transmitted data.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>