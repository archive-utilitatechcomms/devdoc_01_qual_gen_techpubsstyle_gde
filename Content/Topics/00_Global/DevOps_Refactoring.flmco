﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>Refactoring</MicroContentPhrase>
        <MicroContentPhrase>Extract Method</MicroContentPhrase>
        <MicroContentPhrase>Introduce Parameter</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>DevOps - Refactoring</xhtml:h1>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                    <xhtml:p>Refactoring consists of improving the internal structure of an existing program’s source code, while preserving its external behavior.</xhtml:p>
                    <xhtml:p>The noun “refactoring” refers to one particular behavior-preserving transformation, such as “Extract Method” or “Introduce Parameter.”</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Refactoring does “not” mean:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Rewriting code.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Fixing bugs.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Improve observable aspects of software such as its interface.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>Refactoring in the absence of safeguards against introducing defects (i.e. violating the “behaviour preserving” condition) is risky. Safeguards include aids to regression testing including automated unit tests or automated acceptance tests, and aids to formal reasoning such as type systems.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The following are claimed benefits of refactoring:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Refactoring improves objective attributes of code (length, duplication, coupling and cohesion, cyclomatic complexity) that correlate with ease of maintenance.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Refactoring helps code understanding.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Refactoring encourages each developer to think about and understand design decisions, in particular in the context of <xhtml:a xhtml:href="DevOps_CollectiveOwnership.htm" xhtml:target="_blank" xhtml:title=" Collective Ownership" xhtml:alt=" Collective Ownership">collective ownership / collective code ownership</xhtml:a>.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Refactoring favours the emergence of reusable design elements (such as design patterns) and code modules.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Signs Of Use</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p><xhtml:a xhtml:href="DevOps_VersionControl.htm" xhtml:target="_blank" xhtml:title="Version Control" xhtml:alt="Version Control">Version control records</xhtml:a> (such as CVS or git logs) include entries labeled “Refactoring”.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>The code modifications corresponding to such entries can be verified to be behaviour-neutral: no new unit tests or functional tests are introduced, for example.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Skill Levels</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Beginner</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>knows the definition of “refactoring”</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>can use some automated refactorings from the IDE</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>can perform some refactorings by hand</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>is aware of the risks of regression from manual and automated refactorings</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>is aware of code duplication and can remove it by refactoring</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Intermediate</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>knows and is able to remedy a broader range of “code smells”</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>can chain several refactorings to carry out a design intention, in awareness of the dependencies between refactorings</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>refactors continuously, rather than in sporadic and lengthy sessions</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Advanced</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>has an acute sense of code duplication and coupling</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>applies refactorings to non-code elements such as database schema, documents, etc.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>uses refactoring to guide large bodies of code toward design styles intentionally chosen from a broad palette: object-oriented, functional, or inspired by known design patterns</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Although the practice of refactoring has become popular, rigorously establishing its benefit in an academic context has proven thorny, illustrating a common gap between research and common practice.</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p><xhtml:a xhtml:href="http://win.ua.ac.be/~qsoeten/other/data/Soetens2010QUATIC.pdf" xhtml:target="_blank" xhtml:title="Studying the Effect of Refactorings: a Complexity Metrics Perspective" xhtml:alt="Studying the Effect of Refactorings: a Complexity Metrics Perspective">Studying the Effect of Refactorings: a Complexity Metrics Perspective</xhtml:a>, a 2010 study, finds surprisingly little correlation between refactoring episodes, as identified by version control logs, and decrease in cyclomatic complexity.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p><xhtml:a xhtml:href="http://www.amazon.com/dp/0201485672" xhtml:target="_blank" xhtml:title="Refactoring, by Martin Fowler" xhtml:alt="Refactoring, by Martin Fowler">Refactoring</xhtml:a>, by Martin Fowler.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p><xhtml:a xhtml:href="https://www.manning.com/books/the-mikado-method" xhtml:target="_blank" xhtml:title="The Mikado Method by Ola Ellnestam and Daniel Brolund" xhtml:alt="The Mikado Method by Ola Ellnestam and Daniel Brolund">The Mikado Method</xhtml:a> by Ola Ellnestam and Daniel Brolund.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>Such studies may be affected by methodological issues, such as identifying what counts as a “refactoring” when examining code histories after the fact; the above paper, for instance, finds that programmers often label “refactorings” sets of code changes which also include additional functionality or bug fixes.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>