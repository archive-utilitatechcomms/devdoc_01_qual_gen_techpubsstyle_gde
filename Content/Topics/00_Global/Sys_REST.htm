﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../../Resources/TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1>System: Representational State Transfer (REST)</h1>
        <p><a href="https://en.wikipedia.org/wiki/Representational_state_transfer" target="_blank" title="Wikipedia: Representational state transfer" alt="Wikipedia: Representational state transfer">Representational state transfer (REST)</a> is a de-facto standard for a software architecture of interactive applications that use Web services. A Web service that follows this standard is called RESTful. Such a Web service must provide its Web resources in a textual representation and allow them to be read and modified with a stateless protocol and a predefined set of operations. This standard allows interoperability between the computer systems on the Internet that provide these services. REST is an alternative to, for example, <a href="Sys_SOAP.htm" target="_blank">SOAP</a> as way to access a Web service.</p>
        <p>"Web resources" were first defined on the World Wide Web as documents or files identified by their URLs. Today, the definition is much more generic and abstract, and includes every thing, entity, or action that can be identified, named, addressed, handled, or performed in any way on the Web. In a RESTful Web service, requests made to a resource's URI elicit a response with a payload formatted in HTML, XML, JSON, or some other format. For example, the response can confirm that the resource state has been changed. The response can also include hypertext links to related resources. The most common protocol for these requests and responses is HTTP. It provides operations (HTTP methods) GET, HEAD, POST, PUT, PATCH, DELETE, CONNECT, OPTIONS and TRACE.</p>
        <p>By using a stateless protocol and standard operations, RESTful systems aim for fast performance, reliability, and the ability to grow by reusing components that can be managed and updated without affecting the system as a whole, even while it is running.</p>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">Unlike <a href="Sys_SOAP.htm" target="_blank">SOAP-based web services</a>, there is no "official" standard for RESTful web APIs. This is because REST is an architectural style, while SOAP is a protocol. REST is not a standard in itself, but RESTful implementations make use of standards, such as HTTP, URI, JSON, and XML. Many developers also describe their APIs as being RESTful, even though these APIs actually don't fulfil all of the architectural constraints described below (especially the uniform interface constraint).</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>History</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The term representational state transfer was introduced and defined in 2000 by Roy Fielding in his doctoral dissertation. Fielding's dissertation explained the REST principles that were known as the "<a href="Sys_HTTP.htm" target="_blank">HTTP</a> object model" beginning in 1994, and were used in designing the HTTP 1.1 and Uniform Resource Identifiers (URI) standards. The term is intended to evoke an image of how a well-designed Web application behaves: it is a network of Web resources (a virtual state-machine) where the user progresses through the application by selecting resource identifiers such as http://www.example.com/articles/21 and resource operations such as GET or POST (application state transitions), resulting in the next resource's representation (the next application state) being transferred to the end user for their use.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Architectural properties</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The constraints of the REST architectural style affect the following architectural properties:</p>
                <ul>
                    <li>
                        <p>Performance in component interactions, which can be the dominant factor in user-perceived performance and network efficiency.</p>
                    </li>
                </ul>
                <ul>
                    <li>
                        <p>Scalability allowing the support of large numbers of components and interactions among components. Roy Fielding describes REST's effect on scalability as follows:</p>
                    </li>
                </ul>
                <blockquote>
                    <p><i>REST's client–server separation of concerns simplifies component implementation, reduces the complexity of connector semantics, improves the effectiveness of performance tuning, and increases the scalability of pure server components. Layered system constraints allow intermediaries - proxies, gateways, and firewalls - to be introduced at various points in the communication without changing the interfaces between components, thus allowing them to assist in communication translation or improve performance via large-scale, shared caching. REST enables intermediate processing by constraining messages to be self-descriptive: interaction is stateless between requests, standard methods and media types are used to indicate semantics and exchange information, and responses explicitly indicate cacheability.</i>
                    </p>
                </blockquote>
                <ul>
                    <li>
                        <p>Simplicity of a uniform interface.</p>
                    </li>
                </ul>
                <ul>
                    <li>
                        <p>Modifiability of components to meet changing needs (even while the application is running).</p>
                    </li>
                </ul>
                <ul>
                    <li>
                        <p>Visibility of communication between components by service agents.</p>
                    </li>
                </ul>
                <ul>
                    <li>
                        <p>Portability of components by moving program code with the data.</p>
                    </li>
                </ul>
                <ul>
                    <li>
                        <p>Reliability in the resistance to failure at the system level in the presence of failures within components, connectors, or data.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Architectural constraints</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Six guiding constraints define a RESTful system. These constraints restrict the ways that the server can process and respond to client requests so that, by operating within these constraints, the system gains desirable non-functional properties, such as performance, scalability, simplicity, modifiability, visibility, portability, and reliability. If a system violates any of the required constraints, it cannot be considered RESTful. The formal REST constraints are as follows:</p>
                <ol>
                    <li>
                        <p><a href="https://en.wikipedia.org/wiki/Client–server_model" target="_blank" title="Client–server model" alt="Client–server model" style="font-weight: bold;">Client–server architecture</a>: The principle behind the client–server constraints is the separation of concerns. Separating the user interface concerns from the data storage concerns improves the portability of the user interfaces across multiple platforms. It also improves scalability by simplifying the server components. Perhaps most significant to the Web is that the separation allows the components to evolve independently, thus supporting the Internet-scale requirement of multiple organisational domains.</p>
                    </li>
                    <li>
                        <p><a href="https://en.wikipedia.org/wiki/Stateless_protocol" target="_blank" title="Stateless protocol" alt="Stateless protocol" style="font-weight: bold;">Statelessness</a>: In a client–server interaction, state is made up of intrinsic state and extrinsic state. Intrinsic state, called resource state, is stored on the server and consists of information that is independent of the server’s context, thereby making it sharable to all clients of the server. Extrinsic state, called application state, is stored on each client and consists of information that is dependent on the server’s context and therefore cannot be shared. Clients are responsible for passing application state to the server when it needs it. The constraint of storing application state on the client rather than on the server makes the communication stateless.</p>
                    </li>
                    <li>
                        <p><a href="https://en.wikipedia.org/wiki/Web_cache" target="_blank" title="Web cache" alt="Web cache" style="font-weight: bold;">Cacheability</a>: As on the World Wide Web, clients and intermediaries can <a href="Sys_Cache.htm" target="_blank">cache</a> responses. Responses must, implicitly or explicitly, define themselves as either cacheable or non-cacheable to prevent clients from providing stale or inappropriate data in response to further requests. Well-managed caching partially or completely eliminates some client–server interactions, further improving scalability and performance</p>
                    </li>
                    <li>
                        <p><a href="https://en.wikipedia.org/wiki/Layered_system" target="_blank" title="Layered system" alt="Layered system" style="font-weight: bold;">Layered system</a>: A client cannot ordinarily tell whether it is connected directly to the end server or to an intermediary along the way. If a proxy or <a href="Sys_LoadBalancing.htm" target="_blank">load balancer</a> is placed between the client and server, it won't affect their communications, and there won't be a need to update the client or server code. Intermediary servers can improve system scalability by enabling load balancing and by providing shared <a href="Sys_Cache.htm" target="_blank">caches</a>. Also, security can be added as a layer on top of the web services, separating business logic from security logic. Adding security as a separate layer enforces security policies. Finally, intermediary servers can call multiple other servers to generate a response to the client.</p>
                    </li>
                    <li>
                        <p><a href="https://en.wikipedia.org/wiki/Client-side_scripting" target="_blank" title="Client-side scripting" alt="Client-side scripting" style="font-weight: bold;">Code on demand (optional)</a>: Servers can temporarily extend or customise the functionality of a client by transferring executable code: for example, compiled components such as Java applets, or client-side scripts such as JavaScript.</p>
                    </li>
                    <li>
                        <p><a href="https://stackoverflow.com/questions/25172600/rest-what-exactly-is-meant-by-uniform-interface" target="_blank" title="Stack Overflow: REST - What exactly is meant by Uniform Interface?" alt="Stack Overflow: REST - What exactly is meant by Uniform Interface?" style="font-weight: bold;">Uniform interface</a>: The uniform interface constraint is fundamental to the design of any RESTful system. It simplifies and decouples the architecture, which enables each part to evolve independently. The four constraints for this uniform interface are:</p>
                        <ul>
                            <li>
                                <p><b>Resource identification in requests:</b> Individual resources are identified in requests, for example using URIs in RESTful Web services. The resources themselves are conceptually separate from the representations that are returned to the client. For example, the server could send data from its database as HTML, XML or as JSON - none of which are the server's internal representation.</p>
                            </li>
                            <li>
                                <p><b>Resource manipulation through representations:</b> When a client holds a representation of a resource, including any metadata attached, it has enough information to modify or delete the resource's state.</p>
                            </li>
                            <li>
                                <p><b>Self-descriptive messages:</b> Each message includes enough information to describe how to process the message. For example, which parser to invoke can be specified by a media type.</p>
                            </li>
                            <li>
                                <p><b>Hypermedia as the engine of application state (</b><a href="https://en.wikipedia.org/wiki/HATEOAS" target="_blank" style="font-weight: bold;">HATEOAS</a><b>):</b> Having accessed an initial URI for the REST application - analogous to a human Web user accessing the home page of a website - a REST client should then be able to use server-provided links dynamically to discover all the available resources it needs. As access proceeds, the server responds with text that includes hyperlinks to other resources that are currently available. There is no need for the client to be hard-coded with information regarding the structure or dynamics of the application.</p>
                            </li>
                        </ul>
                    </li>
                </ol>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Classification models applied to to web services</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Several models have been developed to help classify REST APIs according to their adherence to various principles of REST design, such as the <a href="https://en.wikipedia.org/wiki/Richardson_Maturity_Model" target="_blank" title="Wikipedia: Richardson Maturity Model" alt="Wikipedia: Richardson Maturity Model">Richardson Maturity Model</a>.</p>
                <p>Web service APIs that adhere to the REST architectural constraints are called RESTful APIs. HTTP-based RESTful APIs are defined with the following aspects:</p>
                <ul>
                    <li>
                        <p>A base URI, such as http://api.example.com/.</p>
                    </li>
                    <li>
                        <p>Standard HTTP methods (e.g., GET, POST, PUT, and DELETE).</p>
                    </li>
                    <li>
                        <p>A media type that defines state transition data elements (e.g., Atom, microformats, application/vnd.collection+json, etc.). The current representation tells the client how to compose requests for transitions to all the next available application states. This could be as simple as a URI or as complex as a Java applet.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Semantics of HTTP methods</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The following table shows how HTTP methods are intended to be used in HTTP APIs, including RESTful ones.</p>
                <table style="width: 100%;margin-left: 0;margin-right: auto;mc-table-style: url('../../Resources/TableStyles/TabMenuMap.css');" class="TableStyle-TabMenuMap" cellspacing="0">
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 50px;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 500px;" />
                    <thead>
                        <tr class="TableStyle-TabMenuMap-Head-Header1">
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">HTTP Method</th>
                            <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><b>GET</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get a representation of the target resource’s state.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><b>POST</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Let the target resource process the representation enclosed in the request.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><b>PUT</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">	Set the target resource’s state to the state defined by the representation enclosed in the request.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><b>DELETE</b>
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">Delete the target resource’s state.</td>
                        </tr>
                    </tbody>
                </table>
                <p>The GET method is safe, meaning that applying it to a resource does not result in a state change of the resource (read-only semantics). The GET, PUT, and DELETE methods are idempotent, meaning that applying them multiple times to a resource results in the same state change of the resource as applying them once, though the response might differ. The GET and POST methods are <a href="Sys_Cache.htm" target="_blank">cacheable</a>, meaning that responses to them are allowed to be stored for future reuse.</p>
                <p>The GET (read), PUT (create and update), and DELETE (delete) methods are <a href="https://en.wikipedia.org/wiki/Create,_read,_update_and_delete" target="_blank">CRUD</a> (Create / Read / Update / Delete) operations as they have storage management semantics, meaning that they let user agents directly manipulate the states of target resources. The POST method is not a CRUD operation but a process operation that has target-resource-specific semantics excluding storage management semantics, so it does not let user agents directly manipulate the states of target resources.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
    </body>
</html>