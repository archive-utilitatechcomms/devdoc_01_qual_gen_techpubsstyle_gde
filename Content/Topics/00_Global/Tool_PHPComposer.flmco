﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="php-composer">
        <MicroContentPhrase>PHP Composer</MicroContentPhrase>
        <MicroContentPhrase>Composer</MicroContentPhrase>
        <MicroContentPhrase>PHP</MicroContentPhrase>
        <MicroContentPhrase>Package Management</MicroContentPhrase>
        <MicroContentPhrase>Package</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>Tool: PHP Composer (Package Management)</xhtml:h1>
                    <xhtml:p><xhtml:a xhtml:href="https://getcomposer.org/" xhtml:target="_blank" xhtml:title="PHP Composer " xhtml:alt="PHP Composer ">Composer</xhtml:a> is an application-level package manager for the PHP programming language that provides a standard format for managing dependencies of PHP software and required libraries.</xhtml:p>
                    <xhtml:p>Composer runs through the command line and installs dependencies (e.g. libraries) for an application. It also allows users to install PHP applications that are available on "Packagist", which is its main repository containing available packages. It also provides autoload capabilities for libraries that specify autoload information to ease usage of third-party code.</xhtml:p>
                    <xhtml:p><xhtml:a xhtml:href="https://getcomposer.org/" xhtml:target="_blank" xhtml:title="PHP Composer " xhtml:alt="PHP Composer ">Composer</xhtml:a> is a tool for dependency management in PHP. It allows you to declare the libraries your project depends on and it will manage (install / update) them for you.</xhtml:p>
                    <xhtml:p>However, Composer is not a package manager in the same sense as <xhtml:a xhtml:href="http://yum.baseurl.org/" xhtml:title="YUm Packet Manager" xhtml:alt="YUm Packet Manager" xhtml:target="_blank">Yum</xhtml:a> or <xhtml:a xhtml:href="https://manpages.debian.org/stretch/apt/apt.8.en.html" xhtml:title="Apt Package Manager" xhtml:alt="Apt Package Manager" xhtml:target="_blank">Apt</xhtml:a> are. Yes, it deals with "packages" or libraries, but it manages them on a per-project basis, installing them in a directory (e.g. vendor) inside your project. By default it does not install anything globally. Thus, it is a dependency manager. It does however support a "global" project for convenience via the global command.</xhtml:p>
                    <xhtml:p>This idea is not new and Composer is strongly inspired by node's npm and ruby's bundler.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Suppose:</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ol>
                                <xhtml:li>You have a project that depends on a number of libraries.</xhtml:li>
                                <xhtml:li>Some of those libraries depend on other libraries.</xhtml:li>
                            </xhtml:ol>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Composer:</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ol>
                                <xhtml:li>Enables you to declare the libraries you depend on.</xhtml:li>
                                <xhtml:li>Finds out which versions of which packages can and need to be installed, and installs them (meaning it downloads them into your project).</xhtml:li>
                            </xhtml:ol>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>&#160;</xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about the tools we use <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_DevTools_GDE/Content/Topics/1_Introduction/01_DevDoc_Resource-DevTools_00.htm" xhtml:target="_blank" xhtml:title="Development Resources: Tools" xhtml:alt="Development Resources: Tools">here</xhtml:a>.</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>