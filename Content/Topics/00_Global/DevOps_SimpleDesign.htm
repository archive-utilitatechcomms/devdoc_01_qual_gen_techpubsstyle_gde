﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>DevOps - Simple Design</h1>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" target="_blank" title="Development Resources: IT Project Management" alt="Development Resources: IT Project Management">Development Resources: IT Project Management</a>.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Principles</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>A team adopting the “simple design” practice bases its software design strategy on the following principles:</p>
                <ul>
                    <li>
                        <p>Design is an ongoing activity, which includes refactoring and heuristics such as YAGNI.</p>
                    </li>
                    <li>
                        <p>Design quality is evaluated based on the rules of code simplicity.</p>
                    </li>
                    <li>
                        <p>All design elements such as “design patterns”, plugin-based architectures, etc. are seen as having costs as well as benefits, and design costs must be justified.</p>
                    </li>
                    <li>
                        <p>Design decisions should be deferred until the “last responsible moment”, so as to collect as much information as possible on the benefits of the chosen option before incurring its costs.</p>
                    </li>
                </ul>
                <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">The phrase YAGNI is associated with <a href="DevOps_ExtremeProgramming.htm" target="_blank" title="Extreme Programming" alt="Extreme Programming"><a href="DevOps_ExtremeProgramming.htm">Extreme Programming</a></a> from its earliest days.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Also Known As</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>The practice is often reduced to the acronym YAGNI, for “You Aren’t Gonna Need It”; this alludes to the usual counter-argument when a programmer tries to propose a costly design element based on its future benefits only (“We’re going to need this Factory sooner or later, we might as well put it in now.” “No, you aren’t gonna need it.”),</p>
                    </li>
                    <li>
                        <p>Another common term is “emergent design”, emphasising that good global design can result from consistently paying attention to the local qualities of code structure (by analogy with the processes studied by complexity theorists where purely local rules reliably give rise to consistent global properties),</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>The first (and fatal) mistake would be to downplay, for instance while staffing a team, the importance of significant design skill among the team, on the basis that “the design will emerge”; emergence is not magic and such skills are still crucial.</p>
                    </li>
                    <li>
                        <p>“Simple design” only refers to the “software” design; it is misleading to invoke the simple design practice to justify decisions which have to do with a customer or <a href="DevOps_ProductOwner.htm" target="_blank" title="Product owner" alt="Product owner">product owner’s</a> requirements, or usability considerations.</p>
                    </li>
                    <li>
                        <p>The practice may be ill-advised when design practices are unlikely to be consistent, for instance if the development group is large and geographically distributed.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Signs Of Use</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>The team maintains a “backlog” of design-specific tasks:</p>
                        <ul>
                            <li>
                                <p>Design flaws requiring an explicit remediation through refactoring.</p>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <p>Opportunities to simplify existing code.</p>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <p>Potential design decisions deferred until more information comes in.</p>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <p>This backlog doesn’t remain stagnant, or a mere list of complaints never addressed; the team sets aside enough of its productive time to regularly address the issues (or decide to live with them).</p>
                    </li>
                    <li>
                        <p>The team regularly uses one or more of the ancillary practices which offer explicit opportunities to discuss design.</p>
                    </li>
                    <li>
                        <p>Any impression that relatively straightforward functional changes require inordinate amounts of effort to implement is a “warning sign” that the practice may not be effectively used.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>Mitigates the common risk of overdesign (“gold plating”).</p>
                    </li>
                    <li>
                        <p>Has been claimed to “flatten the cost of change curve” – i.e. to keep the software easy to change, because all design decisions are agnostic to which particular changes are expected.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Skill Levels</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>As an individual contributor:</p>
                <ul>
                    <li>
                        <p><b>Beginner</b>: able to identify design elements which do not “pull their weight” (aren’t beneficial enough to justify their complexity) and refactor to simplify the code structure.</p>
                    </li>
                    <li>
                        <p><b>Intermediate</b>: able to defer a design decision which may be required by a future requirement, and to identify the conditions under which the decision should be arbitrated.</p>
                    </li>
                    <li>
                        <p><b>Advanced</b>: able to identify the right moment to introduce a far-reaching design decision, such as a plugins-based architecture in a desktop application.</p>
                    </li>
                </ul>
                <p>On the team level, the litmus test is whether the team is able to “share” design decisions, so that these are not based on individual decrees by the architect or lead programmer, but understood and carried out by all developers on the team.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p><a href="http://www.martinfowler.com/articles/designDead.html" target="_blank" title="Is Design Dead?" alt="Is Design Dead?">Is Design Dead?</a>, by Martin Fowler, remains one of the most perceptive discussions of how Agile teams approach design.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>