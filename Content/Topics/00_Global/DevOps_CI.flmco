﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="what-is-micro-content">
        <MicroContentPhrase>Continuous Integration</MicroContentPhrase>
        <MicroContentPhrase>CI</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>DevOps - Continuous Integration (CI)</xhtml:h1>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Development Resources: IT Project Management" xhtml:alt="Development Resources: IT Project Management">Development Resources: IT Project Management</xhtml:a>.</xhtml:p>
                    <xhtml:p>Teams practising continuous <xhtml:a xhtml:href="DevOps_Integration.htm" xhtml:target="_blank" xhtml:title="Integration" xhtml:alt="Integration">integration</xhtml:a> seek two objectives:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>
                            <xhtml:p>Minimise the duration and effort required by each integration episode.</xhtml:p>
                        </xhtml:li>
                        <xhtml:li>
                            <xhtml:p>Be able to deliver a product version suitable for release at any moment.</xhtml:p>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>In practice, this dual objective requires an integration procedure which is reproducible at the very least, and largely automated. This is achieved through <xhtml:a xhtml:href="DevOps_VersionControl.htm" xhtml:target="_blank" xhtml:title="Version Control" xhtml:alt="Version Control">version control</xhtml:a> tools, team policies and conventions, and tools specifically designed to help achieve continuous integration.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>As suggested above, the practice of continuous integration should not be confused with the tools that assist it (CI servers such as Cruise Control, Hudson, etc.). Continuous integration is first and foremost a matter of attitude rather than tools, and it relies on more than one kind of tool: tools for testing, tools for automating build processes, and tools for version control.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Continuous integration aims to lessen the pain of integration by increasing its frequency. Therefore, “any” effort related to producing intermediate releases, and which the team experiences as particularly burdensome, is a candidate for inclusion in the team’s continuous integration process. (This is the reasoning that leads teams to <xhtml:a xhtml:href="DevOps_ContinuousDeployment.htm" xhtml:target="_blank" xhtml:title="Continuous Deployment" xhtml:alt="Continuous Deployment">continuous deployment</xhtml:a>.)</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>1993: the phrase “continuous integration” is already in use and thus predates what will later be known as Agile processes, for instance an article contrasts it with “scheduled” integration, and recommends the latter, citing “lack of thorough testing” as one issue with continuous integration; this helps explain why the automated testing favored by Agile teams is an enabler for continuous integration.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>1996: Steve McConnell <xhtml:a xhtml:href="https://stevemcconnell.com/articles/daily-build-and-smoke-test/" xhtml:target="_blank">describes</xhtml:a> the “Daily Build and Smoke Test” technique, used at Microsoft for Windows NT 3.0 during the 1990’s; the emphasis is not so much on the automation as on the frequency, the daily cycle being at that time considered “extreme”.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>1998: continuous integration is listed among the core practices of Extreme Programming.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>2000: an article by Martin Fowler provides perhaps the <xhtml:a xhtml:href="https://www.martinfowler.com/articles/originalContinuousIntegration.html" xhtml:target="_blank">most complete description</xhtml:a> of the continuous integration practice available at that time.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>2001: <xhtml:a xhtml:href="http://cruisecontrol.sourceforge.net/" xhtml:target="_blank">Cruise Control</xhtml:a>, the first “continuous integration server”, is published under an open source license; it automates monitoring of the source code repository, triggering the build and test process, notifications of the results to the developers, and archival of the test reports; the period 2001-2007 sees a large number of similar tools appear, leading perhaps to an excessive focus on tools over practice.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>2004: an <xhtml:a xhtml:href="http://www.developertesting.com/archives/month200404/20040401-eXtremeFeedbackForSoftwareDevelopment.html" xhtml:target="_blank">article</xhtml:a> by Alberto Savoia proposes “Extreme Feedback Devices” such as lava lamps or dedicated monitors, to display the results of the most recent integration, an important innovation in CI.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Signs of Use</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>For most teams, continuous integration in practice amounts to the following:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>
                                    <xhtml:p>Use of a version control tool (CVS, SVN, Git, etc.).</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>An <xhtml:a xhtml:href="DevOps_AutomatedBuild.htm" xhtml:target="_blank" xhtml:title="Automated Build" xhtml:alt="Automated Build">automated build</xhtml:a> and product release process.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Instrumentation of the build process to trigger <xhtml:a xhtml:href="DevOps_UnitTesting.htm" xhtml:target="_blank" xhtml:title="Unit Testing" xhtml:alt="Unit Testing">unit</xhtml:a> and <xhtml:a xhtml:href="DevOps_AcceptanceTests.htm" xhtml:target="_blank" xhtml:title="Acceptance Tests" xhtml:alt="Acceptance Tests">acceptance tests</xhtml:a> “every time any change is published to version control”.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>In the event of even a single test failing, alerting the team of a “broken build” so that the team can reach a stable, releasable baseline again soonest.</xhtml:p>
                                </xhtml:li>
                                <xhtml:li>
                                    <xhtml:p>Optionally, the use of a tool such as a continuous integration server, which automates the process of integration, testing and reporting of test results.</xhtml:p>
                                </xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>Look for a dedicated display in a prominent spot of the <xhtml:a xhtml:href="DevOps_TeamRoom.htm" xhtml:target="_blank" xhtml:title="Team Room" xhtml:alt="Team Room">team room</xhtml:a>: this can be a normal monitor, an LED display or even a lava lamp, with the sole purpose of reporting on the result of the most recent integration.</xhtml:p>
                            <xhtml:p>Observe also how the team responds to a broken build, suggesting that a defect may have been detected. If the team is aware of defects, but tolerates them or continues working on a product that isn’t in a releasable state, the term continuous integration no longer applies, irrespective of tooling!</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Further Reading</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p><xhtml:a xhtml:href="http://www.martinfowler.com/articles/continuousIntegration.html" xhtml:target="_blank" xhtml:title="Continuous Integration, Martin Fowler (2006)" xhtml:alt="Continuous Integration, Martin Fowler (2006)">Continuous Integration</xhtml:a>, Martin Fowler (2006)</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>