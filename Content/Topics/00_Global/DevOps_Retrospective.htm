﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>DevOps - Retrospective (Heartbeat)</h1>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" target="_blank" title="Development Resources: IT Project Management" alt="Development Resources: IT Project Management">Development Resources: IT Project Management</a>.</p>
        <p>The team meets regularly, usually adhering to the rhythm of its iterations, to explicitly reflect on the most significant events to have occurred since the previous such meeting, and take decisions aiming at remediation or improvement.</p>
        <p>This is often a facilitated meeting following a set format. Several distinct formats have been described, depending in large part on the time set aside for the meeting, typically between one and three hours. One important reason to use a facilitated format is to give all team members an opportunity to speak up.</p>
        <p>The term “retrospective”, popularized by Norm Kerth (see below), has gained favour in the Agile community over better known ones such as “debriefing” or “post-mortem”, for its more positive connotations.</p>
        <p>This is also known as the “sprint retrospective”, or “iteration retrospective”, often abbreviated, e.g. “sprint retro”.</p>
        <p>The term “reflection workshop” from Alistair Cockburn is encountered less often, though it appears to have influenced the Agile Manifesto’s wording of the corresponding principle.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Common Pitfalls</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>A retrospective is intended to reveal facts or feelings which have measurable effects on the team’s performance, and to construct ideas for improvement based on these observations. It will not be useful if it devolves into a verbal joust, or a whining session.</p>
                    </li>
                    <li>
                        <p>On the other hand, an effective retrospective requires that each participant feel comfortable speaking up. The facilitator is responsible for creating the conditions of mutual trust; this may require taking into accounts such factors as hierarchical relationships, the presence of a manager for instance may inhibit discussion of performance issues.</p>
                    </li>
                    <li>
                        <p>Being an all-hands meeting, a retrospective comes at a significant cost in person-hours. Poor execution, either from the usual causes of bad meetings (lack of preparation, tardiness, inattention) or from causes specific to this format (lack of trust and safety, taboo topics), will result in the practice being discredited, even though a vast majority of the Agile community views it as valuable.</p>
                    </li>
                    <li>
                        <p>An effective retrospective will normally result in decisions, leading to action items; it’s a mistake to have too few (there is always room for improvement) or too many (it would be impractical to address “all” issues in the next iteration). One or two improvement ideas per iteration retrospective may well be enough.</p>
                    </li>
                    <li>
                        <p>Identical issues coming up at each retrospective, without measurable improvement over time, may signal that the retrospective has become an empty ritual.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Signs Of Use</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>Taking part, in an observer role, in one such meeting is the best way to ascertain the use of this practice.</p>
                    </li>
                    <li>
                        <p>Failing that, take note of the improvement ideas and action items which are the outputs of retrospectives: they can take the form of a written document, or often of Post-Its summarizing ideas and decisions, displayed in a specific spot on the <a href="DevOps_TeamRoom.htm">team room’s</a> walls.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Expected Benefits</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>Retrospectives leverage the benefits of iterative development: they offer explicit opportunities to improve the team’s performance over the duration of the project.</p>
                    </li>
                    <li>
                        <p>Retrospectives promote ownership and responsibility by the project team with respect to all aspects of the process; participants can understand the rationale behind all process decisions.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Origins</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <ul>
                    <li>
                        <p>1997: in “<a href="http://www.amazon.com/dp/0201498340" target="_blank">Surviving Object-Oriented Projects</a>“, Alistair Cockburn describes several projects (dating as far back as 1993) informally using the practice, but does not give it a label; he summarizes it as “Work in increments, focus after each”.</p>
                    </li>
                    <li>
                        <p>2001: the first description of a “reflection workshop” in the context of an Agile project appears in Alistair Cockburn’s “<a href="http://www.amazon.com/dp/0201699699" target="_blank">Agile Software Development</a>“.</p>
                    </li>
                    <li>
                        <p>2001: regular retrospectives are one of the principles of the <a href="http://agilemanifesto.org/" target="_blank">Agile Manifesto</a>: “At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly”, though not necessarily yet common practice.</p>
                    </li>
                    <li>
                        <p>2001: the term “Project Retrospectives” is introduced in Norm Kerth’s <a href="http://www.dorsethouse.com/books/pr.html" target="_blank">book</a> of the same name.</p>
                    </li>
                    <li>
                        <p>2001: the XP community endorses retrospectives early on, by way of a paper at XP2001 on “<a href="http://v/" target="_blank">Adaptation: XP Style</a>“.</p>
                    </li>
                    <li>
                        <p>2003: thanks in good part to <a href="http://web.archive.org/web/20031208002014/&lt;a href=" target="_blank">http://xpday3.xpday.org/sessions.php#Retro</a>‘&gt;sessions at the XP Day cycle of conferences, more teams start practicing project and iteration retrospectives.</p>
                    </li>
                    <li>
                        <p>2006: the publication of Esther Derby and Diana Larsen’s “<a href="https://www.agilealliance.org/resources/books/agile-retrospectives-making-good-teams-great/" target="_blank">Agile Retrospectives</a>” brings to a close the codification of heartbeat retrospective.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>