﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>DevOps - Kanban</h1>
        <p class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">You can find more information about Agile and Scrum in <a href="../../../../DevDoc_02_Res_Gen_ITProjects/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" target="_blank" title="Development Resources: IT Project Management" alt="Development Resources: IT Project Management">Development Resources: IT Project Management</a>.</p>
        <p>The Kanban Method is a means to design, manage, and improve flow systems for knowledge work. The method also allows organisations to start with their existing workflow and drive evolutionary change. They can do this by visualising their flow of work, limit work in progress (WIP) and stop starting and start finishing.</p>
        <p>The Kanban Method gets its name from the use of kanban – visual signaling mechanisms to control work in progress for intangible work products.</p>
        <p>A general term for systems using the Kanban Method is flow – reflecting that work flows continuously through the system instead of being organised into distinct timeboxes.</p>
        <p>Kanban can be used in any knowledge work setting, and is particularly applicable in situations where work arrives in an unpredictable fashion and/or when you want to deploy work as soon as it is ready, rather than waiting for other work items.</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Values</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Teams applying Kanban to improve the services they deliver embrace the following values:</p>
                <ul>
                    <li>
                        <p><b>Transparency</b>: Sharing information openly using clear and straightforward language improves the flow of business value.</p>
                    </li>
                    <li>
                        <p><b>Balance</b>: Different aspects, viewpoints, and capabilities must be balanced in order to achieve effectiveness.</p>
                    </li>
                    <li>
                        <p><b>Collaboration</b>: Kanban was created to improve the way people work together.</p>
                    </li>
                    <li>
                        <p><b>Customer Focus</b>: Kanban systems aim to optimise the flow of value to customers that are external from the system but may be internal or external to the organisation in which the system exists.</p>
                    </li>
                    <li>
                        <p><b>Flow</b>: Work is a continuous or episodic flow of value.</p>
                    </li>
                    <li>
                        <p><b>Leadership</b>: Leadership (the ability to inspire others to act via example, words, and reflection) is needed at all levels in order to realise continuous improvement and deliver value.</p>
                    </li>
                    <li>
                        <p><b>Understanding</b>: Individual and organisational self-knowledge of the starting point is necessary to move forward and improve.</p>
                    </li>
                    <li>
                        <p><b>Agreement</b>: Everyone involved with a system are committed to improvement and agree to jointly move toward goals while respecting and accommodating differences of opinion and approach.</p>
                    </li>
                    <li>
                        <p><b>Respect</b>: Value, understand, and show consideration for people.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Principles</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Change Management Principles</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Kanban is structured to address the human tendency to resist change.</p>
                        <ul>
                            <li>
                                <p><b>Start with what you do now</b>: Understand current processes as they are actually practiced and respect existing roles, responsibilities and job titles.</p>
                            </li>
                            <li>
                                <p><b>Agree to pursue improvement through evolutionary change</b>.</p>
                            </li>
                            <li>
                                <p><b>Encourage acts of leadership at every level</b>.</p>
                            </li>
                        </ul>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Service Delivery Principles</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>These principles acknowledge that organisations are a collection of interdependent services, and to place the focus on the work, not the people doing the work.</p>
                        <ul>
                            <li>
                                <p>Understand and focus on your customers’ needs and expectations.</p>
                            </li>
                            <li>
                                <p>Manage the work; let people self-organise around it.</p>
                            </li>
                            <li>
                                <p>Evolve policies to improve customer and business outcomes.</p>
                            </li>
                        </ul>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Practices</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The following practices are activities essential to manage a kanban system:</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Visualise</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Kanban systems use mechanisms such as a kanban board to visualise work and the process it goes through.  In order for the visualisation to be the most effective, it should show:</p>
                        <ul>
                            <li>
                                <p>where in the process a team working on a service agrees to do a specific work item (commitment point).</p>
                            </li>
                            <li>
                                <p>Where the team delivers the work item to a customer (delivery point).</p>
                            </li>
                            <li>
                                <p>Policies that determine what work should exist in a particular stage.</p>
                            </li>
                            <li>
                                <p>WIP Limits.</p>
                            </li>
                        </ul>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Limit work in progress</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>When you establish limits to the amount of work you have in progress in a system and use those limits to guide when to start new items, you can smooth out the flow of work and reduce lead times, improve quality, and deliver more frequently.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Manage flow</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>The flow of work in a service should maximise value delivery, minimise lead times and be as predictable as possible.  Teams use empirical control through transparency, inspection and adaption in order to balance these potentially conflicting goals.  A key aspect of managing flow is identifying and addressing bottlenecks and blockers.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Make policies explicit</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Explicit policies help explain a process beyond just the listing of different stages in the workflow.  Policies should be sparse, simple, well-defined, visible, always applied, and readily changeable by the people working on the service.  Examples of policies include: WIP Limits, capacity allocation, definition of done, and other rules for work items existing various stages in the process.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Implement feedback loops</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Feedback loops are an essential element in any system looking to provide evolutionary change.  Look up <a href="#Lifecycle">Lifecycle below</a>, which describes Feedback loops used in Kanban in more detail.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Improve collaboratively, evolve experimentally</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Kanban starts with the process as it currently exists and applies continuous and incremental improvement instead of trying to reach a predefined finished goal.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Roles</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Given Kanban’s approach to start with your existing process and evolve it, there are no roles explicitly called for when adopting Kanban.  Use the roles you currently have on your team.</p>
                <p>There are two roles that have emerged in practice that serve particular purposes.  It’s highly likely that these functions are filled by someone in an existing role as mentioned below.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Service Request Manager</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Understands the needs and expectations of customers, and facilitates the selection and ordering of work items at the Replenishment Meeting.  This function is often filled by a product manager, <a href="DevOps_ProductOwner.htm" target="_blank" title="Product Owner" alt="Product Owner">product owner</a>, or service manager.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Service Delivery Manager</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Responsible for the flow of work to deliver select items to customers.  Facilitates the Kanban Meeting and Delivery Planning.  Other names for this function include flow manager, delivery manager, or flow master.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot><a name="Lifecycle"></a>Lifecycle</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>Because work items tend to flow through a kanban system in single piece flow, and each system is different with respect to stages in its workflow, the best way to describe the lifecycle of the Kanban method is via the feedback loops involved.</p>
                <p>Those feedback loops (cadences) are:</p>
                <ul>
                    <li>
                        <p><b>Strategy Review (Quarterly)</b>: Select the services to provide and the context in which those services are appropriate.</p>
                    </li>
                    <li>
                        <p><b>Operations Review (Monthly)</b>: Understand the balance between and across services, including deploying people and resources to maximise value delivery</p>
                    </li>
                    <li>
                        <p><b>Risk Review (Monthly)</b>: Understand and respond to delivery risks in services</p>
                    </li>
                    <li>
                        <p><b>Service Delivery Review (Bi-Weekly)</b>: Examine and improve the effectiveness of a service.  This is similar to a retrospective that is focused on improving the kanban system.</p>
                    </li>
                    <li>
                        <p><b>Replenishment Meeting (Weekly)</b>: Identify items that the team will work on and determine which work items may be selected next.  This is analogous to a planning meeting for a sprint or iteration.</p>
                    </li>
                    <li>
                        <p><b>The Kanban Meeting (Daily)</b>: A team working on a service coordinates their activities for the day.  This is analogous to a daily standup.</p>
                    </li>
                    <li>
                        <p><b>Delivery Planning Meeting (Per Delivery Cadence)</b>: Monitor and plan deliveries to customers.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Origins and Further Reading</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The Kanban Method formalised the flow approach to knowledge work and offered a means for organisations to improve their software development processes and adopt agile values and principles without first having to undergo substantial cultural change.</p>
                <ul>
                    <li>
                        <p>2004 David J. Anderson applies a theory of constraints approach incorporating a kanban pull system on a project in <a href="http://images.itrevolution.com/images/kanbans/From_Worst_to_Best_in_9_Months_Final_1_3-aw.pdf" target="_blank">Microsoft’s IT department</a>.</p>
                    </li>
                    <li>
                        <p>2006 – 2007 David J. Anderson identifies the Kanban Method through applying it a project at Corbis.</p>
                    </li>
                    <li>
                        <p>2009 Donald Reinertsen discuss the adoption of kanban systems and the use of data collection and an economic model for management decision-making in his book <a href="http://amzn.to/2ozAFl3" target="_blank">The Principles of Product Development Flow</a>.</p>
                    </li>
                    <li>
                        <p>2009 Corey Ladas suggested that Kanban could improve upon Scrum and suggested Scrumban as the transition from Scrum to Kanban in his book <a href="http://amzn.to/2n8OsPq" target="_blank">Scrumban – Essays on Kanban Systems for Lean Software Development</a>.</p>
                    </li>
                    <li>
                        <p>2010 David J. Anderson publishes <a href="http://amzn.to/2oAvtNx" target="_blank">Kanban: Successful Evolutionary Change for Your Technology Business</a> as the first description of the method.</p>
                    </li>
                </ul>
                <ul>
                    <li>
                        <p><a href="http://amzn.to/2oAmtYR" target="_blank">Personal Kanban; Mapping Work, Navigating Life</a> by Jim Benson and Tonianne DeMaria Barry.</p>
                    </li>
                    <li>
                        <p><a href="http://amzn.to/2oXEXyR" target="_blank">Essential Kanban Condensed</a> by David J. Anderson and Andy Carmichael.</p>
                    </li>
                    <li>
                        <p><a href="http://amzn.to/2oAkCn3" target="_blank">Lean from the Trenches: Managing Large-Scale Projects with Kanban</a> by Henrik Kniberg.</p>
                    </li>
                    <li>
                        <p><a href="https://www.infoq.com/minibooks/kanban-scrum-minibook" target="_blank">Kanban and Scrum Making the most of both</a> by Henrik Kniberg and Mattias Skarin.</p>
                    </li>
                </ul>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>